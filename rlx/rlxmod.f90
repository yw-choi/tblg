module rlxmod
  use mpi
  use kinds, only: DP
  use lattice, only: na_uc, atoms_uc, N_LAYER, na_uc_l, latt
  use phonons_fc, only: compute_fc
  use constants, only: ANGSTROM_AU, RYTOEV, PI
  
  implicit none
  public

  character(len=100) :: initial_dx

  integer :: nmode, constraint_type, vdw_type

  logical :: do_rlx, symmetrize

  integer :: nfc
  real(dp) :: conv, rcut
  real(dp), allocatable :: fc(:,:,:)
  integer, allocatable :: fc_pairs(:,:)

contains

  subroutine do_relax

    real(dp), allocatable :: x(:), atoms_rlx(:,:,:), g(:)
    real(dp) :: f
    integer :: ia, m1, il, ia_g

    call compute_fc (rcut, -1, nfc, fc_pairs, fc)

    nmode = 3*na_uc

    allocate(x(nmode), atoms_rlx(3,na_uc,N_LAYER), g(nmode))

    x = 0.d0
    if (master.and.trim(initial_dx).ne.'') then
      open(11, file=trim(initial_dx), form="formatted")
      do ia = 1, na_uc
        read(11,*) x(3*(ia-1)+1:3*(ia-1)+3)
      enddo
      close(11)

      x = x * ANGSTROM_AU
    endif

    call mpi_bcast(x, nmode, mpi_double_precision, 0, comm, mpierr)

    if (do_rlx) then

      if (master) print *, "Start minimization.."
      call start_clock('minimize')
      call minimize ( nmode, x, f, g )
      call stop_clock('minimize')

      if (symmetrize) then
        ! call symmetrize_x(nmode, x)
      endif

      call calcfg ( nmode, x, f, g )

      if (master) then
        open(11, file="atoms.out", form="formatted")
        write(11, "(3F24.16)") latt(:,1)/ANGSTROM_AU
        write(11, "(3F24.16)") latt(:,2)/ANGSTROM_AU 
        write(11, "(3F24.16)") latt(:,3)/ANGSTROM_AU 
        write(11, *) na_uc

        print "(1x,A,F24.16)", 'Total Energy (eV) =', F*RYTOEV
        print *, "atom positions (Ang.) and forces (eV/Ang)"
        atoms_rlx = atoms_uc
        do il = 1, N_LAYER
          do ia = 1, na_uc_l
            ia_g = na_uc_l*(il-1)+ia
            atoms_rlx(:,ia,il) = atoms_rlx(:,ia,il)+x(3*(ia_g-1)+1:3*(ia_g-1)+3)
            print "(3F24.16,1x,3F24.16)", atoms_rlx(:,ia,il)/ANGSTROM_AU, -g(3*(ia_g-1)+1:3*(ia_g-1)+3)*RYTOEV*ANGSTROM_AU
            write(11, "(3F24.16,1x,3F24.16)") atoms_uc(:,ia,il)/ANGSTROM_AU, atoms_rlx(:,ia,il)/ANGSTROM_AU
          enddo
        enddo
        close(11)
      endif

      call mpi_bcast(atoms_rlx, 3*na_uc_l*N_LAYER, mpi_double_precision, 0, comm, mpierr)

    else

      call calcfg ( nmode, x, f, g )

      if (master) then
        print "(1x,A,F24.16)", 'Total Energy (eV) =', F*RYTOEV
        print *, "atom positions (Ang.) and forces (eV/Ang)"
        atoms_rlx = atoms_uc
        do il = 1, N_LAYER
          do ia = 1, na_uc_l
            ia_g = na_uc_l*(il-1)+ia
            atoms_rlx(:,ia,il) = atoms_rlx(:,ia,il)+x(3*(ia_g-1)+1:3*(ia_g-1)+3)
            print "(3F24.16,1x,3F24.16)", atoms_rlx(:,ia,il)/ANGSTROM_AU, -g(3*(ia_g-1)+1:3*(ia_g-1)+3)*RYTOEV*ANGSTROM_AU
          enddo
        enddo

        open(11, file="atoms.out", form="formatted")
        write(11, "(3F24.16)") latt(:,1)/ANGSTROM_AU
        write(11, "(3F24.16)") latt(:,2)/ANGSTROM_AU 
        write(11, "(3F24.16)") latt(:,3)/ANGSTROM_AU 
        write(11, *) na_uc
        atoms_rlx = atoms_uc
        do il = 1, N_LAYER
          do ia = 1, na_uc_l
            ia_g = na_uc_l*(il-1)+ia
            atoms_rlx(:,ia,il) = atoms_rlx(:,ia,il)+x(3*(ia_g-1)+1:3*(ia_g-1)+3)
            write(11, "(3F24.16,1x,3F24.16)") atoms_uc(:,ia,il)/ANGSTROM_AU, atoms_rlx(:,ia,il)/ANGSTROM_AU
          enddo
        enddo
        close(11)
      endif


    endif

  end subroutine do_relax

  subroutine minimize ( n, x, f, g)
    integer, intent(in) :: n
    real(dp), intent(inout) :: G(N),F,x(n) 

    real(dp) :: EPS,ACC
    real(dp), allocatable :: W(:)

    integer :: NMETH, MDIM, IFUN, ITER, NFLAG, MXFUN, &
               IOUT, IDEV

    !! USER-SUPPLIED PARAMETERS
    NMETH = 0 ! 0: CG, 1: BFGS
    EPS = conv
    MXFUN = 30000

    IF (rank.eq.0) THEN
      IOUT = 1
      IDEV = 6
    ELSE
      IOUT = 0
      IDEV = 0
    ENDIF
    ACC = 10.d-20

    MDIM = -1
    IF (NMETH.eq.0) THEN
      MDIM = 5*N+2
    ELSE IF (NMETH.eq.1) THEN
      MDIM = N*(N+7)/2
    ENDIF
    ALLOCATE(W(MDIM))
    
    CALL CONMIN(N,X,F,G,IFUN,ITER,EPS,NFLAG,MXFUN,W, &
                IOUT,MDIM,IDEV,ACC,NMETH)
    if (master) then
      print *, "NFLAG = ", NFLAG
    ENDIF

  end subroutine minimize

  subroutine symmetrize_x (n,x)
    use lattice, only: N_LAYER, N_INTRA_SHELL, NA_INTRA_SHELL, & 
                       na_uc, na_uc_l, na_sc, na_sc_l, &
                       atoms_uc, atoms_sc, sc_uc_map, &
                       isc_map, R_isc, max_inter_neighbors, &
                       num_inter_neighbors, inter_neighbors, &
                       intra_neighbors, ia_offset, na_loc, &
                       recvcounts, displs, &
                       n_C6_equiv, &
                       C6_equiv, &
                       C6_unique,  &
                       n_C6_unique, &
                       Myz_equiv
    integer, intent(in) :: n
    real(dp), intent(inout) :: x(n)

    integer :: il, ia_uc, ja_sc, ja_uc, ishell, ia_shell, &
               m1, m2, m3, ia_uc_g, ja_uc_g, i, j, il2, ifc, jl
    real(dp) :: at_i(3), at_j(3), dx(3), costh, sinth, rot(3,3), &
                dist, rfac, rfac6, rfac12, ni(3), nj(3), th, at_tmp(3)
    real(dp) :: r_ij, rho_ij, rho_ji, f_ij, f_ji, rd2_ij, rd2_ji, v_KC
    real(dp) :: f_intra, f_inter, exp_lrz, exp_rd2_ij, exp_rd2_ji,&
                dfdrho, njdx, nidx, Azr6

    real(dp) :: normals(3,na_uc_l,N_LAYER), xi(3), xj(3), xi_avg(3)

    ! C6
    do i = 1, n_C6_unique
      ia_uc = C6_unique(i)
      if (n_C6_equiv(i).eq.1) then
        x(3*(ia_uc-1)+1:3*(ia_uc-1)+3) = 0.d0
        ja_uc = C6_equiv(1,i)
        x(3*(ja_uc-1)+1:3*(ja_uc-1)+3) = 0.d0
      else
        xi = x(3*(ia_uc-1)+1:3*(ia_uc-1)+3)
        do j = 1, n_C6_equiv(i)
          ja_uc = C6_equiv(j,i)
          th = j*PI/3.0d0
          rot(1,:) = (/  cos(th),  sin(th), 0.0d0 /)
          rot(2,:) = (/ -sin(th),  cos(th), 0.0d0 /)
          rot(3,:) = (/    0.0d0,    0.0d0, 1.0d0 /)

          xj(1) = sum(rot(1,:)*xi(:))
          xj(2) = sum(rot(2,:)*xi(:))
          xj(3) = sum(rot(3,:)*xi(:))
          x(3*(ja_uc-1)+1:3*(ja_uc-1)+3) = xj
        enddo
      endif
    enddo

    ! Myz
    do ia_uc = 1, na_uc_l
      xi = x(3*(ia_uc-1)+1:3*(ia_uc-1)+3)

      xi(2) = -xi(2)
      xi(3) = -xi(3)
      ja_uc = Myz_equiv(ia_uc)
      ja_uc_g = na_uc_l+ja_uc

      x(3*(ja_uc_g-1)+1:3*(ja_uc_g-1)+3) = xi
    enddo
    
  end subroutine symmetrize_x
end module rlxmod
