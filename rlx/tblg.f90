module tblg
  use kinds, only: DP
  use constants, only: ANGSTROM_AU

  implicit none

  public

contains

  subroutine tblg_unitcell (M, N, shift, alat, d_layer, &
                            latt, na_uc, na_uc_l, atoms_uc, costh, theta)

    integer, intent(in) :: M, N ! commensurate supercell index
    real(dp), intent(in) :: alat, &    ! MLG lattice constant in Bohr
                            d_layer, & ! interlayer distance in Bohr
                            shift(2)   ! shift in the second layer (frac.)

    real(dp), intent(out) :: latt(3,3), & ! CUC lattice vector
                             costh, theta ! commensurate rotation angle
    integer, intent(out) :: na_uc, na_uc_l      ! # of atoms in CUC
    real(dp), allocatable, intent(out) :: atoms_uc(:,:,:) ! atom positions in Bohr

    integer :: ncell ! number of primitive unit cells per layer in a CUC (na_l = NA_PUC*ncell)

    integer, parameter :: N_LAYER = 2, & ! number of layers
                          NA_PUC  = 2    ! number of atoms in the primitive unit cell of each layer

    real(dp) :: latt_puc(2,2,N_LAYER),  & ! lattice vector of the primitive unit cell of each layer
                latt_puc_orig(2,2),     & ! lattice vector of the unrotated primitive unit cell
                rot(2,2,N_LAYER),       & ! rotation matrix for each layer: R(th/2), R(-th/2)
                atoms_puc(NA_PUC,2)       ! atom positions in fractional coordinates

    integer, allocatable :: cells(:,:,:),   & ! primitive lattice points of each layer inside the commensurate unit cell
                            displs_ia(:), recvcounts_ia(:)

    integer :: ia_offset, na_loc , ia, icell, ilayer, ia_uc
    real(dp) :: sinth

    na_uc = 4*(N*N+N*M+M*M)
    na_uc_l = na_uc/2

    allocate(atoms_uc(3,na_uc_l,N_LAYER))

    if (N.eq.1.and.M.eq.0 .or. M.eq.1.and.N.eq.0) then
      costh = 1.0d0
      theta = 0.d0
    else
      costh = (N*N+4.d0*N*M+M*M)/(2.d0*(N*N+N*M+M*M))
      theta = acos(costh)
    endif

    latt_puc_orig(:,1) = alat*(/ sqrt(3.d0)/2.d0, -1.d0/2.d0 /)
    latt_puc_orig(:,2) = alat*(/ sqrt(3.d0)/2.d0,  1.d0/2.d0 /)

    atoms_puc(:, 1) = (/ 1.0d0/3.0d0, 1.0d0/3.0d0 /)
    atoms_puc(:, 2) = (/ 2.0d0/3.0d0, 2.0d0/3.0d0 /)

    rot(1,:,1) = (/  cos(theta/2.d0),  sin(theta/2.d0) /)
    rot(2,:,1) = (/ -sin(theta/2.d0),  cos(theta/2.d0) /)
    rot(1,:,2) = (/  cos(theta/2.d0), -sin(theta/2.d0) /)
    rot(2,:,2) = (/  sin(theta/2.d0),  cos(theta/2.d0) /)

    latt_puc(:,:,1) = matmul(rot(:,:,1), latt_puc_orig)
    latt_puc(:,:,2) = matmul(rot(:,:,2), latt_puc_orig)
    
    latt = 0.d0
    latt(1:2,1) =  N*latt_puc(1:2,1,1) +     M*latt_puc(1:2,2,1)
    latt(1:2,2) = -M*latt_puc(1:2,1,1) + (N+M)*latt_puc(1:2,2,1)
    latt(3,3)   = 20.d0*ANGSTROM_AU ! vacuum (usually not used)
    ! latt(1,1) = alat*sqrt(dble(N*N+M*M+N*M))
    ! latt(1,2) = 0.5d0*alat*sqrt(dble(N*N+M*M+N*M))
    ! latt(2,2) = sqrt(3.0d0)/2.d0*alat*sqrt(dble(N*N+M*M+N*M))
    ! latt(3,3) = 20.d0*ANGSTROM_AU ! vacuum (usually not used)

    ! print "(2F24.16)", latt(1:2,1)/ANGSTROM_AU
    ! print "(2F24.16)", latt(1:2,2)/ANGSTROM_AU

    ! stop

    ncell = N*N+N*M+M*M
    allocate(cells(2,ncell,N_LAYER))
    cells = 0
    call find_lattice_points(N, M, ncell, cells(:,:,1))
    call find_lattice_points(M, N, ncell, cells(:,:,2))

    do ilayer = 1, N_LAYER
      ia_uc = 0
      do icell = 1, ncell
        do ia = 1, NA_PUC

          ia_uc = ia_uc + 1
          atoms_uc(1:2, ia_uc, ilayer) = matmul(latt_puc(1:2,1:2,ilayer), &
                                  cells(1:2,icell,ilayer) + atoms_PUC(1:2,ia))

          ! shift in the atoms in the second layer
          if (ilayer.eq.2) then
            atoms_uc(1:2, ia_uc, ilayer) = atoms_uc(1:2, ia_uc,ilayer) &
                                + matmul(latt_puc(1:2,1:2,ilayer), shift(1:2))
          endif
          atoms_uc(3, ia_uc, ilayer) = (ilayer-1)*d_layer
        enddo
      enddo
    enddo


  end subroutine tblg_unitcell

  subroutine find_lattice_points(N, M, ncell, lattice_points)

    integer, intent(in) :: N, M, ncell
    integer, intent(out) :: lattice_points(2,ncell)

    integer :: ip, i1, i2
    real(dp) :: fM, fN

    if (N.eq.0.and.M.eq.1 .or. M.eq.0.and.N.eq.1) then
      lattice_points(:, :) = 0
      return
    endif

    fM = dble(M)
    fN = dble(N)
    ip = 0
    do i1 = -M, N-1
      do i2 = 0, N+2*M-1
        if (                fM/fN*i1 .le. i2 .and. &
                      -(fN+fM)/fM*i1 .le. i2 .and. &
               fN+fM + fM/fN*(i1+fM) .gt. i2 .and. &
             fM - (fN+fM)/fM*(i1-fN) .gt. i2) then
          ip = ip + 1
          lattice_points(:, ip) = (/ i1, i2 /)
        endif
      enddo
    enddo
  end subroutine find_lattice_points
end module tblg
