from numpy import *
import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches

def main():
    M = 0
    N = 1
    a = 2.46 # Ang
    nk1 = 30
    nk2 = 30
    ne = 10 # number of energy contours

    latt, relatt = generate_commensurate_cell(M, N, a)
    enk = loadtxt('enk.dat')
    nk, nb = shape(enk)

    emin = enk.min()
    emax = enk.max()

    print nk, nb, emin, emax, ne

    if nk!=nk1*nk2:
        print "nk mismatch"
        return

    kx = zeros((nk1,nk2))
    ky = zeros((nk1,nk2))
    enk_grid = zeros((nb,nk1,nk2))

    for ik1 in range(nk1):
        for ik2 in range(nk2):
            x1 = float(ik1)/nk1
            x2 = float(ik2)/nk2
            k = x1*relatt[0,0:2]+x2*relatt[1,0:2]
            kx[ik1,ik2] = k[0]
            ky[ik1,ik2] = k[1]

            ik = ik1*nk1 + ik2

            for ib in range(nb):
                enk_grid[ib,ik1,ik2] = enk[ik, ib]

    egrid = linspace(emin, emax, ne)
    for ie, e in enumerate(egrid[1:-2]):
        fig = plt.figure(ie)
        ax = fig.subplots()
        for ib in range(nb):
            ek = enk_grid[ib,:,:]

            if ek.min()>e or ek.max()<e:
                continue

            cs = ax.contour(kx, ky, ek, levels=[e])

        # BZ
        plot_patch(ax, [
             relatt.T.dot([2./3., 1./3., 0.])[:2],
             relatt.T.dot([1./3., 2./3., 0.])[:2],
             relatt.T.dot([-1./3., 1./3., 0.])[:2],
            -relatt.T.dot([2./3., 1./3., 0.])[:2],
            -relatt.T.dot([1./3., 2./3., 0.])[:2],
            -relatt.T.dot([-1./3., 1./3., 0.])[:2],
             relatt.T.dot([2./3., 1./3., 0.])[:2],
            ],0.5,'r')
        plt.savefig('contour.%d.png'%ie)

    savetxt('egrid.dat', egrid[1:-2].T)
    # fig = plt.figure()
    # ax = fig.gca(projection='3d')

    # surf = ax.plot_surface(kx, ky, enk_grid[0,:,:], cmap='coolwarm', \
    #                            linewidth=0, antialiased=False)
    # fig.colorbar(surf, shrink = .5 , aspect=5)
    # plt.show()

    # for ie, e in enumerate(egrid):
    #     plt.figure(ie)


def generate_commensurate_cell(M, N, a):

    if M==1 and N==0 or M==0 and N==1:
        th = 0.0
    else:
        th = arccos(float(N*N+4.*N*M+M*M)/float(2.*(N*N+N*M+M*M)))
    a_puc = a*array([[sqrt(3.)/2., -1./2., 0.],
                     [sqrt(3.)/2.,  1./2., 0.],
                     [0., 0., 10.0/a]]).T

    rot = array([[cos(th/2),  sin(th/2), 0.],
                 [-sin(th/2),  cos(th/2), 0.],
                 [     0.,       0., 1.]])
    rot2 = array([[ cos(th/2), -sin(th/2), 0.],
                  [ sin(th/2),  cos(th/2), 0.],
                  [        0.,       0., 1.]])

    a_1st = rot.dot(a_puc)
    a_2nd = rot2.dot(a_puc)

    # commensurate cell vectors
    latt = array([[ N,   M, 0], \
                   [-M, N+M, 0], \
                   [ 0,   0, 1]]).dot(a_1st.T).T

    relatt = 2*pi*linalg.inv(latt)

    return latt, relatt
def plot_patch(ax, vertices, lw, color):
    ax.add_patch(patches.PathPatch(Path(vertices), lw=lw, edgecolor=color, fill=False))
main()
