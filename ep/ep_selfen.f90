module ep_selfen

  use mpi
  use kinds, only: dp
  use env, only: data_ep
  use constants, only: TPI, RYTOEV, eps12

  use lattice, only: &
    latt, relatt, &
    nrot, s, t_rev, time_reversal

  use epmat, only: &
    dimk, nkmax, nqmax, nq, nk, xk, wk, &
    equiv, equiv_s, ik_stars, ik_isym, n_stars, &
    nbnd, enk, nmode, imodes, freq, &
    acoustic_cut, get_kq, get_ik, &
    IU_EPMAT

  implicit none

  logical :: lep_selfen

  integer :: nef
  real(dp) :: &
    ep_temp, dg_se

  real(dp) :: &
    N_F, e_f

contains

  subroutine ep_selfen_main
    integer :: ief
    real(dp) :: efmin, efmax
    
    if (master) then
      print '(a)', repeat('=',60)
      print "(1x,a,F12.6)", "Calculation of electron self-energy"
      print '(a)', repeat('=',60)
    endif

    efmin = minval(enk)
    efmax = maxval(enk)

    do ief = 1, nef
      if (nef.eq.1) then
        e_f = efmin
      else
        e_f = efmin + (efmax-efmin)/(nef-1)*(ief-1)
      endif

      call compute_occ

      call selfen_elec

    enddo

    
  end subroutine ep_selfen_main

  subroutine selfen_elec 
    integer :: &
      recvcounts(0:nprocs-1), displs(0:nprocs-1), iq_offset, nq_loc, &
      iq_loc, iq_irr, iq, imode, i, ik_irr, ik, &
      ibnd, jbnd, ikq, ikq_irr, ikq_sym, recl_epmat
      
    real(dp) :: ekk, ekq, wgq, wgkq, wq, w0g1, w0g2, g2, weight
    complex(dp) :: ieta
    character(len=100) :: timestr, fn

    real(dp), allocatable :: &
      epmat(:,:,:)

    call start_clock ('selfen_elec')

    allocate(epmat(nbnd,nbnd,nk))
    inquire(iolength=recl_epmat) epmat

    ieta = cmplx(0.d0, dg_se)

    call distribute_indices(nqmax, nprocs, displs, recvcounts) 
    nq_loc = recvcounts(rank)
    iq_offset = displs(rank)

    do iq_loc = 1, nq_loc
      iq = iq_offset + iq_loc
      iq_irr = equiv(iq)

      write(fn, '(a,I0.6,a)') &
        trim(data_ep)//'/epmat.', iq, '.dat'
      open(IU_EPMAT, file=trim(fn), form="unformatted", &
        status="old", access="direct", recl=recl_epmat)

      do i = 1, nmode
        imode = imodes(i)
        wq = freq(i, iq_irr)
        if (wq.lt.acoustic_cut) then
          cycle
        endif

        wgq = wgauss(-wq/ep_temp, -99)
        wgq = wgq / (1.d0 - 2.d0 * wgq)

        read(IU_EPMAT, rec=i) epmat

        do ik_irr = 1, nk
          ik = get_ik(ik_irr)
          call get_kq(ik, iq, ikq, ikq_irr, ikq_sym)

          do ibnd = 1, nbnd
            ekk = enk(ibnd,ik_irr) - e_f
            do jbnd = 1, nbnd
              ekq = enk(jbnd,ikq_irr) - e_f
              wgkq = wgauss( -ekq/ep_temp, -99)
              g2 = lqv(i,iq_irr) * epmat(ibnd,jbnd,ik_irr)

              weight = dble( &
              ( (       wgkq + wgq ) / ( ekk - ( ekq - wq ) - ieta )  +  &
                ( one - wgkq + wgq ) / ( ekk - ( ekq + wq ) - ieta ) ) )

              sigmar(ibnd,ik_irr) = sigmar(ibnd,ik_irr)
            enddo
          enddo
        enddo
      enddo
      close(IU_EPMAT)
    enddo


    call stop_clock ('selfen_elec')
  end subroutine selfen_elec 

  subroutine compute_occ 
    integer :: nspin, isk(nk), is, i, ibnd, ik, ik_irr 
    real(dp) :: nelec, dos_t(2)
    character(len=100) :: fn

    call start_clock('wnk')
    isk = 1
    is = 0
    nspin = 1
    nelec = 0.d0 !! not used in tetra_weights_only

    call linear_tetra_weights_only (nk, nspin, is, isk, nbnd, ntetra, &
      tetra, enk, e_f, wnk, dwnk)

    call tetra_dos_t (enk, nspin, nbnd, nk, e_f, dos_t)

    ! per spin
    N_F = dos_t(1) * .5d0

    wnk = wnk * .5d0 
    if (N_F.lt.eps12) then
      dwnk = 0.d0
    else
      dwnk = dwnk * .5d0 / N_F  
      dwnk = dwnk/sum(dwnk)
    endif

    call stop_clock('wnk')
  end subroutine compute_occ
end module ep_selfen 
