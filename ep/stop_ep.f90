! Gracefully terminate the program
subroutine stop_ep
  use mpi
  integer :: iproc

  call stop_clock ( 'EP' )

  call printblock_start ('Timing info')
  if (rank.eq.0) then 
    call print_clock ( ' ' )
    call system('rm -f CRASH')
  else
    call print_clock('melec')
    call print_clock('g2')
  endif


  call printblock_end()
  call timestamp('End of run')
  call MPI_Finalize(mpierr)
end subroutine stop_ep
