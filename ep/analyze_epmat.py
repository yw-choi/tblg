from numpy import *
import matplotlib.pyplot as plt
from matplotlib.mlab import griddata
from scipy.io import FortranFile
def main():
    f = FortranFile('epmat_band.dat', 'r')
    nband, nband, nk, nmode, nq = f.read_ints(dtype=int32)
    epmat = f.read_record(dtype=complex128).reshape((nband,nband,nk,nmode,nq))
    print shape(epmat)
    imode = 0
    ibnd = 1
    jbnd = 1

    ik = 1617-1

    epmat_q = epmat[ibnd,jbnd,ik,imode,:]

    qpoints = loadtxt('qpoints.dat')

    x = []
    y = []
    z = []
    for iq in range(nq):
        x.append(qpoints[iq,0])
        y.append(qpoints[iq,1])
        z.append(abs(epmat_q[iq]))

    plt.figure(ik)
    plt.tripcolor(x,y,z, shading='gouraud')
    plt.savefig('epmat.%d.png'%ik)
    plt.show()

main()


