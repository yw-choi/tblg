module epmat

  use mpi
  use constants, only: &
    RYTOEV, eps6, eps12, eps14, eps16, PI, TPI, ANGSTROM_AU

  use kinds, only: DP

  use env, only: data_ep, data_ph, data_tb

  use lattice, only: &
    latt, relatt, &
    na_uc, atoms_uc, &
    na_sc, atoms_sc, &
    sc_uc_map, neighbors, num_neighbors, max_neighbors, &
    nsc, R_isc, isc_map, &
    nrot, s, sr, t_rev, time_reversal, irt

  use io, only: &
    read_2d_real_fmt, &
    read_2d_cmplx_rec, &
    read_2d_cmplx

  use kgrid, only: &
    kpoint_grid 

  implicit none
  public

  logical :: lread_epmat

  integer :: &
    lowest_band, &  ! lowest band index to calculate epmat
    highest_band, & ! highest band index to calculate epmat
    nbnd_tot, &    ! total number of bands as calculated by tb.x
    nbnd, &        ! highest_band-lowest_band+1
    nmode_tot, &
    nmode, &
    start_task, &
    end_task, &
    n_print_progress

  integer :: &
    dimk, &
    nkmax, &
    nqmax, &
    nq, &
    nk

  real(dp), allocatable :: &
    xk(:,:), &  ! k-points in frac. cooridnate
    xk_cart(:,:), &  
    wk(:)       ! k-points weight

  integer, allocatable :: &
    equiv(:), &
    equiv_s(:), &
    ik_stars(:,:),  &
    ik_isym(:,:), &
    n_stars(:)

  real(dp) ::  &
    rcut_tb,   & ! cutoff for the hopping derivatives
    mass         ! carbon mass (Ry)

  real(dp), allocatable ::  &
    enk(:,:),  & ! enk(nbnd,nk) electron energies (Ry)
    freq(:,:), & ! freq(nmode,nq) phonon frequencies (Ry)
    lqv(:,:)     ! lqv(nmode,nq) phonon length scale

  complex(DP), allocatable :: &
    phase_fac(:,:)        
                         
  real(dp), allocatable :: &
    epmat_orb(:,:,:)  !  el-ph matrix elements in orbital basis
  
  real(dp) :: &
    acoustic_cut

  character(len=100) :: &
    imodes_fn 
  integer, allocatable :: imodes(:)

  integer, parameter :: &
    IU_EVQ = 100, &
    IU_EPMAT = 101, &
    IU_MELEC = 102

contains 

  subroutine setup_kqgrid
    integer :: ik, ik1, ik2, ik_equiv, ik_irr, iq, ikq, ikq_irr, ikq_sym
    real(dp) :: xkg(3)
    call start_clock ('kqgrid')
    call printblock_start ('k,q-point grid')

    nkmax = dimk*dimk
    allocate(xk(3,nkmax), wk(nkmax), equiv(nkmax), equiv_s(nkmax), xk_cart(3,nkmax))
    xk = 0.d0
    wk = 0.d0

    if (master) then
      call kpoint_grid (nrot, time_reversal, .false., &
        s, t_rev, nkmax, 0,0,0,dimk,dimk,1, nk, xk, wk, &
        equiv, equiv_s, n_stars, ik_stars, ik_isym )

      print *, "k-point grid dimension = ", dimk, "x", dimk
      print *, "Number of irreducible k-points = ", nk

      open(11, file=trim(data_ep)//"/kpoints.dat", form="formatted")
      write(11, *) nkmax, nk
      do ik = 1, nk
        xk_cart(:,ik) = matmul( relatt, xk(:,ik ) )
        write(11, "(4F10.6,1x,F12.8)") xk(1:2,ik), TPI*xk_cart(1:2,ik), wk(ik)
      enddo
      close(11)
    endif

    call mpi_bcast(nk, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(xk, 3*nk, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(wk, nk, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(equiv, nkmax, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(equiv_s, nkmax, mpi_integer, 0, comm, mpierr)

    if (.not.master) then
      allocate(n_stars(nk), ik_stars(2*nrot,nk), ik_isym(2*nrot,nk))
    endif

    call mpi_bcast(n_stars, nk, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(ik_stars, 2*nrot*nk, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(ik_isym, 2*nrot*nk, mpi_integer, 0, comm, mpierr)

    nqmax = nkmax
    nq = nk

    call printblock_end
    call stop_clock ('kqgrid')
  end subroutine setup_kqgrid

  subroutine read_imodes
    integer :: imode, i

    nmode_tot = 3*na_uc
    
    if (trim(imodes_fn).ne.'') then
      if (master) then
        open(11, file=trim(imodes_fn), form="formatted")
        read(11,*) nmode
        allocate(imodes(nmode))
        do imode = 1, nmode
          read(11,*) imodes(imode)
        enddo
        close(11)
      endif

      call mpi_bcast(nmode, 1, mpi_integer, 0, comm, mpierr)

      if (.not.master) then
        allocate(imodes(nmode))
      endif
      call mpi_bcast(imodes, nmode, mpi_integer, 0, comm, mpierr)
    else
      nmode = nmode_tot
      allocate(imodes(nmode))
      do imode = 1, nmode
        imodes(imode) = imode
      enddo
    endif
    
    if (master) then
      do i = 1, nmode
        print *, "i, imodes(i) = ", i, imodes(i)
      enddo
    endif

  end subroutine read_imodes

  subroutine read_eigenvalues
    integer :: iq, imode, i, ik
    character(len=100) :: fn
    real(dp), allocatable :: dat(:)

    call start_clock ('eigenvalues')
    call printblock_start ('Reading eigenvalues')

    nmode_tot = 3*na_uc

    if (lowest_band.le.0) lowest_band = 1
    if (highest_band.le.0) highest_band = nbnd_tot

    nbnd = highest_band-lowest_band+1

    allocate(enk(nbnd,nk))

    if (master) then
      write(fn, '(a)') trim(data_tb)//'/enk.dat' 
      call read_2d_real_fmt(fn, nbnd_tot, lowest_band, highest_band, nk, 1, nk, enk)
    endif

    call mpi_bcast(enk, nbnd*nk, mpi_double_precision, 0, comm, mpierr)

    allocate(freq(nmode,nk), dat(nmode_tot))
    if (master) then
      write(fn, '(a)') trim(data_ph)//'/freq.dat' 
      open(11, file=trim(fn), form="formatted")
      read(11, *)
      do ik = 1, nk
        read(11,*) dat(1:nmode_tot)
        do i = 1, nmode
          freq(i,ik) = dat(imodes(i))
        enddo
      enddo
      close(11)
    endif
    call mpi_bcast(freq, nmode*nk, mpi_double_precision, 0, comm, mpierr)

    allocate(lqv(nmode,nk))
    do ik = 1, nk
      do i = 1, nmode
        if (freq(i,ik).lt.acoustic_cut) then
          lqv(i,ik) = 0.d0
        else
          lqv(i,ik) = sqrt(1.d0/(2*mass*freq(i,ik)))
        endif
      enddo
    enddo

    if (master) then
      print *, "nbnd_tot, nbnd = ", nbnd_tot, nbnd
      print *, "lb, hb = ", lowest_band, highest_band
      print *, "nmode_tot, nmode= ", nmode_tot, nmode
    endif
    call printblock_end
    call stop_clock ('eigenvalues')
  end subroutine read_eigenvalues

  subroutine compute_epmat
    integer :: iq, reclen, ik
    character(len=100) :: epmat_fn
    complex(dp), allocatable :: tmp(:,:,:)

    call printblock_start('Computing ep matrix elements')
    call start_clock ('epmat')

    call compute_epmat_orbital
    call compute_phase_fac
    call compute_epmat_band

    call printblock_end
    call stop_clock ('epmat')
  end subroutine compute_epmat

  ! Compute electron-phonon matrix elements in tight-binding orbital basis
  subroutine compute_epmat_orbital

    integer :: ia, inb, ix
    real(dp) :: at_I(3), at_J(3), dx(3)

    call start_clock ('epmat_orb')
    if (master) then
      print *, "Computing epmat in orbital basis" 
    endif

    allocate(epmat_orb(max_neighbors, 3, na_uc))

    epmat_orb = 0.d0

    do ia = 1, na_uc
      at_I = atoms_uc(1:3, ia)
      do inb = 1, num_neighbors(ia)
        at_J = atoms_sc(1:3, neighbors(inb, ia))
        dx = at_I - at_J 
        do ix = 1, 3
          epmat_orb(inb, ix, ia) = dtdx(dx, ix)
        enddo
      enddo
    enddo

    call stop_clock ('epmat_orb')
  end subroutine compute_epmat_orbital

  subroutine compute_phase_fac

    integer :: ik1, ik2, ik, isc

    real(dp) :: arg, xkg(2)

    call start_clock ('phase_fac')
    if (master) then
      print *, "Computing phase factors"
    endif

    allocate(phase_fac(nsc, nkmax))
    phase_fac = 0.d0

    do ik = 1, nkmax
      do isc = 1, nsc
        ik1 = int((ik-1)/dimk)+1
        ik2 = mod(ik-1,dimk)+1
        xkg(1) = dble(ik1-1)/dimk
        xkg(2) = dble(ik2-1)/dimk

        arg = TPI*sum(R_isc(1:2,isc)*xkg)
        phase_fac(isc,ik) = cmplx(cos(arg),sin(arg),kind=dp)
      enddo
    enddo

    call stop_clock ('phase_fac')
  end subroutine compute_phase_fac

  subroutine compute_epmat_band

    complex(DP), allocatable :: &
      evq_irr(:), &  
      evq_star(:), &
      ck(:,:), &
      ckq(:,:), &
      M_elec(:,:,:,:), & 
      cnk(:,:,:) ! cnk(na_uc,nbnd,nk) electron eigenvectors

    real(dp), allocatable :: &
      g2(:,:,:)

    !------------------------------------------------------!
    ! Local variables                                      ! 
    !------------------------------------------------------!
    character(len=100) :: &
      fn, timestr, fn_epmat

    integer :: &
      ntask, itask, itask_prev, itask_rank, iproc, &
      recl_melec, recl_evq, recl_epmat, MPI_STATUS(MPI_STATUS_SIZE), &
      istar, iq_star, iq_sym, ik, ik_irr, iq_irr, &
      ikq, ikq_irr, ikq_sym, &
      i, imode, ia_uc, ix, ja_uc, ja_sc, jsc, &
      inb, ibnd, jbnd

    logical :: &
      all_done, procs_done(1:nprocs-1)

    complex(dp) :: &
      mat

    complex(dp), external :: zdotu, zdotc
    !------------------------------------------------------!

    call start_clock ('epmat_band')
    if (master) then
      print *, "Computing epmat in band basis"
    endif

    allocate(evq_irr(3*na_uc), evq_star(3*na_uc))
    allocate(g2(nbnd,nbnd,nk))
    allocate(cnk(na_uc,nbnd,nk), ckq(na_uc,nbnd), ck(na_uc,nbnd))
    allocate(M_elec(3*na_uc, nbnd, nbnd, nk))
    inquire(iolength=recl_evq) evq_irr
    inquire(iolength=recl_epmat) g2

    !------------------------------------------------------!
    ! Read electron eigenvectors for irreducible k-points  ! 
    !------------------------------------------------------!
    call start_clock ('io_cnk')
    if (master) then
      do ik_irr = 1, nk
        write(fn, '(a,I0.6,a)') trim(data_tb)//'/cnk.', ik_irr, '.dat'
        call read_2d_cmplx(fn, na_uc, 1, na_uc, nbnd_tot, &
          lowest_band, highest_band, &
          cnk(1:na_uc, 1:nbnd, ik_irr))
      enddo
    endif
    call mpi_bcast(cnk, na_uc*nbnd*nk, &
      mpi_double_complex, 0, comm, mpierr)
    call stop_clock ('io_cnk')
    !------------------------------------------------------!

    if (start_task.le.0) start_task = 1
    if (end_task.le.0) end_task = nqmax
    ntask = end_task - start_task + 1

    if (master) then

      itask = start_task

      !------------------------------------------------------!
      ! Dispatch tasks
      !------------------------------------------------------!
      all_done = .false.
      procs_done = .false.
      do while (.not.all_done) 

        call mpi_recv(itask_prev, 1, mpi_integer, &
          MPI_ANY_SOURCE, 0, comm, MPI_STATUS, mpierr)

        itask_rank = MPI_STATUS(MPI_SOURCE)
        
        call mpi_send(itask, 1, mpi_integer, &
          itask_rank, itask_prev, comm, mpierr) 

        ! mark procs as done when itask>end_task is sent.
        if (itask.gt.end_task) then
          procs_done(itask_rank) = .true.
          print *, "rank ", itask_rank, " has done all the tasks!"
        else
          WRITE(timestr, "(a,i5,a,i5,a,i5)") &
            "Computing qpt # ", itask, '/', end_task, ' at rank ', itask_rank
          call timestamp (timestr)
        endif

        !------------------------------------------------------!
        ! Should we proceed to the next task?
        !------------------------------------------------------!
        if (itask.le.end_task) then
          itask = itask + 1
        else
          ! check all the procs are complete.
          all_done = .true.
          do iproc = 1, nprocs-1
            if (.not.procs_done(iproc)) then
              all_done = .false.
              print *, "rank ", iproc, " not done yet!"
              exit
            endif
          enddo
        endif
      enddo

    else !! rank!=0
      itask_prev = start_task-1 ! initial value for previous task 
      do while (.true.)
        !------------------------------------------------------!
        ! Request a new task                                   ! 
        !------------------------------------------------------!
        call mpi_send(itask_prev, 1, mpi_integer, 0, 0, comm, mpierr) 
        call mpi_recv(itask, 1, mpi_integer, 0, itask_prev, comm, mpi_status, mpierr)
        !------------------------------------------------------!

        ! Exit if there is no more task to be done 
        if (itask.gt.end_task) then
          exit
        endif

        !------------------------------------------------------!
        ! Handles a new task
        !------------------------------------------------------!
        iq_star = itask
        iq_irr = equiv(iq_star)
        iq_sym  = equiv_s(iq_star)

        write(fn, '(a,I0.6,a)') &
          trim(data_ph)//'/evq.', iq_irr, '.dat'
        open(IU_EVQ, file=trim(fn), form="unformatted", &
          status="old", access="direct", recl=recl_evq, action="read")

        write(fn_epmat, '(a,I0.6,a)') &
          trim(data_ep)//'/epmat.', iq_star, '.dat'
        open(IU_EPMAT, file=trim(fn_epmat), form="unformatted", &
          status="unknown", access="direct", recl=recl_epmat)

        call start_clock('melec')

        M_elec = 0.d0
        do ik_irr = 1, nk
          ik = get_ik(ik_irr)
          call get_kq(ik, iq_star, ikq, ikq_irr, ikq_sym)
          call rotate_cnk(ikq_irr, ikq_sym, &
            cnk(:,:,ikq_irr), ckq)
          ck = cnk(:,:,ik_irr)

          do ia_uc = 1, na_uc
            do ix = 1, 3
              do inb = 1, num_neighbors(ia_uc)
                ja_sc = neighbors(inb, ia_uc)
                ja_uc = sc_uc_map(ja_sc)
                jsc = isc_map(ja_sc)
                do jbnd = 1, nbnd
                  do ibnd = 1, nbnd
                    M_elec(3*(ia_uc-1)+ix,ibnd,jbnd,ik_irr) = &
                     M_elec(3*(ia_uc-1)+ix,ibnd,jbnd,ik_irr) &
                     + epmat_orb(inb,ix,ia_uc)*(phase_fac(jsc,ik)*conjg(ckq(ia_uc,jbnd)) * ck(ja_uc,ibnd) &
                                             + conjg(phase_fac(jsc,ikq)*ckq(ja_uc,jbnd)) * ck(ia_uc,ibnd))
                  enddo
                enddo
              enddo
            enddo
          enddo
        enddo ! ik

        call stop_clock('melec')

        call start_clock('g2')
        do i = 1, nmode
          g2 = 0.d0
          if (freq(i,iq_irr).gt.acoustic_cut) then
            read(IU_EVQ, rec=imodes(i)) evq_irr
            call rotate_evq(iq_irr, iq_sym, &
              evq_irr, evq_star)

            do ik_irr = 1, nk
              do jbnd = 1, nbnd
                do ibnd = 1, nbnd
                  mat = lqv(i,iq_irr)*ZDOTU(3*na_uc, evq_star, 1, M_elec(:,ibnd,jbnd,ik_irr), 1)
                  g2(ibnd,jbnd,ik_irr) = dble(dconjg(mat)*mat)
                enddo
              enddo
            enddo ! ik
          endif

          write(IU_EPMAT, rec=i) g2
        enddo ! i = 1, nmode
        call stop_clock('g2')

        close(IU_EPMAT)
        close(IU_EVQ)
        !------------------------------------------------------!

        itask_prev = itask
      enddo
    endif

    deallocate(evq_irr, evq_star)
    deallocate(g2,cnk,ckq,M_elec)
    call mpi_barrier(comm, mpierr)

    call stop_clock ('epmat_band') 
  end subroutine compute_epmat_band

  ! derivative of hopping function w.r.t. ix axis,
  ! evaluated at d
  real(dp) function dtdx ( d, ix ) 
    ! Slater-Koster tight-binding parameters
    real(DP), parameter :: Vpi  = -2.7d0/RYTOEV   , &
                           Vsig =  0.48d0/RYTOEV  , &
                           a0   =  2.46/sqrt(3.d0)*ANGSTROM_AU,  &
                           d0   =  3.34*ANGSTROM_AU, &
                           r0   =  0.184d0 * 2.46*ANGSTROM_AU

    integer, intent(in) :: ix
    real(dp), intent(in) :: d(3)
    real(dp) :: dist, fac1, fac2

    dist = norm2(d)

    !! Do not calculate duplicate parts
    if (ix.eq.1 .or. ix.eq.2) then

      fac1 = d(ix)*d(3)*d(3)/(r0*dist**3)
      fac2 = 2*d(ix)*d(3)*d(3)/dist**4

      dtdx = Vpi*exp(-(dist-a0)/r0)*( fac1 - d(ix)/(r0*dist) + fac2 ) &
             + Vsig*exp(-(dist-d0)/r0)*( -fac1 -fac2 ) 

    else
      fac1 = d(ix)*d(3)*d(3)/(r0*dist**3)
      fac2 = 2*d(3)*(dist**2-d(3)**2)/dist**4

      dtdx = Vpi*exp(-(dist-a0)/r0)*( fac1 - d(ix)/(r0*dist) - fac2 ) &
             + Vsig*exp(-(dist-d0)/r0)*( -fac1 + fac2 )
    endif

  end function dtdx

  integer function get_ik(ik_irr) result(ik)
    integer, intent(in) :: ik_irr
    integer :: ik1, ik2 
    real(dp) :: k_irr(3)
    k_irr = xk(:,ik_irr)

    ik1 = nint(k_irr(1)*dimk)
    ik2 = nint(k_irr(2)*dimk)
    ik = ik1*dimk+ik2+1
  end function get_ik

  subroutine get_kq(ik, iq, ikq, ikq_irr, ikq_sym)
    integer, intent(in) :: ik, iq
    integer, intent(out) :: ikq, ikq_irr, ikq_sym

    integer :: ik1, ik2, iq1, iq2, ikq1, ikq2
    real(dp) :: k_irr(3), k(3), q(3), kq(3)

    ik1 = int((ik-1)/dimk)
    ik2 = mod(ik-1, dimk)

    iq1 = int((iq-1)/dimk)
    iq2 = mod(iq-1, dimk)

    ikq1 = ik1+iq1
    ikq2 = ik2+iq2

    if (ikq1.ge.dimk) then
      ikq1 = ikq1 - dimk
    endif

    if (ikq2.ge.dimk) then
      ikq2 = ikq2 - dimk
    endif

    ikq = ikq1*dimk + ikq2 + 1
    ikq_irr = equiv(ikq)
    ikq_sym = equiv_s(ikq)
  end subroutine get_kq

  subroutine rotate_cnk(ik_irr, isym, cnk_irr, cnk_star)
    integer, intent(in) :: ik_irr, isym
    complex(dp), intent(in) :: cnk_irr(na_uc, nbnd)
    complex(dp), intent(out) :: cnk_star(na_uc, nbnd)

    real(dp) :: dx(3), k(3), arg
    complex(dp) :: cfac
    integer :: ia, sia, irot

    if (isym.eq.0) then
      cnk_star = cnk_irr
      return
    endif

    cnk_star = 0.d0
    irot = abs(isym)

    k = TPI*matmul(relatt, xk(:,ik_irr))

    do ia = 1, na_uc
      sia = irt(ia,irot)
      dx = matmul(transpose(sr(:,:,irot)), atoms_uc(:,sia)) &
        - atoms_uc(:,ia)
      arg = dot_product(k, dx)
      cfac = cmplx(cos(arg), sin(arg), kind=dp)

      if (isym.gt.0) then
        cnk_star(sia,:) = cfac*cnk_irr(ia,:)
      else
        cnk_star(sia,:) = conjg(cfac*cnk_irr(ia,:))
      endif
    enddo
  end subroutine rotate_cnk

  subroutine rotate_evq(iq_irr, isym, evq_irr, evq_star)
    integer, intent(in) :: iq_irr, isym
    complex(dp), intent(in) :: evq_irr(3*na_uc)
    complex(dp), intent(out) :: evq_star(3*na_uc)

    real(dp) :: dx(3), q(3), arg
    complex(dp) :: cfac
    integer :: ia, sia, irot

    if (isym.eq.0) then
      evq_star = evq_irr
      return
    endif

    evq_star = 0.d0
    irot = abs(isym)

    q = TPI*matmul(relatt, xk(:,iq_irr))

    do ia = 1, na_uc
      sia = irt(ia,irot)
      dx = matmul(transpose(sr(:,:,irot)), atoms_uc(:,sia)) &
        - atoms_uc(:,ia)
      arg = dot_product(q, dx)
      cfac = cmplx(cos(arg), sin(arg), kind=dp)

      if (isym.gt.0) then
        evq_star(3*(sia-1)+1:3*(sia-1)+3) = &
          cfac*matmul(sr(:,:,irot), evq_irr(3*(ia-1)+1:3*(ia-1)+3))
      else
        evq_star(3*(sia-1)+1:3*(sia-1)+3) = &
          conjg(cfac*matmul(sr(:,:,irot), evq_irr(3*(ia-1)+1:3*(ia-1)+3)))
      endif
    enddo
  end subroutine rotate_evq
end module epmat

