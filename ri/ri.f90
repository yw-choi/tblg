program ri

  use mpi
  use kinds, only: dp
  use input, only: read_input
  use constants, only: PI
  use lattice
  implicit none

  real(dp) :: at1(3), at2(3), dist, costh, th, s_AA, s_AB, s
  real(dp), allocatable :: s_CC(:), reg_idx(:)

  integer :: i, j, ia, m1, inb, ja_sc, task

  call mpi_initialize
  call timestamp('Start of run')

  call init_clocks ( .true. )
  call start_clock ( 'RI' ) 

  if (master) then
    print '(a)', repeat('=',60)
    print '(a,a,a)', '== ','Registry index calculation for twisted bilayer graphene',' =='
    print '(a)', repeat('=',60)

    print *, 'Running on ', nprocs, ' nodes'
    print '(a)', repeat('=',60)
  endif

  call read_input


  do task = 1, 2
    call setup_lattice(task)

    if (task.eq.1) allocate(s_CC(na_uc_l), reg_idx(na_uc_l))
    s_CC = 0.d0
    do ia = ia_offset+1, ia_offset+na_loc
      at1 = atoms_uc(:,ia,1)

      do inb = 1, num_inter_neighbors(ia,1)
        ja_sc = inter_neighbors(inb,ia,1)
        if (mod(ia,2).ne.mod(ja_sc,2)) cycle
        at2 = atoms_sc(:,ja_sc,2)

        ! lateral distance
        dist = norm2(at2(1:2)-at1(1:2)) 

        if (dist.gt.2*r_ri) then
          cycle
        endif

        costh = .5*dist/r_ri
        th = acos(costh)

        s = r_ri**2 * 2*th - dist*r_ri*sin(th)
        s_CC(ia) = s_CC(ia)+s
      enddo
    enddo

    call mpi_allgatherv(mpi_in_place, 0, mpi_datatype_null,&
                  s_CC, &
                  recvcounts, &
                  displs, &
                  mpi_double_precision, comm, mpierr)

    s_AA = PI*r_ri**2
    s_AB = 0
    reg_idx = (s_CC-s_AB)/(s_AA-s_AB)

    if (master) then
      if (task.eq.1) then
        open(11, file="reg_idx.1.dat")
      else
        open(11, file="reg_idx.2.dat")
      endif
      do ia = 1, na_uc_l
        write(11, "(3F12.6)") atoms_uc(1:2,ia,1), reg_idx(ia)
        write(6, "(I5,3F12.6)") ia, atoms_uc(1:2,ia,1), reg_idx(ia)
      enddo
      close(11)
    endif
  enddo


  call stop_ri
end program ri
