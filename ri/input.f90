module input

  use mpi
  use constants, only: RYTOEV, ANGSTROM_AU, AMU_RY
  use lattice, only: struct_fn, r_ri, alat
  implicit none
  public

contains

  subroutine read_input

    logical :: exst
    logical, external :: makedirqq

    call printblock_start ( "Input parameters" )

    !! &system
    alat = 2.46d0
    r_ri = 5.d0

    namelist/system/ struct_fn, alat, r_ri

    if (master) then
      read(5, system)
      write(6, system)

    endif

    call mpi_bcast(alat, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(r_ri, 1, mpi_double_precision, 0, comm, mpierr)

    call printblock_end

  end subroutine read_input

end module input
