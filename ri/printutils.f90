subroutine printblock_start(msg)
  use mpi

  character(len=*) :: msg
  integer :: i, lenmsg, left, right
  integer, parameter :: WIDTH=60

  if (master) then

    lenmsg = len(msg)
    write(6, '(a,a,a)', advance='no'), '== ', trim(msg), ' '
    do i=1,WIDTH-4-lenmsg
      write(6, '(a)', advance='no') '='
    enddo
    write(6, '(a)')
  endif

  call mpi_barrier(comm, mpierr)
end subroutine printblock_start

subroutine printblock_end()
  use mpi
  integer :: i
  integer, parameter :: WIDTH=60

  if (master) then
    do i = 1,WIDTH
      write(6, '(a)', advance='no') '='
    enddo
    write(6,'(a)')
    write(6, *)
  endif
  call mpi_barrier(comm, mpierr)
end subroutine printblock_end
