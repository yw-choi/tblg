module lattice
  use mpi
  use kinds, only: DP
  use constants, only: PI, eps6, ANGSTROM_AU
  use matrix_inversion, only: invmat
  use sort, only: hpsort_eps

  implicit none
  public

  real(dp) :: latt(3,3), relatt(3,3), r_ri
  integer :: na_uc, na_sc, na_uc_l, na_sc_l
  real(dp), allocatable :: &
    atoms_uc(:,:,:), & ! atoms_uc(3,na_uc_l,N_LAYER)
    atoms_sc(:,:,:)    ! atoms_sc(3,na_sc_l,N_LAYER)

  integer, allocatable :: &
    sc_uc_map(:,:), &    ! sc_uc_map(na_sc_l,N_LAYER) sc atom index to uc index
    isc_map(:,:),   &    ! isc_map(na_sc_l,N_LAYER) supercell index
    R_isc(:,:)           ! R_isc(3,nsc)   supercell Bravais lattice vector

  integer :: max_inter_neighbors ! max. number of neighbors
  integer, allocatable :: &
    num_inter_neighbors(:,:), & ! num_neighbors(na_uc_l, N_LAYER)
    inter_neighbors(:,:,:)      ! inter_neighbors(max_neighbors, na_uc_l, N_LAYER)

  integer, allocatable :: &
    intra_neighbors(:,:,:,:)    ! intra_neighbors(maxval(NA_SHELL), N_SHELL, na_uc_l, N_LAYER)

  integer :: lsc1, lsc2, nsc1, nsc2, nsc 

  character(len=100) :: struct_fn

  real(dp) :: alat

  integer, allocatable :: displs(:), recvcounts(:), ia_offset, na_loc
  integer, parameter :: N_LAYER = 2
  integer, parameter :: &
    N_INTRA_SHELL = 4, &
    NA_INTRA_SHELL(N_INTRA_SHELL) = (/ 3, 6, 3, 6 /)
  integer :: task_m
contains

  subroutine setup_lattice (task)
    integer, intent(in) :: task
    integer :: il, ia
    real(dp) :: dummy(3)

    task_m = task
    call start_clock('setup_lattice')
    call printblock_start ( "Crystal Structure" )

    if (master) then
      open(11, file=struct_fn, form="formatted", status="old")
      read(11,*) latt(:,1)
      read(11,*) latt(:,2)
      read(11,*) latt(:,3)
      read(11,*) na_uc

      na_uc_l = na_uc / 2

      if (task.eq.1) allocate(atoms_uc(3,na_uc_l,2))

      do il = 1, 2
        do ia = 1, na_uc_l
          if (task.eq.1) then
            read(11,*) atoms_uc(:,ia,il), dummy
          else
            read(11,*) dummy, atoms_uc(:,ia,il)
          endif
        enddo
      enddo
      
      close(11)
    endif


    call mpi_bcast(latt, 3*3, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(relatt, 3*3, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(na_uc, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(na_uc_l, 1, mpi_integer, 0, comm, mpierr)

    if (.not.master.and.task.eq.1) then
      allocate(atoms_uc(3, na_uc_l, N_LAYER))
    endif

    call mpi_bcast(atoms_uc, 3*na_uc_l*N_LAYER, mpi_double_precision, 0, comm, mpierr)


    if (task.eq.1) allocate(displs(0:nprocs-1), recvcounts(0:nprocs-1))
    call distribute_indices ( na_uc_l, nprocs, rank, displs, recvcounts )
    ia_offset = displs(rank)
    na_loc = recvcounts(rank)

    call construct_aux_supercell ( 10*r_ri )

    call setup_neighbors ( 10*r_ri )

    call printblock_end
    call stop_clock('setup_lattice')
  end subroutine setup_lattice

  subroutine construct_aux_supercell ( rcut )
    real(dp), intent(in) :: rcut

    integer :: i, stat, ia, ia_sc, i1, isc1, i2, isc2, isc 

    lsc1 = 1 + int(rcut / norm2(latt(1:3,1)) )
    lsc2 = lsc1 ! just in case they differ..
    nsc1 = 2*lsc1+1
    nsc2 = 2*lsc2+1
    nsc = nsc1*nsc2 ! total number of aux. supercells

    na_sc = na_uc * nsc
    na_sc_l = na_sc/N_LAYER

    if (task_m.eq.1) then
      allocate(atoms_sc(3, na_sc_l, N_LAYER))
      allocate(sc_uc_map(na_sc_l, N_LAYER), isc_map(na_sc_l, N_LAYER), R_isc(3,nsc))
    endif
    atoms_sc = 0.d0

    ia_sc = 0
    isc = 0
    do i1 = 0, nsc1-1
      if (i1.gt.lsc1) then
        isc1 = i1 - nsc1
      else
        isc1 = i1 
      endif

      do i2 = 0, nsc2-1
        if (i2.gt.lsc2) then
          isc2 = i2 - nsc2
        else
          isc2 = i2 
        endif

        isc = isc + 1

        R_isc(1:3,isc) = (/ isc1, isc2, 0 /)

        do ia = 1, na_uc_l
          ia_sc = ia_sc + 1
          sc_uc_map(ia_sc,:) = ia
          isc_map(ia_sc,:) = isc
          atoms_sc(1:3, ia_sc, 1) = matmul(latt(:,:), R_isc(1:3,isc)) &
                                 + atoms_uc(1:3, ia, 1)
          atoms_sc(1:3, ia_sc, 2) = matmul(latt(:,:), R_isc(1:3,isc)) &
                                 + atoms_uc(1:3, ia, 2)
        enddo
      enddo
    enddo

    if (master) then
      print '(1x,a,I3,a,I3)', "Aux. supercell = ", nsc1, ' x ', nsc2
      print *, 'Number of atoms in aux. supercell = ', na_sc
    endif

    call mpi_barrier ( comm, mpierr )

  end subroutine construct_aux_supercell

  subroutine setup_neighbors ( rcut )
    real(DP), intent(in) :: rcut

    integer :: ia_uc, il, ja_sc, ja_uc, ishell, ia_shell, il2
    real(dp) :: dist(na_sc_l), dx(3)
    integer :: ind(na_sc_l)

    integer :: nnb_inter, nnb_intra

    if (task_m.eq.1) then
      allocate(intra_neighbors(maxval(NA_INTRA_SHELL), N_INTRA_SHELL, na_uc_l, N_LAYER))
    endif

    !! intra
    do il = 1, N_LAYER
      do ia_uc = ia_offset+1, ia_offset+na_loc
        do ja_sc = 1, na_sc_l
          dist(ja_sc) = norm2(atoms_sc(:,ja_sc,il)-atoms_uc(:,ia_uc,il))
        enddo

        ind(1) = 0
        call hpsort_eps( na_sc_l, dist, ind, 0.0001d0)

        nnb_intra = 0
        do ishell = 1, N_INTRA_SHELL
          do ia_shell = 1, NA_INTRA_SHELL(ishell)
            nnb_intra = nnb_intra + 1
            intra_neighbors(ia_shell, ishell, ia_uc, il) = ind(nnb_intra+1) ! except the same atom
          enddo
        enddo
      enddo

      call mpi_allgatherv(mpi_in_place, 0, mpi_datatype_null,&
                    intra_neighbors(:,:,:,il), &
                    maxval(NA_INTRA_SHELL)*N_INTRA_SHELL*recvcounts, &
                    maxval(NA_INTRA_SHELL)*N_INTRA_SHELL*displs, &
                    mpi_integer, comm, mpierr)
    enddo

    if (task_m.eq.1) allocate(num_inter_neighbors(na_uc_l,N_LAYER))

    !! inter
    num_inter_neighbors = 0
    do il = 1, N_LAYER
      il2 = 3 - il

      do ia_uc = ia_offset+1, ia_offset+na_loc
        do ja_sc = 1, na_sc_l
          dist(ja_sc) = norm2(atoms_sc(:,ja_sc,il2)-atoms_uc(:,ia_uc,il))

          if (dist(ja_sc).lt.rcut) then

            num_inter_neighbors(ia_uc, il) = num_inter_neighbors(ia_uc, il) + 1

          endif

        enddo

      enddo
      call mpi_allgatherv(mpi_in_place, 0, mpi_datatype_null,&
                    num_inter_neighbors(:,il), &
                    recvcounts, &
                    displs, &
                    mpi_integer, comm, mpierr)
    enddo

    max_inter_neighbors = maxval(num_inter_neighbors)

    if (task_m.eq.2) deallocate(inter_neighbors)
    allocate(inter_neighbors(max_inter_neighbors,na_uc_l,N_LAYER))
    do il = 1, N_LAYER
      il2 = 3 - il

      do ia_uc = ia_offset+1, ia_offset+na_loc
        nnb_inter = 0
        do ja_sc = 1, na_sc_l
          dist(ja_sc) = norm2(atoms_sc(:,ja_sc,il2)-atoms_uc(:,ia_uc,il))

          if (dist(ja_sc).lt.rcut) then
            nnb_inter = nnb_inter + 1

            inter_neighbors(nnb_inter,ia_uc,il) = ja_sc

          endif

        enddo

      enddo
      call mpi_allgatherv(mpi_in_place, 0, mpi_datatype_null,&
                    inter_neighbors(:,:,il), &
                    max_inter_neighbors*recvcounts, &
                    max_inter_neighbors*displs, &
                    mpi_integer, comm, mpierr)
    enddo

  end subroutine setup_neighbors

  subroutine write_orb_idx

    integer :: ia_sc, il, inb, ishell, ia_shell, ia_uc
    character(len=100) :: fn

    if (master) then
      write(fn, '(a)') 'orb_idx.dat'
      open(11, file=trim(fn), form="formatted")

      write(11, '(7A7)') &
        'il', & 
        'ia_sc', & 
        'ia_uc', & 
        'isc', & 
        'n1', & 
        'n2', & 
        'n3'
      do il = 1, N_LAYER
        do ia_sc = 1, na_sc_l

          write(11,'(7I7)') &
            il, &
            ia_sc, &
            sc_uc_map(ia_sc,il), &
            isc_map(ia_sc,il), &
            R_isc(1,isc_map(ia_sc,il)), &
            R_isc(2,isc_map(ia_sc,il)), &
            R_isc(3,isc_map(ia_sc,il))
        enddo
      enddo

      close(11)

      write(fn, '(a)') 'intra_neighbors.dat'
      open(11, file=trim(fn), form="formatted")

      write(11, '(6A7)') &
        'il', & 
        'ia_uc', & 
        'ishell', & 
        'ia_shell', & 
        'ja_sc', & 
        'ja_uc'
      do il = 1, N_LAYER
        do ia_uc = 1, na_uc_l

          do ishell = 1, N_INTRA_SHELL
            do ia_shell = 1, NA_INTRA_SHELL(ishell)

              write(11,'(6I7)') &
                il, &
                ia_uc, &
                ishell, &
                ia_shell, &
                intra_neighbors(ia_shell, ishell, ia_uc, il), &
                sc_uc_map(intra_neighbors(ia_shell, ishell, ia_uc, il),il)

            enddo
          enddo
        enddo
      enddo

      close(11)

      write(fn, '(a)') 'inter_neighbors.dat'
      open(11, file=trim(fn), form="formatted")

      write(11, '(6A7)') &
        'il', & 
        'ia_uc', & 
        'inb', & 
        'il2', & 
        'ja_sc', & 
        'ja_uc'
      do il = 1, N_LAYER
        do ia_uc = 1, na_uc_l

          do inb = 1, num_inter_neighbors(ia_uc, il)
            write(11,'(6I7)') &
              il, &
              ia_uc, &
              inb, &
              3-il, &
              inter_neighbors(inb, ia_uc, il), &
              sc_uc_map(inter_neighbors(inb, ia_uc, il), 3-il)
          enddo
        enddo
      enddo

      close(11)
    endif
    call mpi_barrier(comm, mpierr)
  end subroutine write_orb_idx
end module lattice
