module input

  use mpi
  use constants, only: RYTOEV, ANGSTROM_AU, AMU_RY, ry_to_kelvin
  use env, only: data_ph, data_tb, data_ep
  use lattice, only: struct_fn
  use epmat, only: nk1, nk2, nk3, nk, nq1, nq2, nq3, nq, &
                   lowest_band, highest_band, nband_tot, nband, read_epmat, &
                   acoustic_cut, rcut_tb, mass, imodes_fn

  use ep_selfen, only: &
    l_ep_selfen, &
    e_f, temperature, degaussw

  implicit none
  public

contains

  subroutine read_input

    logical :: exst
    logical, external :: makedirqq

    call printblock_start ( "Input parameters" )

    !! &input
    data_ph = './data'
    data_tb = './data'
    data_ep = './data'
    struct_fn = 'atoms.in'

    rcut_tb = 5.d0 * ANGSTROM_AU
    mass = 12.0107 * AMU_RY
    acoustic_cut = 1.d-6 / RYTOEV
    lowest_band = -1
    highest_band = -1

    l_ep_selfen = .false.
    e_f = 0.d0
    temperature = 20e-3 / 13.605

    imodes_fn = ''
    degaussw = 1d-3/RYTOEV

    namelist/input/ data_ph, data_tb, data_ep, struct_fn, rcut_tb, &
                    nk1, nk2, nk3, &
                    nq1, nq2, nq3, mass, read_epmat, &
                    acoustic_cut, nband_tot, lowest_band, highest_band, &
                    e_f, l_ep_selfen, temperature, imodes_fn, degaussw

    if (master) then
      rewind 5
      read(5, input)
      write(6, input)

      inquire(directory=trim(data_ep), exist=exst)

      if (.not.exst) then
        if (.not.makedirqq(data_ep)) then
          call errore('read_input', 'Failed to create data directory', 1)
        endif
      endif

      mass = mass * AMU_RY
      rcut_tb = rcut_tb * ANGSTROM_AU
      acoustic_cut = acoustic_cut / RYTOEV

      e_f = e_f / RYTOEV
      temperature = temperature / ry_to_kelvin
      degaussw = degaussw / RYTOEV
      print *, 'temperature (Ry) = ', temperature
      print *, 'acoustic_cut (Ry) = ', acoustic_cut
    endif

    call mpi_bcast(data_ep, 100, mpi_character, 0, comm, mpierr)
    call mpi_bcast(data_tb, 100, mpi_character, 0, comm, mpierr)
    call mpi_bcast(data_ph, 100, mpi_character, 0, comm, mpierr)
    call mpi_bcast(struct_fn, 100, mpi_character, 0, comm, mpierr)
    call mpi_bcast(imodes_fn, 100, mpi_character, 0, comm, mpierr)
    call mpi_bcast(rcut_tb, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(nband_tot, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(nk1, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(nk2, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(nk3, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(lowest_band, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(highest_band, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(nq1, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(nq2, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(nq3, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(mass, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(read_epmat, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(acoustic_cut, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(e_f, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(degaussw, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(l_ep_selfen, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(temperature, 1, mpi_double_precision, 0, comm, mpierr)

    nk = nk1*nk2*nk3
    nq = nq1*nq2*nq3

    !! band indices
    if (lowest_band.ge.1.and.highest_band.ge.1 & 
        .and.lowest_band.gt.highest_band) then
      call errore('input', 'lb should be greater than hb', 1)
    endif

    call printblock_end

  end subroutine read_input

end module input
