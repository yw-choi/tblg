module ep_selfen

  use mpi
  use kinds, only: dp
  use env, only: data_ep
  use constants, only: TPI, RYTOEV, eps12, PI, eps6

  use epmat, only: &
    nk1, nk2, nk3, nk, kpoints, kpoints_cart, &
    nq1, nq2, nq3, nq, qpoints, qpoints_cart, &
    nband, enk, &
    nmode_tot, nmode, imodes, freq, &
    nph, recvcounts_nph, displs_nph, &
    iph_offset, nph_loc, kqmap, acoustic_cut

  use epmat_io, only: &
    io_epmat, IU_EPMAT

  use io, only: &
    io_2d_real_fmt
  implicit none

  logical :: l_ep_selfen

  real(dp) :: &
    e_f, & ! Fermi energy (Ry)
    degaussw, & 
    N_F, & ! DOS per spin at the Fermi energy (states/spin/Ry)
    temperature ! Temperature (Ry)

  real(dp), allocatable :: lambda_nk(:,:), gamma_qv(:,:,:), sigma_r(:,:), sigma_i(:,:)

  real(dp), allocatable :: nqv(:,:), fnk(:,:)

  real(dp), allocatable :: &
    wnk(:,:), & ! wnk(nband,nk) tetrahedron weights
    dwnk(:,:)   ! dwnk(nband,nk) d/dE wnk (DOS weights)

contains

  subroutine ep_selfen_main
    integer :: iq, reclen
    character(len=100) :: epmat_fn
    complex(dp), allocatable :: tmp(:,:,:)

    call start_clock ('ep_selfen')

    !! open epmat files 
    allocate(tmp(nband,nband,nk))
    inquire(iolength=reclen) tmp
    do iq = 1, nq
      write(epmat_fn, '(a,I0.4,a)') trim(data_ep)//'/epmat.', iq, '.dat'
      open(IU_EPMAT+iq, file=trim(epmat_fn), &
           form="unformatted", status="unknown", access="direct", recl=reclen, action="read")
    enddo
    call mpi_barrier(comm, mpierr)

    call compute_dos_weights
    call compute_occ
    call compute_selfen
    call compute_gamma_qv

    do iq = 1, nq
      close(IU_EPMAT+iq)
    enddo
    call mpi_barrier(comm, mpierr)

    call stop_clock ('ep_selfen')
  end subroutine ep_selfen_main

  subroutine compute_occ
    integer :: imode, ibnd, ik, iq

    real(dp), external :: wgauss, w0gauss

    allocate(nqv(nmode_tot,nq), fnk(nband,nk))

    do ik = 1, nk
      do ibnd = 1, nband
        fnk(ibnd, ik) = wgauss(-(enk(ibnd,ik)-e_f)/temperature, -99)
      enddo
    enddo

    do iq = 1, nq
      do imode = 1, nmode_tot
        nqv(imode,iq) = wgauss(-freq(imode,iq)/temperature, -99)
        nqv(imode,iq) = nqv(imode,iq)/(1.d0-2.d0*nqv(imode,iq))
      enddo
    enddo
  end subroutine compute_occ

  subroutine compute_selfen
    integer :: iph_loc, iph, ibnd, jbnd, ik, iq, imode, i
    complex(dp), allocatable :: epmat(:,:,:)
    integer :: ikq, kq_G(3)
    character(len=100) :: timestr, fn
    real(dp) :: wgkq, wgq, ekk, ekq, wq, one, two, g2, weight, w0g1, w0g2, &
                tmp, tmp2, tmp3
    complex(dp) :: ci
    integer :: n
    real(dp), allocatable :: sigmar_tmp(:), sigmai_tmp(:), lambda_tmp(:)
    real(dp), external :: w0gauss

    call start_clock('compute_selfen')
    call printblock_start("Computing lambda_k")

    allocate(lambda_nk(nband,nk),epmat(nband,nband,nk),sigma_r(nband,nk),sigma_i(nband,nk), &
      sigmar_tmp(nband), sigmai_tmp(nband), lambda_tmp(nband))

    lambda_nk = 0.d0
    sigma_r = 0.d0
    sigma_i = 0.d0
    one = 1.d0
    two = 2.d0
    ci = cmplx(0.0d0, one)

    do iph_loc = 1, nph_loc
      iph = iph_offset + iph_loc
      WRITE(TIMESTR, "(a,i5,a,i5)") "Computing iph_loc # ", iph_loc, '/', nph_loc
      call timestamp (timestr)
      i = mod(iph-1, nmode) + 1
      imode = imodes(i)
      iq    = (iph-1)/nmode + 1
      
      call io_epmat('read', iq, imode, nband, nk, epmat)

      if (freq(imode, iq).lt.acoustic_cut) then
        cycle
      endif

      epmat = dconjg(epmat)*epmat

      do ik = 1, nk
        call kqmap(kpoints(:,ik), qpoints(:,iq), ikq, kq_G)

        do ibnd = 1, nband
          do jbnd = 1, nband

            g2 = dble(epmat(ibnd,jbnd,ik))
            ekk = enk(ibnd,ik)-e_f
            ekq = enk(jbnd,ikq)-e_f
            wq  = freq(imode,iq)
            wgkq = fnk(jbnd,ikq)
            wgq = nqv(imode,iq)

            weight = 1.0d0/nq * real (                                                   &
                    ( (       wgkq + wgq ) / ( ekk - ( ekq - wq ) - ci * degaussw )  +  &
                      ( one - wgkq + wgq ) / ( ekk - ( ekq + wq ) - ci * degaussw ) ) )

            sigma_r(ibnd,ik) = sigma_r(ibnd,ik) + g2 * weight

            w0g1=w0gauss( (ekk-ekq+wq)/degaussw, 0) /degaussw
            w0g2=w0gauss( (ekk-ekq-wq)/degaussw, 0) /degaussw
            weight = pi * 1.0d0/nq * ( (wgkq+wgq)*w0g1 + (one-wgkq+wgq)*w0g2 )
            !
            sigma_i(ibnd,ik) = sigma_i(ibnd,ik) + g2 * weight

            lambda_nk(ibnd,ik) = lambda_nk(ibnd,ik) &
              + 1.0d0/nq*g2 * &
               ( (       wgkq + wgq ) * ( (ekk - ( ekq - wq ))**two - degaussw**two ) /       &
                                        ( (ekk - ( ekq - wq ))**two + degaussw**two )**two +  &
                 ( one - wgkq + wgq ) * ( (ekk - ( ekq + wq ))**two - degaussw**two ) /       &
                                        ( (ekk - ( ekq + wq ))**two + degaussw**two )**two )  
            ! lambda_nk(ibnd,ik) = lambda_nk(ibnd,ik) &
            !   + dble(epmat(ibnd,jbnd,ik)) &
            !   * ( (1-fnk(jbnd,ikq))/(e_f-enk(jbnd,ikq)-freq(imode,iq))**2 &
            !     + (  fnk(jbnd,ikq))/(e_f-enk(jbnd,ikq)+freq(imode,iq))**2 ) 
          enddo
        enddo
      enddo
    enddo

    call mpi_allreduce(mpi_in_place, sigma_i, nband*nk, mpi_double_precision, mpi_sum, comm, mpierr)
    call mpi_allreduce(mpi_in_place, sigma_r, nband*nk, mpi_double_precision, mpi_sum, comm, mpierr)
    call mpi_allreduce(mpi_in_place, lambda_nk, nband*nk, mpi_double_precision, mpi_sum, comm, mpierr)
    
    sigmar_tmp = 0.d0
    sigmai_tmp = 0.d0
    lambda_tmp = 0.d0
    do ik = 1, nk
      do ibnd = 1, nband
        n = 0
        tmp = 0.d0
        tmp2 = 0.d0
        tmp3 = 0.d0
        do jbnd = 1, nband
          if (abs(enk(ibnd,ik)-enk(jbnd,ik)).lt.eps6) then
            n = n + 1
            tmp = tmp + sigma_r(jbnd,ik)
            tmp2 = tmp2 + sigma_i(jbnd,ik)
            tmp3 = tmp3 + lambda_nk(jbnd,ik)
          endif
        enddo

        sigmar_tmp(ibnd) = tmp / float(n)
        sigmai_tmp(ibnd) = tmp2 / float(n)
        lambda_tmp(ibnd) = tmp3 / float(n)
      enddo

      sigma_r(:,ik) = sigmar_tmp
      sigma_i(:,ik) = sigmai_tmp
      lambda_nk(:,ik) = lambda_tmp
    enddo


    if (master) then
      write(fn,'(a)') 'lambda_nk.dat'
      call io_2d_real_fmt('write', fn, nband, nk, lambda_nk)
      write(fn,'(a)') 'sigma_r.dat'
      call io_2d_real_fmt('write', fn, nband, nk, sigma_r)
      write(fn,'(a)') 'sigma_i.dat'
      call io_2d_real_fmt('write', fn, nband, nk, sigma_i)
    endif

    call printblock_end
    call stop_clock('compute_selfen')

  end subroutine compute_selfen

  subroutine compute_gamma_qv
    integer :: iph_loc, iph, ibnd, jbnd, ik, iq, imode, i
    complex(dp), allocatable :: epmat(:,:,:)
    integer :: ikq, kq_G(3)
    character(len=100) :: timestr, fn

    integer :: nsigma, isig
    real(dp), allocatable :: sigma(:)
    real(dp) :: fac
    real(dp), external :: wgauss, w0gauss

    call start_clock('compute_gqv')
    call printblock_start("Computing gamma_qv")

    nsigma = 5
    allocate(gamma_qv(nmode,nq,nsigma),epmat(nband,nband,nk))
    allocate(sigma(nsigma))
    sigma(1) =  1d-3 / RYTOEV
    sigma(2) =  2d-3 / RYTOEV
    sigma(3) =  4d-3 / RYTOEV
    sigma(4) =  8d-3 / RYTOEV
    sigma(5) = 15d-3 / RYTOEV

    gamma_qv = 0.d0

    do iph_loc = 1, nph_loc
      iph = iph_offset + iph_loc
      WRITE(TIMESTR, "(a,i5,a,i5)") "Computing iph_loc # ", iph_loc, '/', nph_loc
      call timestamp (timestr)
      i = mod(iph-1, nmode) + 1
      imode = imodes(i)
      iq    = (iph-1)/nmode + 1
      
      call io_epmat('read', iq, imode, nband, nk, epmat)

      if (freq(imode, iq).lt.acoustic_cut) then
        cycle
      endif

      epmat = dconjg(epmat)*epmat

      do ik = 1, nk
        call kqmap(kpoints(:,ik), qpoints(:,iq), ikq, kq_G)

        do ibnd = 1, nband
          do jbnd = 1, nband
            fac = dble(epmat(ibnd,jbnd,ik))*(fnk(ibnd,ik)-fnk(jbnd,ikq))
            do isig = 1, nsigma

              gamma_qv(i,iq,isig) = gamma_qv(i,iq,isig)+fac*w0gauss(((enk(jbnd,ikq)-enk(ibnd,ik)-freq(imode,iq))/sigma(isig)),0)/sigma(isig)

            enddo
          enddo
        enddo
      enddo
    enddo

    call mpi_allreduce(mpi_in_place, gamma_qv, nmode*nq, mpi_double_precision, mpi_sum, comm, mpierr)

    gamma_qv = 4*PI*gamma_qv / nq

    if (master) then
      do isig = 1, nsigma
        write(fn,'(a,I1,a)') 'gamma_qv.',isig,'.dat'
        call io_2d_real_fmt('write', fn, nmode, nq, gamma_qv(:,:,isig))
      enddo
    endif

    call printblock_end
    call stop_clock('compute_gqv')

  end subroutine compute_gamma_qv

  ! Calculates energy derivative of linear tetrahedron weights.
  ! Currently dwnk is computed from the subroutine written by H. J. Choi.
  ! Note that dwnk is calculated within the linear tetrahedron method,
  ! not including Bloechl's correction.
  subroutine compute_dos_weights 
    real(dp) :: ik1(nk), ik2(nk), ik3(nk), ck1(3), ck2(3), ck3(3), &
                ak1, ak2, ak3, x1, x2, x3
    integer :: i, ik
    character(len=100) :: fn
    integer :: ie, ie_loc
    integer :: recvcounts(0:nprocs-1), displs(0:nprocs-1), ie_offset, ne_loc

    call start_clock('wnk')
    call printblock_start("Computing DOS weights")

    do i = 1, nk
      ak1 = kpoints(1,ik)
      ak2 = kpoints(2,ik)
      ak3 = kpoints(3,ik)
      x1 = kpoints_cart(1,ik)*TPI
      x2 = kpoints_cart(2,ik)*TPI
      x3 = kpoints_cart(3,ik)*TPI
      if(ak1.lt.0)ak1 = ak1 + 1.0D0
      if(ak2.lt.0)ak2 = ak2 + 1.0D0
      if(ak3.lt.0)ak3 = ak3 + 1.0D0
      ik1(i) = ak1*nk1+0.5
      ik2(i) = ak2*nk2+0.5
      ik3(i) = ak3*nk3+0.5
      if(ik1(i).eq.1.and.ik2(i).eq.0.and.ik3(i).eq.0) then
         ck1(1) = x1 
         ck1(2) = x2 
         ck1(3) = x3 
      elseif(ik1(i).eq.0.and.ik2(i).eq.1.and.ik3(i).eq.0) then
         ck2(1) = x1 
         ck2(2) = x2 
         ck2(3) = x3 
      elseif(ik1(i).eq.0.and.ik2(i).eq.0.and.ik3(i).eq.1) then
         ck3(1) = x1 
         ck3(2) = x2 
         ck3(3) = x3 
      endif
    enddo

    allocate(wnk(nband,nk), dwnk(nband,nk))

    ! calculation of weight, dwnk at e_f
    call cal_DOS(nband,nk,ck1,ck2,ck3,enk,e_f, &
                  nk1,nk2,nk3,wnk,dwnk,1,N_F)

    ! per spin
    N_F = N_F * .5d0
    wnk = wnk * .5d0 
    if (N_F.lt.eps12) then
      dwnk = 0.d0
    else
      dwnk = dwnk * .5d0 / N_F  
    endif

    if (master) then
      print *, "Fermi energy (eV) = ", E_F * RYTOEV
      print *, "DOS per spin at E_F (states/spin/eV) = ", N_F/RYTOEV
      print *, "2*sum(wnk) = ", 2*sum(wnk)
      print *, "sum(dwnk) = ", sum(dwnk)
    endif

    if (master) then
      write(fn,'(a)') 'dwnk.dat'
      call io_2d_real_fmt('write', fn, nband, nk, dwnk)
    endif

    call printblock_end
    call stop_clock('wnk')
  end subroutine compute_dos_weights

end module ep_selfen
