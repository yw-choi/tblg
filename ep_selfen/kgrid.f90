module kgrid

  use kinds, only: DP

  implicit none
  public

contains

  subroutine generate_uniform_grid ( nk1, nk2, nk3, kpoints_frac )
    integer, intent(in) :: nk1, nk2, nk3
    real(dp), intent(out) :: kpoints_frac(3,nk1*nk2*nk3)

    integer :: ik1, ik2, ik3, ik

    ik = 0
    do ik1 = 0, nk1-1
      do ik2 = 0, nk2-1
        do ik3 = 0, nk3-1
          ik = ik + 1
          kpoints_frac(1, ik) = dble(ik1)/nk1
          kpoints_frac(2, ik) = dble(ik2)/nk2
          kpoints_frac(3, ik) = dble(ik3)/nk3
        enddo
      enddo
    enddo

  end subroutine generate_uniform_grid 

  subroutine read_kpoints_band ( fn, nk_band, kpoints_band )
    character(len=100) :: fn
    integer, intent(out) :: nk_band
    real(dp), allocatable, intent(out) :: kpoints_band(:,:)

    integer :: ik 

    open(11, file=trim(fn), form="formatted", status="old")

    read(11,*) nk_band

    allocate(kpoints_band(3,nk_band))

    do ik = 1, nk_band
      read(11,*) kpoints_band(:,ik)
    enddo
    close(11)

  end subroutine read_kpoints_band
end module kgrid
