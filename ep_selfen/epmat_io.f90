module epmat_io
  use mpi
  use constants, only: RYTOEV, eps6, eps12, eps14, eps16, PI
  use kinds, only: DP
  use env, only: data_ep

  implicit none
  public

  integer, parameter :: IU_EPMAT = 1000

  character(len=100) :: epmat_fn
contains

  subroutine io_epmat( task, iq, imode, nbnd, nks, epmat )
    character(len=*), intent(in) :: task ! 'read' or 'write'
    integer, intent(in) :: iq, imode, nbnd, nks

    complex(dp), intent(inout) :: epmat(nbnd,nbnd,nks)
    integer :: ios, iph_read

    if (trim(task).eq.'read') then
      read(IU_EPMAT+iq, rec=imode, iostat=ios) epmat
      if (ios.ne.0) then
        print *, rank, iq, imode, ios
        call errore('io_epmat', 'reading failed', ios)
      endif
    else if (trim(task).eq.'write') then
      write(IU_EPMAT+iq, rec=imode, iostat=ios) epmat
      if (ios.ne.0) then
        print *, rank, iq, imode, ios
        call errore('io_epmat', 'writing failed', ios)
      endif
    else
      call errore('io_epmat', 'invalid task', 1)
    endif

  end subroutine io_epmat

end module epmat_io
