module io
  use mpi
  use kinds, only: DP
  use constants, only: RYTOEV, ANGSTROM_AU
  implicit none
  public

contains

  subroutine read_2d_real_fmt ( fn, n1, l1, h1, n2, l2, h2, arr )
    character(len=*), intent(in) :: fn
    integer, intent(in) :: n1, l1, h1, n2, l2, h2
    real(dp), intent(out) :: arr(h1-l1+1, h2-l2+1)

    character(len=100) :: dummy
    integer :: n1_read, n2_read, i1, i2
    logical :: exst

    real(dp) :: dat(n1)

    inquire(file=trim(fn), exist=exst)
    
    if (.not.exst) then
      call errore('io_2d_real_fmt', trim(fn)//' not found', 1)
    endif

    open(11, file=trim(fn), form="formatted", status="old")

    read(11, *) dummy, n1_read, n2_read

    do i2 = 1, n2
      if (l2.le.i2.and.i2.le.h2) then
        read(11,*) dat
        arr(1:h1-l1+1, i2-l2+1) = dat(l1:h1)
      else
        read(11,*) dat
      endif
    enddo

    close(11)
  end subroutine read_2d_real_fmt

  subroutine read_2d_cmplx ( fn, n1, l1, h1, n2, l2, h2, arr )
    character(len=*), intent(in) :: fn
    integer, intent(in) :: n1, l1, h1, n2, l2, h2
    complex(dp), intent(out) :: arr(h1-l1+1, h2-l2+1)

    character(len=100) :: dummy
    integer :: n1_read, n2_read, i1, i2, reclen
    logical :: exst

    complex(dp) :: dat(n1)

    inquire(iolength=reclen) dat(1:n1)
    inquire(file=trim(fn), exist=exst)
    
    if (.not.exst) then
      call errore('read_2d_cmplx', trim(fn)//' not found', 1)
    endif

      open(11, file=trim(fn), form="unformatted", status="old", &
           access="direct", recl=reclen)

    do i2 = l2, h2
      read(11, rec=i2) dat
      arr(1:h1-l1+1, i2-l2+1) = dat(l1:h1)
    enddo

    close(11)
  end subroutine read_2d_cmplx

  subroutine read_2d_cmplx_rec ( fn, n1, l1, h1, i2, arr )
    character(len=*), intent(in) :: fn
    integer, intent(in) :: n1, l1, h1, i2
    complex(dp), intent(out) :: arr(h1-l1+1)

    character(len=100) :: dummy
    integer :: n1_read, n2_read, i1, reclen
    logical :: exst

    complex(dp) :: dat(n1)

    inquire(iolength=reclen) dat(1:n1)
    inquire(file=trim(fn), exist=exst)
    
    if (.not.exst) then
      call errore('read_2d_cmplx_rec', trim(fn)//' not found', 1)
    endif

    open(11, file=trim(fn), form="unformatted", status="old", &
         access="direct", recl=reclen)

    read(11, rec=i2) dat
    arr(1:h1-l1+1) = dat(l1:h1)

    close(11)

  end subroutine read_2d_cmplx_rec

  subroutine io_2d_real_fmt ( task, fn, n1, n2, arr )
    character(len=*), intent(in) :: task, & ! 'read' or 'write'
                                    fn
    integer, intent(inout) :: n1, n2
    real(dp), intent(inout) :: arr(n1,n2)

    character(len=100) :: dummy
    integer :: n1_read, n2_read, i1, i2
    logical :: exst

    if (trim(task).eq.'read') then
      inquire(file=trim(fn), exist=exst)
      
      if (.not.exst) then
        call errore('io_2d_real_fmt', trim(fn)//' not found', 1)
      endif

      open(11, file=trim(fn), form="formatted", status="old")

      read(11, *) dummy, n1_read, n2_read

      do i2 = 1, n2
        read(11,*) arr(:,i2)
      enddo

      close(11)
      
    else if (trim(task).eq.'write') then
      open(11, file=trim(fn), form='formatted')
      write(11,'(a,2I)') "#", n1, n2
      do i2=1,n2
        do i1=1,n1
            write(11, '(F20.12,1x)', advance='no') arr(i1,i2) 
        enddo
        write(11, '(a)')
      enddo
      close(11)
    else
      call errore('io_2d_real_fmt', 'invalid task', 1)
    endif

  end subroutine io_2d_real_fmt

end module io
