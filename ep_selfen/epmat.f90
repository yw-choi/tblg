module epmat

  use mpi
  use constants, only: &
    RYTOEV, eps6, eps12, eps14, eps16, PI, ANGSTROM_AU

  use kinds, only: DP

  use env, only: data_ep, data_ph, data_tb

  use lattice, only: &
    latt, relatt, &
    na_uc, atoms_uc, &
    na_sc, atoms_sc, &
    sc_uc_map, neighbors, num_neighbors, max_neighbors, &
    nsc, R_isc, isc_map

  use io, only: &
    read_2d_real_fmt, &
    read_2d_cmplx_rec, &
    read_2d_cmplx

  use kgrid, only: &
    generate_uniform_grid

  use epmat_io, only: &
    io_epmat, &
    IU_EPMAT

  implicit none
  public

  logical :: read_epmat

  integer :: nk1, nk2, nk3, nk, nq1, nq2, nq3, nq, nmode_tot, nmode

  integer :: &
    lowest_band, &  ! lowest band index to calculate epmat
    highest_band, & ! highest band index to calculate epmat
    nband_tot, &    ! total number of bands as calculated by tb.x
    nband           ! highest_band-lowest_band+1

  real(dp), allocatable :: &
    kpoints(:,:), &       ! k-points in frac. cooridnate
    kpoints_cart(:,:), &  ! k-points in cart. coordinate (2pi/bohr unit)
    qpoints(:,:), &       ! q-points in frac. cooridnate
    qpoints_cart(:,:)     ! q-points in cart. coordinate (2pi/bohr unit)

  real(dp) ::  &
    rcut_tb,   & ! cutoff for the hopping derivatives
    mass         ! carbon mass (Ry)

  real(dp), allocatable ::  &
    enk(:,:),  & ! enk(nband,nk) electron energies (Ry)
    freq(:,:), & ! freq(nmode_tot,nq) phonon frequencies (Ry)
    lqv(:,:)     ! lqv(nmode_tot,nq) phonon length scale

  complex(DP), allocatable :: &
    phase_fac(:,:)        
                         
  real(dp), allocatable :: &
    epmat_orb(:,:,:)  ! epmat_orb(3, max_neighbors, na_uc)
                      !  el-ph matrix elements in orbital basis
  
  integer :: nph, iph_offset, nph_loc
  integer, allocatable :: recvcounts_nph(:), displs_nph(:)

  real(dp) :: &
    acoustic_cut

  character(len=100) :: &
    imodes_fn 
  integer, allocatable :: imodes(:)
contains 

  subroutine read_imodes
    integer :: imode, i
    
    if (trim(imodes_fn).ne.'') then
      if (master) then
        open(11, file=trim(imodes_fn), form="formatted")
        read(11,*) nmode
        allocate(imodes(nmode))
        do imode = 1, nmode
          read(11,*) imodes(imode)
        enddo
        close(11)
      endif

      call mpi_bcast(nmode, 1, mpi_integer, 0, comm, mpierr)

      if (.not.master) then
        allocate(imodes(nmode))
      endif
      call mpi_bcast(imodes, nmode, mpi_integer, 0, comm, mpierr)
    else
      nmode = nmode_tot
      allocate(imodes(nmode))
      do imode = 1, nmode
        imodes(imode) = imode
      enddo
    endif
    
    nph = nq*nmode

    !! Distribution of q-points
    allocate(displs_nph(0:nprocs-1), recvcounts_nph(0:nprocs-1))
    call distribute_indices(nph, nprocs, rank, displs_nph, recvcounts_nph)
    iph_offset = displs_nph(rank)
    nph_loc = recvcounts_nph(rank)

    if (master) then
      print *, "nmode_tot, nmode = ", nmode_tot, nmode
      print *, "nq, nph = ", nq, nph
      do i = 1, nmode
        print *, "i, imodes(i) = ", i, imodes(i)
      enddo

      print *, "Distribution of nph"
      do i = 0, nprocs-1
        print '(1x,a,3I9)', 'rank, iph_offset, nph_loc = ', &
          i, displs_nph(i), recvcounts_nph(i)
      enddo
    endif

  end subroutine read_imodes

  subroutine setup_kqgrid

    integer :: ik, iq, ikq, ikq_G(3)
    real(dp) :: diff(3)
    call start_clock ('kqgrid')
    call printblock_start ('k,q-point grid')

    !! q-grid should be subgrid of k-grid
    if (mod(nk1,nq1).ne.0 .or. &
        mod(nk2,nq2).ne.0 .or. &
        mod(nk3,nq3).ne.0) then
        call errore('setup_kqgrid', 'q-grid must be subgrid of k-grid', 1)
    endif

    !! kgrid
    if (master) then
      print '(1x,a,3I5)', &
        "Generating uniform k-grid with nk1, nk2, nk3 = ", nk1, nk2, nk3
    endif

    allocate(kpoints(3,nk))
    call generate_uniform_grid(nk1, nk2, nk3, kpoints)

    allocate(kpoints_cart(3,nk))
    do ik = 1, nk
      kpoints_cart(:,ik) = matmul(relatt, kpoints(:,ik))/(2*PI)
    enddo

    if (nk.lt.4) then
      call errore('setup_kqgrid', &
        'at least 4 k-points are required for tetrahedron method', 1)
    endif

    !! qgrid
    if (master) then
      print '(1x,a,3I5)', &
        "Generating uniform q-grid with nq1, nq2, nq3 = ", nq1, nq2, nq3
    endif

    allocate(qpoints(3,nq))
    call generate_uniform_grid(nq1, nq2, nq3, qpoints)

    allocate(qpoints_cart(3,nq))
    do iq = 1, nq
      qpoints_cart(:,iq) = matmul(relatt, qpoints(:,iq))/(2*PI)
    enddo

    call printblock_end
    call stop_clock ('kqgrid')
  end subroutine setup_kqgrid

  subroutine read_eigenvalues
    integer :: iq, imode, i
    character(len=100) :: fn

    call start_clock ('eigenvalues')
    call printblock_start ('Reading eigenvalues')

    nmode_tot = 3*na_uc

    if (lowest_band.le.0) lowest_band = 1
    if (highest_band.le.0) highest_band = nband_tot

    nband = highest_band-lowest_band+1

    allocate(enk(nband,nk))

    write(fn, '(a)') trim(data_tb)//'/enk.dat' 
    call read_2d_real_fmt(fn, nband_tot, lowest_band, highest_band, nk, 1, nk, enk)

    allocate(freq(nmode_tot,nq))
    write(fn, '(a)') trim(data_ph)//'/freq.dat' 
    call read_2d_real_fmt(fn, nmode_tot, 1, nmode_tot, nq, 1, nq, freq)

    allocate(lqv(nmode_tot,nq))
    do iq = 1, nq
      do imode = 1, nmode_tot
        lqv(imode,iq) = sqrt(1.d0/(2*mass*freq(imode,iq)))
      enddo
    enddo

    if (master) then
      print *, "nband_tot = ", nband_tot
      print *, "lb, hb = ", lowest_band, highest_band
      print *, "nband, nmode_tot = ", nband, nmode_tot
    endif
    call printblock_end
    call stop_clock ('eigenvalues')
  end subroutine read_eigenvalues

  subroutine compute_epmat
    integer :: iq, reclen
    character(len=100) :: epmat_fn
    complex(dp), allocatable :: tmp(:,:,:)

    call printblock_start('Computing ep matrix elements')
    call start_clock ('epmat')

    !! open epmat files 
    if (master) then
      allocate(tmp(nband,nband,nk))
      inquire(iolength=reclen) tmp
      do iq = 1, nq
        write(epmat_fn, '(a,I0.4,a)') trim(data_ep)//'/epmat.', iq, '.dat'
        open(IU_EPMAT+iq, file=trim(epmat_fn), &
             form="unformatted", status="unknown", access="direct", recl=reclen, action="write")
      enddo
    endif

    call mpi_barrier(comm, mpierr)

    call compute_epmat_orbital
    call compute_phase_fac
    call compute_epmat_band

    if (master) then
      do iq = 1, nq
        close(IU_EPMAT+iq)
      enddo
    endif

    call printblock_end
    call stop_clock ('epmat')
  end subroutine compute_epmat

  ! Compute electron-phonon matrix elements in tight-binding orbital basis
  subroutine compute_epmat_orbital

    integer :: ia, inb, ix
    real(dp) :: at_I(3), at_J(3), dx(3)

    call start_clock ('epmat_orb')
    if (master) then
      print *, "Computing epmat in orbital basis" 
    endif

    allocate(epmat_orb(max_neighbors, 3, na_uc))

    epmat_orb = 0.d0

    do ia = 1, na_uc
      at_I = atoms_uc(1:3, ia)
      do inb = 1, num_neighbors(ia)
        at_J = atoms_sc(1:3, neighbors(inb, ia))
        dx = at_I - at_J 
        do ix = 1, 3
          epmat_orb(inb, ix, ia) = dtdx(dx, ix)
        enddo
      enddo
    enddo

    call stop_clock ('epmat_orb')
  end subroutine compute_epmat_orbital

  subroutine compute_phase_fac

    integer :: ik, isc

    real(dp) :: arg

    call start_clock ('phase_fac')
    if (master) then
      print *, "Computing phase factors"
    endif

    allocate(phase_fac(nsc, nk))
    phase_fac = 0.d0

    do ik = 1, nk

      do isc = 1, nsc
        arg = 2*pi*sum(R_isc(1:3,isc)*kpoints(1:3,ik))
        phase_fac(isc,ik) = cmplx(cos(arg),sin(arg),kind=dp)
      enddo
    enddo

    call stop_clock ('phase_fac')
  end subroutine compute_phase_fac

  subroutine compute_epmat_band

    complex(DP), allocatable :: &
      evq(:), &           ! e_q(3*na_uc) phonon eigenvector for iph = (iq,imode)
      cnk(:,:,:), &       ! cnk(na_uc,nband,nk) electron eigenvectors
      epmat_band(:,:,:)   ! epmat_band(nband,nband,nk)
                          !  el-ph matrix elements in band basis

    integer :: iq, i, imode, ik, ikq, ibnd, jbnd, ikq_G(3)
    integer :: ia_uc, ix, inb, ja_sc, ja_uc, jsc, iph, iph_loc, iproc
    complex(DP) :: mat, elec_fac, ph_fac
    character(len=100) :: timestr, fn

    integer :: nph_progress, mpi_status(MPI_STATUS_SIZE), source, buf, &
               iph_prev, iph_loc_count
    integer, parameter :: IPH_DONE = -1, IPH_INIT = 0
    logical :: procs_complete(0:nprocs-1), isdone
    call start_clock ('epmat_band')
    if (master) then
      print *, "Computing epmat in band basis"
    endif

    ! read all the electron wfns and phonon eigenmode_tots
    allocate(evq(nmode_tot))
    allocate(epmat_band(nband,nband,nk))
    allocate(cnk(na_uc,nband,nk))

    call start_clock ('io_cnk')
    if (master) then
      do ik = 1, nk
        write(fn, '(a,I0.6,a)') trim(data_tb)//'/cnk.', ik, '.dat'
        call read_2d_cmplx(fn, na_uc, 1, na_uc, nband_tot, lowest_band, highest_band, &
                           cnk(1:na_uc, 1:nband, ik))
      enddo
    endif

    call mpi_bcast(cnk, na_uc*nband*nk, mpi_double_complex, 0, comm, mpierr)
    call stop_clock ('io_cnk')

    write(fn, '(a,I0.6,a)') 'iph.',rank,'.info'
    open(240, file=trim(fn), form="formatted")

    if (master) then
      !! master rank
      nph_progress = 0
      do while (nph_progress.lt.nph) 

        call mpi_recv(epmat_band, nband*nband*nk, mpi_double_complex, MPI_ANY_SOURCE, MPI_ANY_TAG, comm, mpi_status, mpierr)
        nph_progress = nph_progress + 1
        call mpi_send(nph_progress, 1, mpi_integer, mpi_status(MPI_SOURCE), mpi_status(MPI_TAG), comm, mpierr)
        WRITE(TIMESTR, "(a,i5,a,i5)") "Computing iph ", nph_progress, ' at rank ', mpi_status(MPI_SOURCE)
        call timestamp (timestr)

        !! IF EPMAT_BAND IS RECIVED, WRITE TO FILE
        iph = mpi_status(MPI_TAG)
        if (iph.gt.IPH_INIT) then
          i = mod(iph-1, nmode) + 1
          imode = imodes(i)
          iq    = (iph-1)/nmode + 1
          call io_epmat('write', iq, imode, nband, nk, epmat_band)
        endif
      enddo

      procs_complete = .false.
      do while (.true.)
        call mpi_recv(epmat_band, nband*nband*nk, mpi_double_complex, MPI_ANY_SOURCE, MPI_ANY_TAG, comm, mpi_status, mpierr)
        call mpi_send(IPH_DONE, 1, mpi_integer, mpi_status(MPI_SOURCE), mpi_status(MPI_TAG), comm, mpierr)
        !! IF EPMAT_BAND IS RECIVED, WRITE TO FILE
        iph = mpi_status(MPI_TAG)
        if (iph.gt.IPH_INIT) then
          i = mod(iph-1, nmode) + 1
          imode = imodes(i)
          iq    = (iph-1)/nmode + 1
          call io_epmat('write', iq, imode, nband, nk, epmat_band)
        endif

        procs_complete(mpi_status(MPI_SOURCE)) = .true.
        WRITE(TIMESTR, "(a,i5,a)") 'Rank ', mpi_status(MPI_SOURCE), " done!"
        call timestamp (timestr)

        isdone = .true.
        do iproc = 1, nprocs-1
          if (.not. procs_complete(iproc)) then
            isdone = .false.
          endif
        enddo
        if (isdone) then
          exit
        endif
      enddo
    else
      !! computing rank
      call mpi_send(epmat_band, nband*nband*nk, mpi_double_complex, 0, IPH_INIT, comm, mpierr) 
      call mpi_recv(iph, 1, mpi_integer, 0, 0, comm, mpi_status, mpierr)
      iph_loc_count = 0

      do while (iph .ne. IPH_DONE) 

        iph_loc_count = iph_loc_count + 1
        
        !! DO CALC FOR IPH
        write(240, '(a,I5,I10,I10)') "rank, iph, iph_loc_count = ", rank, iph, iph_loc_count

        i = mod(iph-1, nmode) + 1
        imode = imodes(i)
        iq    = (iph-1)/nmode + 1

        epmat_band = 0.d0

        if (freq(imode, iq).gt.acoustic_cut) then
          call start_clock ('io_evq')
          write(fn, '(a,I0.6,a)') trim(data_ph)//'/evq.', iq, '.dat'
          call read_2d_cmplx_rec(fn, nmode_tot, 1, nmode_tot, imode, &
                                 evq(1:nmode_tot))
          call stop_clock ('io_evq')

          call start_clock ('epmat_qv')
          do ik = 1, nk
            call kqmap(kpoints(1:3,ik), qpoints(1:3,iq), ikq, ikq_G)

            do ibnd = 1, nband
              do jbnd = 1, nband

                !! epmat(ibnd,jbnd,ik) for given iq,imode
                mat = 0.d0 

                do ia_uc = 1, na_uc
                  do ix = 1, 3
                    ph_fac = evq(3*(ia_uc-1)+ix)

                    do inb = 1, num_neighbors(ia_uc)
                      ja_sc = neighbors(inb, ia_uc)
                      ja_uc = sc_uc_map(ja_sc)
                      jsc = isc_map(ja_sc)

                      mat = mat + &
                        ph_fac * epmat_orb(inb,ix,ia_uc) &
                        * ( conjg( cnk(ia_uc,jbnd,ikq) ) * phase_fac(jsc,ik)*cnk(ja_uc,ibnd,ik) &
                        + conjg( phase_fac(jsc,ikq)*cnk(ja_uc,jbnd,ikq) ) * cnk(ia_uc,ibnd,ik) )

                    enddo ! inb
                  enddo ! ix
                enddo ! ia_uc

                epmat_band(ibnd,jbnd,ik) = lqv(imode,iq)*mat
              enddo ! jbnd
            enddo ! ibnd
          enddo ! ik
          call stop_clock ('epmat_qv')
        endif

        !! SEND DATA TO MASTER AND GET A NEW IPH
        call mpi_send(epmat_band, nband*nband*nk, mpi_double_complex, 0, iph, comm, mpierr) 
        iph_prev = iph
        call mpi_recv(iph, 1, mpi_integer, 0, iph_prev, comm, mpi_status, mpierr)
      enddo

      deallocate(evq, epmat_band, cnk)
    endif
    
    close(240)
    call mpi_barrier(comm, mpierr)

    call stop_clock ('epmat_band') 
  end subroutine compute_epmat_band

  ! q-grid should be subgrid of k-grid
  ! k-grid should be positive
  subroutine kqmap(k,q, ikq, kq_G)
    real(dp), intent(in) :: k(3), q(3)
    integer, intent(out) :: ikq, kq_G(3)

    integer :: ik1, ik2, ik3, iq1, iq2, iq3

    integer :: ikq1, ikq2, ikq3

    ik1 = nint(k(1)*nk1)
    ik2 = nint(k(2)*nk2)
    ik3 = nint(k(3)*nk3)
    iq1 = nint(q(1)*nk1)
    iq2 = nint(q(2)*nk2)
    iq3 = nint(q(3)*nk3)

    ikq1 = ik1+iq1
    ikq2 = ik2+iq2
    ikq3 = ik3+iq3

    kq_G = 0
    if (ikq1.ge.nk1) then
      ikq1 = ikq1 - nk1
      kq_G(1) = 1
    endif

    if (ikq2.ge.nk2) then
      ikq2 = ikq2 - nk2
      kq_G(2) = 1
    endif

    if (ikq3.ge.nk3) then
      ikq3 = ikq3 - nk3
      kq_G(3) = 1
    endif


    ikq = ikq1*nk2*nk3 + ikq2*nk3 + ikq3 + 1

  end subroutine kqmap

  ! derivative of hopping function w.r.t. ix axis,
  ! evaluated at d
  real(dp) function dtdx ( d, ix ) 
    ! Slater-Koster tight-binding parameters
    real(DP), parameter :: Vpi  = -2.7d0/RYTOEV   , &
                           Vsig =  0.48d0/RYTOEV  , &
                           a0   =  2.46/sqrt(3.d0)*ANGSTROM_AU,  &
                           d0   =  3.35*ANGSTROM_AU, &
                           r0   =  0.184d0 * 2.46*ANGSTROM_AU

    integer, intent(in) :: ix
    real(dp), intent(in) :: d(3)
    real(dp) :: dist, fac1, fac2

    dist = norm2(d)

    !! Do not calculate duplicate parts
    if (ix.eq.1 .or. ix.eq.2) then

      fac1 = d(ix)*d(3)*d(3)/(r0*dist**3)
      fac2 = 2*d(ix)*d(3)*d(3)/dist**4

      dtdx = Vpi*exp(-(dist-a0)/r0)*( fac1 - d(ix)/(r0*dist) + fac2 ) &
             + Vsig*exp(-(dist-d0)/r0)*( -fac1 -fac2 ) 

    else
      fac1 = d(ix)*d(3)*d(3)/(r0*dist**3)
      fac2 = 2*d(3)*(dist**2-d(3)**2)/dist**4

      dtdx = Vpi*exp(-(dist-a0)/r0)*( fac1 - d(ix)/(r0*dist) - fac2 ) &
             + Vsig*exp(-(dist-d0)/r0)*( -fac1 + fac2 )
    endif

  end function dtdx
end module epmat

