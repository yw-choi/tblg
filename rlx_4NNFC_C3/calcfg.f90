subroutine calcfg(n,x,f,g)
  use mpi

  use kinds, only: DP
  use lattice, only: N_LAYER, N_INTRA_SHELL, NA_INTRA_SHELL, & 
                     na_uc, na_uc_l, na_sc, na_sc_l, &
                     atoms_uc, atoms_sc, sc_uc_map, &
                     isc_map, R_isc, max_inter_neighbors, &
                     num_inter_neighbors, inter_neighbors, &
                     intra_neighbors, ia_offset, na_loc, &
                     recvcounts, displs, &
                     n_C3_equiv, &
                     C3_equiv, &
                     C3_unique,  &
                     n_C3_unique, &
                     Myz_equiv
  use phonons_fc, only: LJ_eps, LJ_sig, &
                        C0, C2, C4, C, delta, lambda, A_KC, z0 
  use constants, only: RYTOEV, ANGSTROM_AU, PI
  use rlxmod, only: constraint_type, nfc, fc_pairs, fc, vdw_type, &
    symmetrize
  integer, intent(in) :: n
  real(dp), intent(inout) :: x(n)
  real(dp), intent(out) :: f, g(n)

  integer :: il, ia_uc, ja_sc, ja_uc, ishell, ia_shell, &
             m1, m2, m3, ia_uc_g, ja_uc_g, i, j, il2, ifc, jl
  real(dp) :: at_i(3), at_j(3), dx(3), costh, sinth, rot(3,3), &
              dist, rfac, rfac6, rfac12, ni(3), nj(3), th, at_tmp(3)
  real(dp) :: r_ij, rho_ij, rho_ji, f_ij, f_ji, rd2_ij, rd2_ji, v_KC
  real(dp) :: f_intra, f_inter, exp_lrz, exp_rd2_ij, exp_rd2_ji,&
              dfdrho, njdx, nidx, Azr6

  real(dp) :: normals(3,na_uc_l,N_LAYER), g_inter(n), gi(3), gj(3)

  real(dp) :: gi_avg(3), g_nosymm(n), gerr, maxgerr


  f = 0.d0
  g = 0.d0

  f_intra = 0.d0

  do ifc = 1, nfc
    il = (fc_pairs(1,ifc)-1)/na_uc_l+1
    ia_uc = mod(fc_pairs(1,ifc)-1,na_uc_l)+1

    jl = (fc_pairs(2,ifc)-1)/na_sc_l+1
    ja_sc = mod(fc_pairs(2,ifc)-1,na_sc_l)+1
    ja_uc = sc_uc_map(ja_sc, jl)

    ia_uc_g = na_uc_l*(il-1)+ia_uc
    ja_uc_g = na_uc_l*(jl-1)+ja_uc

    do m1 = 1,3
      do m2 = 1,3
        i = 3*(ia_uc_g-1)+m1
        j = 3*(ja_uc_g-1)+m2
        f_intra = f_intra + 0.5d0*fc(m1,m2,ifc)*x(i)*x(j)
        g(i) = g(i) + fc(m1,m2,ifc)*x(j)
      enddo
    enddo
  enddo

  !! inter
  f_inter = 0.d0
  g_inter = 0.d0
  if (vdw_type.eq.1) then
    ! LJ 
    do il = 1, N_LAYER
      il2 = 3 - il
      do ia_uc = ia_offset+1, ia_offset+na_loc

        ia_uc_g = na_uc_l*(il-1)+ia_uc
        at_i = atoms_uc(:,ia_uc,il)
        do m1 = 1, 3
          at_i(m1) = at_i(m1) + x(3*(ia_uc_g-1)+m1)
        enddo
        
        do inb = 1, num_inter_neighbors(ia_uc,il)
          ja_sc = inter_neighbors(inb, ia_uc, il)
          ja_uc = sc_uc_map(ja_sc,il2)
          ja_uc_g = na_uc_l*(il2-1)+ja_uc

          at_j = atoms_sc(:,ja_sc,il2)
          do m1 = 1, 3
            at_j(m1) = at_j(m1) + x(3*(ja_uc_g-1)+m1)
          enddo

          dx = at_i - at_j
          dist = norm2(dx)

          rfac = LJ_sig/dist
          rfac6 = rfac**6

          if (il.eq.1) then
            f_inter = f_inter + 4.d0*LJ_eps*rfac6*(rfac6-1.d0)
          endif

          do m1 = 1, 3
            i = 3*(ia_uc_g-1)+m1
            g_inter(i) = g_inter(i) &
              + 4.d0*LJ_eps*rfac6*(-12.d0*rfac6+6.d0)/dist**2*dx(m1)
          enddo
        enddo
      enddo
    enddo

  else if (vdw_type.eq.2) then

    normals = 0.d0
    do il = 1, N_LAYER
      do ia_uc = ia_offset+1, ia_offset+na_loc
        call compute_normal(il, ia_uc, intra_neighbors(1:3, 1, ia_uc, il), n, x, normals(1:3,ia_uc,il))
      enddo
      call mpi_allgatherv(mpi_in_place, 0, mpi_datatype_null,&
                    normals(:,:,il), 3*recvcounts, 3*displs, &
                    mpi_double_precision, comm, mpierr)
    enddo


    ! KC
    do il = 1, N_LAYER
      il2 = 3 - il
      do ia_uc = ia_offset+1, ia_offset+na_loc

        ia_uc_g = na_uc_l*(il-1)+ia_uc
        at_i = atoms_uc(:,ia_uc,il)
        do m1 = 1, 3
          at_i(m1) = at_i(m1) + x(3*(ia_uc_g-1)+m1)
        enddo

        ! normal
        ni = normals(1:3,ia_uc,il)
        
        do inb = 1, num_inter_neighbors(ia_uc,il)
          ja_sc = inter_neighbors(inb, ia_uc, il)
          ja_uc = sc_uc_map(ja_sc,il2)
          ja_uc_g = na_uc_l*(il2-1)+ja_uc

          at_j = atoms_sc(:,ja_sc,il2)
          do m1 = 1, 3
            at_j(m1) = at_j(m1) + x(3*(ja_uc_g-1)+m1)
          enddo

          ! normal
          nj = normals(1:3,ja_uc,il2)

          dx = at_i - at_j
          r_ij = norm2(dx)

          nidx = dot_product(ni,dx)
          njdx = dot_product(nj,dx)
          rho_ij = sqrt(r_ij**2-nidx**2)
          rho_ji = sqrt(r_ij**2-njdx**2)

          rd2_ij = (rho_ij/delta)**2
          exp_rd2_ij = exp(-rd2_ij)
          f_ij = exp_rd2_ij * (C0+C2*rd2_ij+C4*rd2_ij*rd2_ij)

          rd2_ji = (rho_ji/delta)**2
          exp_rd2_ji = exp(-rd2_ji)
          f_ji = exp_rd2_ji * (C0+C2*rd2_ji+C4*rd2_ji*rd2_ji)

          exp_lrz = exp(-lambda*(r_ij-z0))
          Azr6 = A_KC*(z0/r_ij)**6
          v_KC = exp_lrz*(C+f_ij+f_ji)-Azr6
          
          f_inter = f_inter + 0.5d0*v_KC

          do m1 = 1,3
            i = 3*(ia_uc_g-1)+m1

            dfdx_ij = ( -2/delta**2*(C0+C2*rd2_ij+C4*rd2_ij*rd2_ij) &
              + 2*C2/delta**2 + 4*C4*rho_ij**2/delta**4)*exp_rd2_ij * (dx(m1) - nidx*ni(m1))
            dfdx_ji = ( -2/delta**2*(C0+C2*rd2_ji+C4*rd2_ji*rd2_ji) &
              + 2*C2/delta**2 + 4*C4*rho_ji**2/delta**4)*exp_rd2_ji * (dx(m1) - njdx*nj(m1))

            g_inter(i) = g_inter(i) &
              + exp_lrz*( -lambda*dx(m1)/r_ij*(C+f_ij+f_ji) &
              + dfdx_ij + dfdx_ji ) &
              + 6*Azr6 * dx(m1)/r_ij**2
          enddo

        enddo
      enddo
    enddo

  endif

  call mpi_allreduce (mpi_in_place, f_inter, 1, mpi_double_precision, mpi_sum, comm, mpierr)
  call mpi_allreduce (mpi_in_place, g_inter, n, mpi_double_precision, mpi_sum, comm, mpierr)

  g = g + g_inter

  g_nosymm = g

  if (symmetrize) then
    ! C3
    do i = 1, n_C3_unique
      ia_uc = C3_unique(i)
      if (n_C3_equiv(i).eq.0) then
        g(3*(ia_uc-1)+1:3*(ia_uc-1)+3) = 0.d0
      else
        gi = g(3*(ia_uc-1)+1:3*(ia_uc-1)+3)
        gi_avg = gi
        do j = 1, n_C3_equiv(i)
          ja_uc = C3_equiv(j,i)
          th = 2.0d0*j*PI/3.0d0
          rot(1,:) = (/  cos(th), -sin(th), 0.0d0 /)
          rot(2,:) = (/  sin(th),  cos(th), 0.0d0 /)
          rot(3,:) = (/    0.0d0,    0.0d0, 1.0d0 /)

          gj = g(3*(ja_uc-1)+1:3*(ja_uc-1)+3)
          gi(1) = sum(rot(1,:)*gj(:))
          gi(2) = sum(rot(2,:)*gj(:))
          gi(3) = sum(rot(3,:)*gj(:))
          gi_avg = gi_avg + gi
        enddo
        gi_avg = gi_avg / (1+n_C3_equiv(i))
        g(3*(ia_uc-1)+1:3*(ia_uc-1)+3) = gi_avg
        do j = 1, n_C3_equiv(i)
          ja_uc = C3_equiv(j,i)
          th = 2.d0*j*PI/3.0d0
          rot(1,:) = (/  cos(th),  sin(th), 0.0d0 /)
          rot(2,:) = (/ -sin(th),  cos(th), 0.0d0 /)
          rot(3,:) = (/    0.0d0,    0.0d0, 1.0d0 /)

          g(3*(ja_uc-1)+1:3*(ja_uc-1)+3) = matmul(rot, gi_avg)
        enddo
      endif
    enddo

    ! Myz
    do ia_uc = 1, na_uc_l
      gi = g(3*(ia_uc-1)+1:3*(ia_uc-1)+3)

      gi(2) = -gi(2)
      gi(3) = -gi(3)
      ja_uc = Myz_equiv(ia_uc)
      ja_uc_g = na_uc_l+ja_uc

      g(3*(ja_uc_g-1)+1:3*(ja_uc_g-1)+3) = gi
    enddo
  endif
  maxgerr = -1.d0
  do ia_uc = 1, na_uc
    gerr = norm2(g(3*(ia_uc-1)+1:3*(ia_uc-1)+3)-g_nosymm(3*(ia_uc-1)+1:3*(ia_uc-1)+3))
    if (gerr.gt.maxgerr) then
      maxgerr = gerr
    endif
  enddo

  if (master) then
    print *, 'max|g-g_nosymm| = ', maxgerr*ANGSTROM_AU*RYTOEV
  endif


  !! manual constraints
  if (constraint_type.eq.1) then
    !! fix the z-position of the atoms in the first layer
    do ia_uc = 1, na_uc_l
      i = 3*(ia_uc-1)+3
      g(i) = 0.d0
    enddo
  else if (constraint_type.eq.2) then
    !! fix the z-position of all the atoms
    do ia_uc = 1, na_uc
      i = 3*(ia_uc-1)+3
      g(i) = 0.d0
    enddo
  else if (constraint_type.eq.3) then
    !! fix the xy-position of all the atoms
    do ia_uc = 1, na_uc
      i = 3*(ia_uc-1)+1
      g(i) = 0.d0
      i = 3*(ia_uc-1)+2
      g(i) = 0.d0
    enddo
  endif


  ! print '(I,A,3F20.12)', rank, "f, f_intra, f_inter =", f, f_intra, f_inter 
  ! call mpi_allreduce (mpi_in_place, f_intra, 1, mpi_double_precision, mpi_sum, comm, mpierr)

  f = f_intra + f_inter
  ! print *, rank, f
  if (master) then
    print '(A,3F20.12)', "f, f_intra, f_inter =", f, f_intra, f_inter 
  endif
end subroutine calcfg

subroutine compute_normal(il, ia_uc, neighbors, n, x, normal)
  use kinds, only: DP
  use lattice, only: N_LAYER, N_INTRA_SHELL, NA_INTRA_SHELL, & 
                     na_uc, na_uc_l, na_sc, na_sc_l, &
                     atoms_uc, atoms_sc, sc_uc_map, &
                     isc_map, R_isc, max_inter_neighbors, &
                     num_inter_neighbors, inter_neighbors, &
                     intra_neighbors, ia_offset, na_loc, &
                     recvcounts, displs
  use constants, only: ANGSTROM_AU
  integer, intent(in) :: il, ia_uc, neighbors(3)
  real(dp), intent(in) :: x(n)
  real(dp), intent(out) :: normal(3)

  ! real(dp) :: at_i(3), dx(3,3), c(3,3)

  ! integer :: ia_uc_g, i_uc(3), i_uc_g(3), i

  ! ia_uc_g = na_uc_l*(il-1)+ia_uc
  ! at_i = atoms_uc(1:3, ia_uc, il) + x(3*(ia_uc_g-1)+1:3*(ia_uc_g-1)+3)

  ! do i = 1,3
  !   i_uc(i) = sc_uc_map(neighbors(i),il)
  !   i_uc_g(i) = na_uc_l*(il-1)+i_uc(i)
  !   dx(1:3,i) = atoms_sc(1:3, neighbors(i), il) &
  !               + x(3*(i_uc_g(i)-1)+1:3*(i_uc_g(i)-1)+3) - at_i
  ! enddo

  ! call cross_prod( dx(1:3, 1), dx(1:3, 2), c(1:3,1) )
  ! call cross_prod( dx(1:3, 2), dx(1:3, 3), c(1:3,2) )
  ! call cross_prod( dx(1:3, 3), dx(1:3, 1), c(1:3,3) )

  ! if (c(3,1).lt.0.d0) then
  !   c = -c
  ! endif

  ! do i = 1,3
  !   c(1:3,i) = c(1:3,i)/norm2(c(1:3,i))
  ! enddo

  ! do i = 1,3
  !   normal(1:3) = normal(1:3) + c(1:3,i)
  ! enddo

  ! normal = normal/norm2(normal)
  normal = (/0.d0,0.d0,1.0d0/)
end subroutine compute_normal

subroutine cross_prod(a,b,c)
  use kinds, only: DP
  real(dp) :: a(3), b(3), c(3)
  c(1) = a(2)*b(3)-a(3)*b(2)
  c(2) = a(3)*b(1)-a(1)*b(3)
  c(3) = a(1)*b(2)-a(2)*b(1)
end subroutine cross_prod

