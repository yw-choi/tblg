
FC=mpif90 -fc=ifort

IFLAGS=-I./utils -I.
FCFLAGS=-traceback -O3 -xSSE4.2 
LDFLAGS=

UTIL=utils/libutil.a
LIBS= -mkl $(UTIL)

PROGRAMS=rlx.x
OBJECTS=mpi.o kind.o timestamp.o constants.o stop_rlx.o invmat.o \
				printutils.o sort.o distribute_indices.o \
			    tblg.o lattice.o phonons_fc.o calcfg.o conmin.o rlxmod.o solve.o input.o 

all: $(PROGRAMS)

rlx.o: $(OBJECTS)
rlx.x: rlx.o $(UTIL) $(OBJECTS)
	$(FC) -o rlx.x rlx.o $(IFLAGS) $(FCFLAGS) $(LDFLAGS) $(OBJECTS) $(LIBS) 

$(UTIL):
	cd utils; make
# ======================================================================
# And now the general rules, these should not require modification
# ======================================================================

# General rule for building prog from prog.o; $^ (GNU extension) is
# used in order to list additional object files on which the
# executable depends
%: %.o
	$(FC) $(IFLAGS) $(FCFLAGS) $(LIBS) -o $@ $^ $(LDFLAGS)

# General rules for building prog.o from prog.f90 or prog.F90; $< is
# used in order to list only the first prerequisite (the source file)
# and not the additional prerequisites such as module or include files
%.o: %.f90
	$(FC) $(IFLAGS) $(FCFLAGS) -c $<
%.o: %.F90
	$(FC) $(IFLAGS) $(FCFLAGS) -c $<
%.o: %.f
	$(FC) $(IFLAGS) $(FCFLAGS) -c $<
%.o: %.F
	$(FC) $(IFLAGS) $(FCFLAGS) -c $<

# Utility targets
.PHONY: clean veryclean

clean:
	find . -type f | xargs touch
	rm -f *.o *.mod *.MOD
	cd utils; make clean

veryclean: clean
	rm -f *~ $(PROGRAMS)

calcfg.o: phonons_fc.o lattice.o rlxmod.o
