module rlxmod
  use mpi
  use kinds, only: DP
  use lattice, only: na_uc, atoms_uc, N_LAYER, na_uc_l, latt
  use phonons_fc, only: compute_fc
  use constants, only: ANGSTROM_AU, RYTOEV, PI
  
  implicit none
  public

  character(len=100) :: initial_dx

  integer :: nmode, constraint_type, vdw_type

  logical :: do_rlx, symmetrize

  integer :: nfc
  real(dp) :: conv, rcut
  real(dp), allocatable :: fc(:,:,:)
  integer, allocatable :: fc_pairs(:,:)

contains

  subroutine do_relax

    real(dp), allocatable :: x(:), atoms_rlx(:,:,:), g(:)
    real(dp) :: f
    integer :: ia, m1, il, ia_g

    call compute_fc (rcut, -1, nfc, fc_pairs, fc)

    nmode = 3*na_uc

    allocate(x(nmode), atoms_rlx(3,na_uc,N_LAYER), g(nmode))

    x = 0.d0
    if (master.and.trim(initial_dx).ne.'') then
      open(11, file=trim(initial_dx), form="formatted")
      do ia = 1, na_uc
        read(11,*) x(3*(ia-1)+1:3*(ia-1)+3)
      enddo
      close(11)

      x = x * ANGSTROM_AU
    endif

    call mpi_bcast(x, nmode, mpi_double_precision, 0, comm, mpierr)

    if (do_rlx) then

      if (master) print *, "Start minimization.."
      call start_clock('minimize')
      call minimize ( nmode, x, f, g )
      call stop_clock('minimize')

      call calcfg ( nmode, x, f, g )

      if (master) then
        open(11, file="atoms.out", form="formatted")
        write(11, "(3F24.16)") latt(:,1)/ANGSTROM_AU
        write(11, "(3F24.16)") latt(:,2)/ANGSTROM_AU 
        write(11, "(3F24.16)") latt(:,3)/ANGSTROM_AU 
        write(11, *) na_uc

        print "(1x,A,F24.16)", 'Total Energy (eV) =', F*RYTOEV
        print *, "atom positions (Ang.) and forces (eV/Ang)"
        atoms_rlx = atoms_uc
        do il = 1, N_LAYER
          do ia = 1, na_uc_l
            ia_g = na_uc_l*(il-1)+ia
            atoms_rlx(:,ia,il) = atoms_rlx(:,ia,il)+x(3*(ia_g-1)+1:3*(ia_g-1)+3)
            print "(3F24.16,1x,3F24.16)", atoms_rlx(:,ia,il)/ANGSTROM_AU, -g(3*(ia_g-1)+1:3*(ia_g-1)+3)*RYTOEV*ANGSTROM_AU
            write(11, "(3F24.16,1x,3F24.16)") atoms_uc(:,ia,il)/ANGSTROM_AU, atoms_rlx(:,ia,il)/ANGSTROM_AU
          enddo
        enddo
        close(11)
      endif

      call mpi_bcast(atoms_rlx, 3*na_uc_l*N_LAYER, mpi_double_precision, 0, comm, mpierr)

    else

      call calcfg ( nmode, x, f, g )

      if (master) then
        print "(1x,A,F24.16)", 'Total Energy (eV) =', F*RYTOEV
        print *, "atom positions (Ang.) and forces (eV/Ang)"
        atoms_rlx = atoms_uc
        do il = 1, N_LAYER
          do ia = 1, na_uc_l
            ia_g = na_uc_l*(il-1)+ia
            atoms_rlx(:,ia,il) = atoms_rlx(:,ia,il)+x(3*(ia_g-1)+1:3*(ia_g-1)+3)
            print "(3F24.16,1x,3F24.16)", atoms_rlx(:,ia,il)/ANGSTROM_AU, -g(3*(ia_g-1)+1:3*(ia_g-1)+3)*RYTOEV*ANGSTROM_AU
          enddo
        enddo

        open(11, file="atoms.out", form="formatted")
        write(11, "(3F24.16)") latt(:,1)/ANGSTROM_AU
        write(11, "(3F24.16)") latt(:,2)/ANGSTROM_AU 
        write(11, "(3F24.16)") latt(:,3)/ANGSTROM_AU 
        write(11, *) na_uc
        atoms_rlx = atoms_uc
        do il = 1, N_LAYER
          do ia = 1, na_uc_l
            ia_g = na_uc_l*(il-1)+ia
            atoms_rlx(:,ia,il) = atoms_rlx(:,ia,il)+x(3*(ia_g-1)+1:3*(ia_g-1)+3)
            write(11, "(3F24.16,1x,3F24.16)") atoms_uc(:,ia,il)/ANGSTROM_AU, atoms_rlx(:,ia,il)/ANGSTROM_AU
          enddo
        enddo
        close(11)
      endif


    endif

  end subroutine do_relax

  subroutine minimize ( n, x, f, g)
    integer, intent(in) :: n
    real(dp), intent(inout) :: G(N),F,x(n) 

    real(dp) :: EPS,ACC
    real(dp), allocatable :: W(:)

    integer :: NMETH, MDIM, IFUN, ITER, NFLAG, MXFUN, &
               IOUT, IDEV

    !! USER-SUPPLIED PARAMETERS
    NMETH = 0 ! 0: CG, 1: BFGS
    EPS = conv
    MXFUN = 30000

    IF (rank.eq.0) THEN
      IOUT = 1
      IDEV = 6
    ELSE
      IOUT = 0
      IDEV = 0
    ENDIF
    ACC = 10.d-20

    MDIM = -1
    IF (NMETH.eq.0) THEN
      MDIM = 5*N+2
    ELSE IF (NMETH.eq.1) THEN
      MDIM = N*(N+7)/2
    ENDIF
    ALLOCATE(W(MDIM))
    
    CALL CONMIN(N,X,F,G,IFUN,ITER,EPS,NFLAG,MXFUN,W, &
                IOUT,MDIM,IDEV,ACC,NMETH)
    if (master) then
      print *, "NFLAG = ", NFLAG
    ENDIF

  end subroutine minimize
end module rlxmod
