program rlx

  use mpi
  use kinds, only: dp
  use constants, only: ANGSTROM_AU, RYTOEV
  use input, only: read_input
  use lattice, only: setup_lattice
  use rlxmod, only: do_relax, minimize, rcut, symmetrize

  implicit none

  integer :: i, j, ia, m1

  call mpi_initialize
  call timestamp('Start of run')

  call init_clocks ( .true. )
  call start_clock ( 'RLX' ) 

  if (master) then
    print '(a)', repeat('=',60)
    print '(a,a,a)', '== ','Structural relaxation for twisted bilayer graphene',' =='
    print '(a)', repeat('=',60)

    print *, 'Running on ', nprocs, ' nodes'
    print '(a)', repeat('=',60)
  endif

  call read_input

  call setup_lattice (rcut, symmetrize)

  call do_relax

  call stop_rlx
end program rlx
