program main 
  implicit none

  integer :: iu, na, nb, i, j
  double complex :: dat(11164,21)
  iu = 9
  na = 11164
  nb = 21
  open(iu, file="./unk.00045.dat", form="unformatted")
  do i=1,nb
    read(iu) (dat(j,i), j=1,na)
  enddo
  close(iu)
  print *, dat(:,1)
end program main
