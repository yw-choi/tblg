import time
from mpi4py import MPI

class Timer:

    _idxmap = {}
    _idxcnt = 0
    _routines = []

    def __init__(self, program):

        comm = MPI.COMM_WORLD
        self.rank = comm.Get_rank()
        self.program = program
        self.start(program)

    def start(self, routine, silent=False):
        ts = time.time()
        if routine not in self._idxmap:
            self._idxmap[routine] = self._idxcnt
            self._routines.append([])
            self._idxcnt += 1
        self._routines[self._idxmap[routine]].append([ts, None])
        if self.rank == 0 and not silent:
            print '>>> %s start: %s' % (routine, time.strftime("%H:%M:%S"))

    def end(self, routine, silent=False):
        ts = time.time()
        self._routines[self._idxmap[routine]][-1][1] = ts
        if self.rank == 0 and not silent:
            print '>>> %s end: %s' % (routine, time.strftime("%H:%M:%S"))

    def print_clock(self):
        self.end(self.program)
        if self.rank == 0:
            print '>>> print_clock'
            print '%30s   %5s   %8s' % ('routine', 'count', 'total time')
            for i, r in enumerate(self._routines):
                routine = ""
                for k in self._idxmap:
                    if self._idxmap[k] == i:
                        routine = k

                dt = 0.
                for rr in r:
                    dt += rr[1]-rr[0]
                print '%30s   %5d   %8.1f sec. ' % (routine, len(r), dt)



