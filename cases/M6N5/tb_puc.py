# Tight-binding model calculation of twisted bilayer graphene with arbitrary commensurate rotation angle.
# Reference: S. Fang and E. Kaxiras, Phys. Rev. B 93, 235153 (2016)
from numpy import *
import matplotlib.pyplot as plt


# # lattice vectors
# alat = array([[sqrt(3.)/2., -1./2.],
#               [sqrt(3.)/2.,  1./2.]])
# delta_A = array([0,0,0])
# delta_B = (alat[0,:]+alat[1,:])/3

# Parameters for intralayer/interlayer hopping (See Table I.)
# in-plane hopping parameters in eV
t_intra = [
       0.0   , # this is the onsite energy
      -2.8922,
       0.2425,
      -0.2656,
       0.0235,
       0.0524,
      -0.0209,
      -0.0148,
      -0.0211]

def main():
    M = 1
    N = 1

    # Lattice constants
    a = 2.46 # Ang
    d = 3.35 # Ang
    c = 20   # Ang

    t = a*array([[sqrt(3.)/2., -1./2., 0.],
                 [sqrt(3.)/2.,  1./2., 0.],
                 [0., 0., c/a]]).T
    atoms_1st_uc = []
    atoms_1st_uc.append(t.dot(array([1./3,1./3,0])))
    atoms_1st_uc.append(t.dot(array([2./3,2./3,0])))
    atoms_1st_uc = array(atoms_1st_uc)

    max_shells = len(t_intra)
    cutoff = a/sqrt(3)*max_shells # cutoff for determining the size of aux. supercell

    sc_dim = [ int(ceil(cutoff/linalg.norm(t[:,0]))),
               int(ceil(cutoff/linalg.norm(t[:,1]))),
               0 ]

    print "Aux. supercell for 1st layer"
    atoms_1st_sc, iuo_1st, isc_1st = make_aux_supercell(t, atoms_1st_uc, sc_dim)

    print "Finding intralayer neighbors for the 1st layer"
    shells_1st = intralayer_neighbors(atoms_1st_uc, atoms_1st_sc, iuo_1st, isc_1st, max_shells)

    kpoints = loadtxt('./kpath.dat')

    na_uc_1st = len(atoms_1st_uc)

    g = 2*pi*linalg.inv(t).T
    eigvals = []
    for ik, k_frac in enumerate(kpoints):
        k = g.dot(k_frac)
        Hk = matrix(zeros((na_uc_1st, na_uc_1st)), dtype=complex128)

        # intralayer hoppings
        for ia in range(na_uc_1st):
            for ishell, shell in enumerate(shells_1st[ia]):
                for ia_sc in shell:
                    d = atoms_1st_sc[ia_sc]-atoms_1st_uc[ia]
                    Hk[ia, iuo_1st[ia_sc]] += t_intra[ishell]*exp(1.j* k.dot(d))

        ek, uk = linalg.eig(Hk)

        plt.scatter([ik]*len(ek), real(ek), s=1, c='k')
    plt.show()

    return

def generate_commensurate_cell(M, N, a, d, c):
    """
    construct commensurate supercell (M,N) of twisted bilayer graphene

    M, N commensurate supercell index
    a    lattice constant in Ang
    d    interlayer distance in Ang
    c    cell size including the vacuum region
    """

    print "Generating commensurate supercell with (M,N)=(%d,%d) ..." % (M, N)
    print ">>> a = %.3f Ang" % a
    print ">>> d = %.3f Ang" % d
    print ">>> c = %.3f Ang" % c
    print ">>> ( M,  N) /  theta         /  na"

    th = arccos(float(N*N+4.*N*M+M*M)/float(2.*(N*N+N*M+M*M)))
    na = 4*(N*N+N*M+M*M)
    print ">>> (%2d, %2d) /  %.4f deg   /  %d" %( M, N, th*180/pi, na)

    a_puc = a*array([[sqrt(3.)/2., -1./2., 0.],
                     [sqrt(3.)/2.,  1./2., 0.],
                     [0., 0., c/a]]).T

    rot = array([[cos(th/2),  sin(th/2), 0.],
                 [-sin(th/2),  cos(th/2), 0.],
                 [     0.,       0., 1.]])
    rot2 = array([[ cos(th/2), -sin(th/2), 0.],
                  [ sin(th/2),  cos(th/2), 0.],
                  [        0.,       0., 1.]])

    a_1st = rot.dot(a_puc)
    a_2nd = rot2.dot(a_puc)

    vert = array([0,0,d])

    # commensurate cell vectors
    t = array([[ N,   M, 0], \
               [-M, N+M, 0], \
               [ 0,   0, 1]]).dot(a_1st.T).T
    tp = array([[   M,   N, 0], \
                [  -N, M+N, 0], \
                [   0,   0, 1]]).dot(a_2nd.T).T

    # Find the cells in the commensurate super cell.
    cells_1st = supercell_lattice_points(N, M)
    cells_2nd = supercell_lattice_points(M, N)

    # Atom positions
    atoms_1st = []
    for cell in cells_1st:
        point = cell[0] * a_1st[:,0] + cell[1] * a_1st[:,1]
        c1 = cell
        c2 = cell + array([1./3, 1./3])
        atoms_1st.append(c1[0]*a_1st[:,0]+c1[1]*a_1st[:,1])
        atoms_1st.append(c2[0]*a_1st[:,0]+c2[1]*a_1st[:,1])
    atoms_1st = array(atoms_1st)

    atoms_2nd = []
    for cell in cells_2nd:
        point = cell[0] * a_2nd[:,0] + cell[1] * a_2nd[:,1]
        c1 = cell
        c2 = cell + array([1./3, 1./3])
        atoms_2nd.append(c1[0]*a_2nd[:,0]+c1[1]*a_2nd[:,1]+vert)
        atoms_2nd.append(c2[0]*a_2nd[:,0]+c2[1]*a_2nd[:,1]+vert)
    atoms_2nd = array(atoms_2nd)

    return t, a_1st, a_2nd, atoms_1st, atoms_2nd

def make_aux_supercell(t, atoms_uc, sc_dim):
    """
    t         : lattice vector
    atoms_uc  : atom positions in the unit cell
    sc_dim(3) : auxiliary supercell dimension
    """

    n_sc = int(prod(2*array(sc_dim[:])+1))
    na_uc = len(atoms_uc)
    na_sc = int(na_uc*n_sc)

    print "Aux. supercell dimension = %d x %d x %d" % (2*sc_dim[0]+1, 2*sc_dim[1]+1, 2*sc_dim[2]+1)
    print "Number of atoms in the aux. supercell = %d" % na_sc


    atoms_sc = zeros((na_sc,3))
    iuo = arange(na_sc)
    isc = zeros((na_sc,3), dtype=int32)

    atoms_sc[0:na_uc, :] = atoms_uc

    ia_sc = na_uc
    for i1 in range(-sc_dim[0], sc_dim[0]+1):
        for i2 in range(-sc_dim[1], sc_dim[1]+1):
            for i3 in range(-sc_dim[2], sc_dim[2]+1):
                # skip the original unit cell
                if i1==0 and i2==0 and i3==0:
                    continue

                R = t.dot([i1,i2,i3])

                for ia, at in enumerate(atoms_uc):
                    atoms_sc[ia_sc,:] = R + at
                    isc[ia_sc, :] = R
                    iuo[ia_sc] = ia
                    ia_sc += 1

    return atoms_sc, iuo, isc

def intralayer_neighbors(atoms_uc, atoms_sc, iuo, isc, max_shells):
    na_uc = len(atoms_uc)
    na_sc = len(atoms_sc)

    tol = 0.001

    shells = []
    for ia_uc, at in enumerate(atoms_uc):
        disp = atoms_sc - at
        dists = apply_along_axis(linalg.norm, axis=1, arr=disp)
        neighbors = argsort(dists)

        shells_at = []
        nshell = 0
        prev_dist = -1
        for i in neighbors:
            if dists[i]>prev_dist+tol:
                if nshell==max_shells:
                    break

                shells_at.append([i])
                prev_dist = dists[i]
                nshell += 1
            else:
                shells_at[nshell-1].append(i)

        shells.append(shells_at)
    return shells


def distance_matrix(t, atoms):
    max_neighbor = 8

    na = len(atoms)
    distmat = zeros((na,na))

    for ia in range(na):
        for ja in range(ia, na):
            d = linalg.norm(atoms[ia] - atoms[ja])
            distmat[ia, ja] = d
            distmat[ja, ia] = d

    return distmat

def supercell_lattice_points(N,M):
    """
    Find the lattice points in the commensurate supercell
    spanned by (N,M), (-M, N+M) vectors.
    Inequailities to be satisfied are as follows
    1 : M/N*i1 <= i2
    2 : -(N+M)/M*i1 <= i2
    3 : N+M + M/N*(i1+M) > i2
    4 : M - (N+M)/M*(i1-N) > i2
    """

    fM = float(M)
    fN = float(N)

    cells = []
    for i1 in range(-M,N):
        for i2 in range(0,N+2*M):
            if fM/fN*i1 <= i2 and \
               -(fN+fM)/fM*i1 <= i2 and \
               fN+fM + fM/fN*(i1+fM) > i2 and \
               fM - (fN+fM)/fM*(i1-fN) > i2:
                   cells.append([i1,i2])
    return array(cells, dtype=int32)

# interlayer hopping parameters
l_i = [0.3155, -0.0688, -0.0083] # eV
xi_i = [1.7543, 3.4692, 2.8764]  # dimensionless
x_i = [None, 0.5212, 1.5206]     # dimensionless
kappa_i = [2.0010, None, 1.5731] # dimensionless

# Eq (2), (3)
def interlayer_hopping(rbar, th12, th21):
    v0 = l_i[0] * exp(-xi_i[0]*rbar**2) * cos(kappa_i[0]*rbar)
    v3 = l_i[1] * rbar**2 * exp(-xi_i[1]*(rbar-x_i[1])**2)
    v6 = l_i[2] * exp(-xi_i[2]*(rbar-x_i[2])**2) * sin(kappa_i[2]*rbar)

    return v0 + v3 * (cos(3*th12)+cos(3*th21)) \
              + v6 * (cos(6*th12)+cos(6*th21))

main()

