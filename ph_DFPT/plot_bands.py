# Tight-binding model calculation of twisted bilayer graphene with arbitrary commensurate rotation angle.
from numpy import *
import matplotlib.pyplot as plt


def main():
    ymax = 1600
    ymin = -10

    plt.figure(1)
    enk = loadtxt('data/freq_disp.dat')*8.065544
    nk, nb = shape(enk)
    for ib in range(nb):
        plt.plot(enk[::-1,ib], 'r-')

    plt.ylim(ymin, ymax)
    gridlinespec = { 'color': 'darkgrey',
                    'linestyle': ':',
                    'linewidth': 0.5 }
    plt.grid(True, axis='x', **gridlinespec)
    plt.axhline(0,  **gridlinespec)
    plt.ylabel('Energy (eV)')

    # enk = loadtxt('enk.FK.dat')
    # nk, nb = shape(enk)
    # for ib in range(nb):
    #     if ib == 0:
    #         plt.plot(enk[:,ib], 'b-', lw=0.5, label="FK")
    #     else:
    #         plt.plot(enk[:,ib], 'b-', lw=0.5)

    # enk = loadtxt('enk.SK.FK.dat')
    # nk, nb = shape(enk)
    # for ib in range(nb):
    #     if ib == 0:
    #         plt.plot(enk[:,ib], 'k-', lw=0.5, label="SK+FK")
    #     else:
    #         plt.plot(enk[:,ib], 'k-', lw=0.5)

    plt.show()

    return

main()

