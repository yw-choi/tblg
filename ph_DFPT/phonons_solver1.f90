module phonons_solver1
  use mpi
  use kinds, only: DP
  use constants, only: TPI
  use diag, only: diagh_full

  use env, only : data_dir

  use lattice, only: &
    latt, relatt, N_LAYER, &
    na_uc, na_uc_l, atoms_uc, &
    na_sc, na_sc_l, atoms_sc, &
    sc_uc_map, isc_map, R_isc, &
    N_INTRA_SHELL, NA_INTRA_SHELL, intra_neighbors, &
    max_inter_neighbors, num_inter_neighbors, inter_neighbors  

  use io, only: &
    io_2d_cmplx, &
    io_2d_real_fmt

  implicit none

  public

contains

  ! Parallel over k-points.
  ! LAPACK is used
  subroutine solve_ph1 (nmode, nq, qpoints, mass, nfc, fc_pairs, fc, &
                        write_evq, band, freq)
    integer, intent(in) :: nmode, nq, nfc, fc_pairs(2,nfc)
    real(dp), intent(in) :: qpoints(3,nq), fc(3,3,nfc), mass
    logical, intent(in) :: write_evq, band
    real(dp), intent(out) :: freq(nmode,nq)

    integer :: recvcounts(0:nprocs-1), displs(0:nprocs-1), &
               iq_offset, nq_loc, iq_loc, iq
    integer :: ib, i, imode
    complex(dp), allocatable :: dynmat(:,:), evq(:,:)
    complex(dp), external :: zdotc
    real(dp) :: errmax, invnorm
    character(len=100) :: fn

    allocate(dynmat(nmode, nmode), evq(nmode,nmode))

    freq = 0.d0

    !! Distribution of q-points
    call distribute_indices(nq, nprocs, rank, displs, recvcounts)
    iq_offset = displs(rank)
    nq_loc = recvcounts(rank)

    if (master) then
      print *, "Distribution of q-points"

      do i = 0, nprocs-1
        print '(1x,a,3I5)', 'rank, iq_offset, nq_loc = ', i, displs(i), recvcounts(i)
      enddo
    endif
                   
    !! main loop
    do iq_loc = 1, nq_loc
      if (master) then
        print '(1x,a,I5,a,I5)', " >> Computing qpt # ", iq_loc, '/', nq_loc
      endif

      iq = iq_loc + iq_offset

      call build_dynmat(qpoints(1:3,iq), mass, nfc, fc_pairs, fc, nmode, dynmat, errmax)

      call diagh_full(nmode, dynmat, freq(:,iq) )

      evq = dynmat

      if (write_evq) then
        if (band) then
          write(fn, '(a,I0.6,a)') trim(data_dir)//'/evq_disp.', iq, '.dat'
        else
          write(fn, '(a,I0.6,a)') trim(data_dir)//'/evq.', iq, '.dat'
        endif
        call start_clock('io_evq')
        call io_2d_cmplx ( 'write', fn, nmode, nmode, evq )
        call stop_clock('io_evq')
      endif
    enddo
    
    if (master) then
      print *, "Finished diagonalization. Collecting eigenvalues.."
    endif

    call mpi_allgatherv(mpi_in_place, 0, mpi_datatype_null,&
                  freq, nmode*recvcounts, nmode*displs, &
                  mpi_double_precision, comm, mpierr)

    do iq = 1, nq
      do imode = 1, nmode
        if (freq(imode,iq).lt.0.d0) then
          freq(imode,iq) = -sqrt(-freq(imode,iq))
        else
          freq(imode,iq) = sqrt(freq(imode,iq))
        endif
      enddo
    enddo

  end subroutine solve_ph1

  subroutine build_dynmat(q, mass, nfc, fc_pairs, fc, nmode, dynmat, errmax)
    real(dp), intent(in) :: q(3), mass

    integer, intent(in) :: nfc, fc_pairs(2,nfc), nmode
    real(dp), intent(in) :: fc(3,3,nfc) 

    real(dp), intent(out) :: errmax
    complex(dp), intent(out) :: dynmat(nmode,nmode)

    integer :: ifc, ia_uc, ja_sc, ja_uc, m1, m2, il, jl, i, j
    real(dp) :: arg, err, R(3)
    complex(dp) :: fac

    call start_clock ('build_dynmat')

    dynmat = 0.d0

    do ifc = 1, nfc
      il = (fc_pairs(1,ifc)-1)/na_uc_l+1
      ia_uc = mod(fc_pairs(1,ifc)-1,na_uc_l)+1

      jl = (fc_pairs(2,ifc)-1)/na_sc_l+1
      ja_sc = mod(fc_pairs(2,ifc)-1,na_sc_l)+1
      ja_uc = sc_uc_map(ja_sc, jl)

      R = R_isc(1:3, isc_map(ja_sc, jl))
      arg = TPI*sum(q*R)
      fac = cmplx(cos(arg),sin(arg),kind=dp)

      do m1 = 1,3
        do m2 = 1,3
          i = 3*((il-1)*na_uc_l+ia_uc-1)+m1
          j = 3*((jl-1)*na_uc_l+ja_uc-1)+m2
          dynmat(i,j) = dynmat(i,j) + fc(m1,m2,ifc)*fac
        enddo
      enddo
    enddo

    errmax = -1.d0
    do i = 1, nmode
      do j = i, nmode
        err = abs(dynmat(i,j)-conjg(dynmat(j,i)))
        if (err.gt.errmax) then
          errmax = err
        endif
        dynmat(i,j) = 0.5d0*(dynmat(i,j)+conjg(dynmat(j,i)))
        dynmat(j,i) = conjg(dynmat(i,j))
      enddo
    enddo

    dynmat = dynmat / mass

    call stop_clock ('build_dynmat')
  end subroutine build_dynmat
end module phonons_solver1
