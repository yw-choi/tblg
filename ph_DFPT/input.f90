module input

  use mpi
  use constants, only: RYTOEV, ANGSTROM_AU, AMU_RY
  use env, only: data_dir
  use lattice, only: read_struct, struct_fn, M, N, alat, d_layer, shift
  use phonons, only: rcut_ph, write_evq, nq1, nq2, nq3, &
                     mass, nq, calc_disp, qpoints_disp_fn, &
                     write_evq_disp, vdw_type, paralleloverq, &
                     read_freq, calc_dos

  implicit none
  public

contains

  subroutine read_input

    logical :: exst
    logical, external :: makedirqq

    call printblock_start ( "Input parameters" )

    !! &input
    read_struct = .false.
    data_dir = "./data"

    M = 0
    N = 1
    alat = 2.46d0
    d_layer = 3.35d0
    shift(1:2) = 0.d0

    rcut_ph = 5.d0 * ANGSTROM_AU
    write_evq = .false.
    nq1 = -1
    nq2 = -1
    nq3 = -1
    mass = 12.0107 * AMU_RY

    calc_disp = .false.
    write_evq_disp = .false.
    qpoints_disp_fn = ''
    vdw_type = 1

    paralleloverq = .true.

    read_freq = .false.

    calc_dos = .true.

    namelist/input/ data_dir, read_struct, struct_fn, &
                    M, N, alat, d_layer, shift, &
                    rcut_ph, write_evq, nq1, nq2, nq3, &
                    mass, calc_disp, write_evq_disp, &
                    qpoints_disp_fn, vdw_type, &
                    paralleloverq, read_freq, calc_dos

    if (master) then
      read(5, input)
      write(6, input)

      inquire(directory=trim(data_dir), exist=exst)

      if (.not.exst) then
        if (.not.makedirqq(data_dir)) then
          call errore('read_input', 'Failed to create data directory', 1)
        endif
      endif

      alat = alat * ANGSTROM_AU
      d_layer = d_layer * ANGSTROM_AU
      rcut_ph = rcut_ph * ANGSTROM_AU
      mass = mass * AMU_RY
    endif

    call mpi_bcast(read_struct, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(data_dir, 100, mpi_character, 0, comm, mpierr)
    call mpi_bcast(M, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(N, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(alat, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(d_layer, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(shift, 2, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(rcut_ph, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(vdw_type, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(write_evq, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(paralleloverq, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(nq1, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(nq2, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(nq3, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(mass, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(calc_disp, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(qpoints_disp_fn, 100, mpi_character, 0, comm, mpierr)
    call mpi_bcast(write_evq_disp, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(read_freq, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(calc_dos, 1, mpi_logical, 0, comm, mpierr)

    nq = nq1 * nq2 * nq3

    call printblock_end

  end subroutine read_input

end module input
