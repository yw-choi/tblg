module io
  use mpi
  use kinds, only: DP
  use env, only: data_dir
  use constants, only: RYTOEV, ANGSTROM_AU
  implicit none
  public

contains

  subroutine io_2d_real_fmt ( task, fn, n1, n2, arr )
    character(len=*), intent(in) :: task, & ! 'read' or 'write'
                                    fn
    integer, intent(inout) :: n1, n2
    real(dp), intent(inout) :: arr(n1,n2)

    character(len=100) :: dummy
    integer :: n1_read, n2_read, i1, i2
    logical :: exst

    if (trim(task).eq.'read') then
      inquire(file=trim(fn), exist=exst)
      
      if (.not.exst) then
        call errore('io_2d_real_fmt', trim(fn)//' not found', 1)
      endif

      open(11, file=trim(fn), form="formatted", status="old")

      read(11, *) dummy, n1_read, n2_read

      do i2 = 1, n2
        read(11,*) arr(:,i2)
      enddo

      close(11)
      
    else if (trim(task).eq.'write') then
      open(11, file=trim(fn), form='formatted')
      write(11,'(a,2I)') "#", n1, n2
      do i2=1,n2
        do i1=1,n1
            write(11, '(F20.12,1x)', advance='no') arr(i1,i2) 
        enddo
        write(11, '(a)')
      enddo
      close(11)
    else
      call errore('io_2d_real_fmt', 'invalid task', 1)
    endif

  end subroutine io_2d_real_fmt

  subroutine io_2d_cmplx ( task, fn, n1, n2, arr )
    character(len=*), intent(in) :: task, & ! 'read' or 'write'
                                    fn
    integer, intent(in) :: n1, n2
    complex(dp), intent(inout) :: arr(n1,n2)
    
    integer :: i1, i2
    integer :: reclen
    logical :: exst

    inquire(iolength=reclen) arr(1:n1,1)
    inquire(file=trim(fn), exist=exst)
    
    if (trim(task).eq.'read') then
      
      if (.not.exst) then
        call errore('io_2d_cmplx', trim(fn)//' not found', 1)
      endif

      open(11, file=trim(fn), form="unformatted", status="old", access="direct", recl=reclen)
      do i2 = 1, n2
        read(11, rec=i2) arr(1:n1, i2)
      enddo
      close(11)
      
    else if (trim(task).eq.'write') then

      if (exst) then
        open(11, file=trim(fn), status="old")
        close(11, status="delete")
      endif

      open(11, file=trim(fn), form="unformatted", &
           status="unknown", access="direct", recl=reclen)
      do i2 = 1, n2
        write(11, rec=i2) arr(1:n1,i2)
      enddo
      close(11)

    else
      call errore('io_2d_cmplx', 'invalid task', 1)
    endif

  end subroutine io_2d_cmplx

  
end module io
