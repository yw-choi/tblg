module phonons_fc
  use mpi
  use matrix_inversion, only: invmat
  use constants, only: AMU_RY, RYTOEV, PI, ANGSTROM_AU
  use kinds, only: DP
  use sort, only: hpsort_eps

  use lattice, only: &
    N, M, latt, relatt, N_LAYER, latt_puc, nsc1, nsc2, lsc1, lsc2, &
    N_INTRA_SHELL, NA_INTRA_SHELL, intra_neighbors, &
    na_uc, na_uc_l, atoms_uc, &
    na_sc, na_sc_l, atoms_sc, &
    sc_uc_map, isc_map, R_isc, &
    max_inter_neighbors, num_inter_neighbors, inter_neighbors, &
    ia_offset, na_loc, recvcounts, displs, ncell, cells, cellmap1, cellmap2, theta

  implicit none

  public
  
  ! K. Zhang and E. B. Tadmor, Journal of the Mechanics and Physics of Solids 112, 225 (2018). 
  !! LJ potential parameters
  real(dp), parameter :: LJ_eps = 2.39d-3 / RYTOEV, &
                         LJ_sig = 3.41 * ANGSTROM_AU

  !! KC potential parameters
  real(dp), parameter :: &
    C0 = 15.71d-3 / RYTOEV, &
    C2 = 12.29d-3 / RYTOEV, &
    C4 = 4.933d-3 / RYTOEV, &
    C  = 3.030d-3 / RYTOEV, &
    delta = 0.578d0 * ANGSTROM_AU, &
    lambda = 3.629d0 / ANGSTROM_AU, &
    A_KC = 10.238d-3 / RYTOEV, &
    z0 = 3.34d0 * ANGSTROM_AU

contains 

  subroutine compute_fc (rcut_ph, vdw_type, &
                         nfc, fc_pairs, fc)
    real(dp), intent(in) :: rcut_ph
    integer, intent(in) :: vdw_type

    integer, intent(out) :: nfc
    integer, allocatable, intent(out) :: fc_pairs(:,:)
    real(dp), allocatable, intent(out) :: &
      fc(:,:,:)
                                    
    integer :: nfc_intra, nfc_inter

    real(dp) :: at1(3), at2(3), dx(3), dist
    integer :: ifc_loc, ia_uc, ishell, ia_shell, ja_sc, ja_uc, il2, ifc2, ia_uc_l, iproc

    real(dp) :: costh, sinth, fac

    real(dp), allocatable :: fc_diag(:,:,:,:)
    real(dp), allocatable :: normals(:,:,:)
    real(dp) :: val

    integer :: nfc_intra_loc, nfc_inter_loc, nfc_loc
    integer, allocatable :: fc_pairs_loc(:,:)
    real(dp), allocatable :: fc_loc(:,:,:)

    integer :: recvcounts_nfc(0:nprocs-1), displs_nfc(0:nprocs-1)

    integer :: nq1_fc, nq2_fc
    real(dp), allocatable :: fc_MLG(:,:,:,:,:,:)
    integer :: inb, il, ia, ja, iq1, iq2, i1, i2, n1, n2, ia_puc, ja_puc, i, &
               isc, isc1, isc2, m1, m2
    integer :: R_ia_frac_puc(2), R_ja_frac_puc(2), R_sc(2), &
               iu, icell, ifc, jcell, ja_cell(2)
    real(dp) :: invlatt(2,2), rot(3,3,2), at2_frac(3), at2_frac_uc(3), &
                R_ja_cart(2), R_ja_frac_sc(2), x(2)

    real(dp) :: test1(2), test2(2)

    rot(1,:,1) = (/  cos(theta/2),  sin(theta/2), 0.0d0 /)
    rot(2,:,1) = (/ -sin(theta/2),  cos(theta/2), 0.0d0 /)
    rot(3,:,1) = (/ 0.0d0, 0.0d0, 1.0d0 /)
    rot(1,:,2) = (/  cos(theta/2), -sin(theta/2), 0.0d0 /)
    rot(2,:,2) = (/  sin(theta/2),  cos(theta/2), 0.0d0 /)
    rot(3,:,2) = (/ 0.0d0, 0.0d0, 1.0d0 /)

    call invmat (2, latt(1:2,1:2), invlatt)
    call read_fc_MLG (nfc_intra, nq1_fc, nq2_fc, fc_MLG)

    if (vdw_type.eq.-1) then !! no interlayer coupling
      nfc_inter = 0
    else
      nfc_inter = sum(num_inter_neighbors(:, :))
    endif

    nfc = nfc_intra + nfc_inter + na_uc ! including diagonal terms as computed by ASR

    nfc_intra_loc = nfc_intra / na_uc_l * na_loc
    if (vdw_type.eq.-1) then
      nfc_inter_loc = 0
    else
      nfc_inter_loc = sum(num_inter_neighbors(ia_offset+1:ia_offset+na_loc,:))
    endif
    nfc_loc = nfc_intra_loc + nfc_inter_loc + na_loc * N_LAYER

    allocate(fc_pairs(2,nfc), fc(3,3,nfc))
    allocate(fc_pairs_loc(2,nfc_loc), fc_loc(3,3,nfc_loc))
    fc = 0
    fc_pairs = 0 
    fc_loc = 0
    fc_pairs_loc = 0 

    !! computing fc
    ifc_loc = 0
    !! intralayer components
    do il = 1, N_LAYER
      do ia_uc = ia_offset+1, ia_offset+na_loc
        icell = int((ia_uc-1)/2)+1
        ia_puc = mod(ia_uc-1, 2)+1
        R_ia_frac_puc = cells(1:2, icell, il)
        at1 = atoms_uc(1:3, ia_uc, il)

        ! loop over MLG supercell 
        do iq1 = 0, nq1_fc-1
          if (iq1.gt.nq1_fc/2) then
            i1 = iq1 - nq1_fc
          else
            i1 = iq1
          endif

          do iq2 = 0, nq2_fc-1
            if (iq2.gt.nq2_fc/2) then
              i2 = iq2 - nq2_fc
            else
              i2 = iq2
            endif

            R_ja_frac_puc = R_ia_frac_puc + (/ i1, i2 /)

            R_ja_cart = matmul(latt_puc(:,:,il), R_ja_frac_puc)

            R_ja_frac_sc = matmul(invlatt, R_ja_cart)

            R_sc = floor(R_ja_frac_sc+1.0d-6)
            x = R_ja_frac_sc - R_sc

            if (il.eq.1) then
              ja_cell = (/ nint(x(1)*N-x(2)*M), nint(x(1)*M+x(2)*(N+M)) /)
            else
              ja_cell = (/ nint(x(1)*M-x(2)*N), nint(x(1)*N+x(2)*(M+N)) /)
            endif

            !! DBG
            ! test1 = matmul(latt(1:2,1:2), R_sc) + matmul(latt_puc(:,:,il), ja_cell)
            ! test2 = R_ja_cart

            ! if (norm2(test1-test2).gt.1d-8) then
            !   print *, R_ja_frac_puc
            !   print *, R_ja_cart
            !   print *, R_ja_frac_sc
            !   print *, R_sc
            !   print *, x
            !   print *, ja_cell
            !   stop
            ! endif

            if (il.eq.1) then
              jcell = cellmap1(M+ja_cell(1), ja_cell(2))
            else
              jcell = cellmap2(N+ja_cell(1), ja_cell(2))
            endif

            ! test1 = matmul(latt(1:2,1:2), R_sc)
            if (R_sc(1).lt.0) then
              R_sc(1) = R_sc(1) + nsc1
            endif

            if (R_sc(2).lt.0) then
              R_sc(2) = R_sc(2) + nsc2
            endif

            isc = nsc2*R_sc(1) + R_sc(2) + 1
            ! test2 = matmul(latt(1:2,1:2), R_isc(1:2, isc))

            ! if (norm2(test1-test2).gt.1d-8) then
            !   print *, R_ja_frac_puc
            !   print *, R_ja_cart
            !   print *, R_ja_frac_sc
            !   print *, R_sc
            !   print *, x
            !   print *, ja_cell
            !   stop
            ! endif
            do ja_puc = 1,2
              ja_uc = (jcell-1)*2 + ja_puc 
              ja_sc = (isc-1)*na_uc_l + ja_uc

              ifc_loc = ifc_loc + 1
              fc_pairs_loc(1, ifc_loc) = (il-1)*na_uc_l+ia_uc
              fc_pairs_loc(2, ifc_loc) = (il-1)*na_sc_l+ja_sc

              fc_loc(1:3,1:3,ifc_loc) = matmul(rot(:,:,il), matmul(fc_MLG(iq1+1,iq2+1,ia_puc,ja_puc,:,:), transpose(rot(:,:,il))))

            enddo

          enddo ! iq2
        enddo ! iq1
      enddo ! ia_uc
    enddo ! il

    if (vdw_type.gt.0) then
      ! Compute normal vectors for each atom,
      ! which is required to calculate KC potentials.
      if (vdw_type.eq.2) then
        allocate(normals(3,na_uc_l,N_LAYER))
        normals = 0.d0
        do il = 1, N_LAYER
          do ia_uc = ia_offset+1, ia_offset+na_loc
            call compute_normal(il, ia_uc, intra_neighbors(1:3, 1, ia_uc, il), normals(1:3,ia_uc,il))
          enddo
          call mpi_allgatherv(mpi_in_place, 0, mpi_datatype_null,&
                        normals(:,:,il), 3*recvcounts, 3*displs, &
                        mpi_double_precision, comm, mpierr)
        enddo
      endif

      !! interlayer components
      do il = 1, N_LAYER
        il2 = 3-il
        do ia_uc = ia_offset+1, ia_offset+na_loc
          at1 = atoms_uc(1:3, ia_uc, il)

          do inb = 1, num_inter_neighbors(ia_uc, il)
            ja_sc = inter_neighbors(inb, ia_uc, il)
            ja_uc = sc_uc_map(ja_sc, il2)
            at2 = atoms_sc(1:3, ja_sc, il2)
          
            ifc_loc = ifc_loc + 1

            dx = at2 - at1
            dist = norm2(dx)

            if (vdw_type.eq.1) then
              !! LJ
              fac =  -4.d0*LJ_eps*(156.d0*LJ_sig**12/dist**16-42.d0*LJ_sig**6/dist**10)

              do m1 = 1,3
                do m2 = 1,3
                  fc_loc(m1,m2, ifc_loc) = fac*dx(m1)*dx(m2)
                enddo
              enddo

            else
              !! KC
              do m1 = 1, 3
                do m2 = 1, 3
                  call fc_KC(m1,m2,at1, at2, normals(:,ia_uc,il), normals(:,ja_uc,il2), val)

                  fc_loc(m1,m2,ifc_loc) = val
                enddo
              enddo
            endif
            fc_pairs_loc(1, ifc_loc) = (il-1)*na_uc_l+ia_uc
            fc_pairs_loc(2, ifc_loc) = (il2-1)*na_sc_l+ja_sc
          enddo
        enddo
      enddo

      if (nfc_intra_loc+nfc_inter_loc.ne.ifc_loc) then
        print *, ifc_loc
        print *, nfc_intra_loc+nfc_inter_loc
        call errore('compute_fc', 'number of fc does not match', 1)
      endif
    endif

    allocate(fc_diag(3,3,na_loc,N_LAYER))
    fc_diag = 0.d0
    do ifc2 = 1, nfc_intra_loc + nfc_inter_loc 

      il = (fc_pairs_loc(1, ifc2)-1)/na_uc_l+1
      ia_uc_l = mod(fc_pairs_loc(1, ifc2)-1, na_uc_l)+1

      fc_diag(:,:,ia_uc_l-ia_offset,il) = fc_diag(:,:,ia_uc_l-ia_offset,il) - fc_loc(:,:,ifc2)
    enddo

    ifc_loc = nfc_intra_loc + nfc_inter_loc 
    do il = 1, N_LAYER
      do ia_uc = ia_offset+1, ia_offset+na_loc

        ifc_loc = ifc_loc + 1
        fc_pairs_loc(1, ifc_loc) = (il-1)*na_uc_l+ia_uc
        fc_pairs_loc(2, ifc_loc) = (il-1)*na_sc_l+ia_uc
        fc_loc(:,:,ifc_loc) = fc_diag(:,:,ia_uc-ia_offset,il)
      enddo
    enddo

    if (nfc_loc.ne.ifc_loc) then
      print *, ifc_loc
      print *, nfc_loc
      call errore('compute_fc', 'number of fc does not match2', 1)
    endif

    call mpi_allgather(nfc_loc, 1, mpi_integer, &
      recvcounts_nfc, 1, mpi_integer, comm, mpierr)

    displs_nfc = 0
    do iproc = 1, nprocs-1
      displs_nfc(iproc) = displs_nfc(iproc-1)+recvcounts_nfc(iproc-1)
    enddo

    call mpi_allgatherv(fc_pairs_loc, 2*nfc_loc, mpi_integer,&
                  fc_pairs, 2*recvcounts_nfc, 2*displs_nfc, &
                  mpi_integer, comm, mpierr)
    call mpi_allgatherv(fc_loc, 9*nfc_loc, mpi_double_precision,&
                  fc, 9*recvcounts_nfc, 9*displs_nfc, &
                  mpi_double_precision, comm, mpierr)

  end subroutine compute_fc

  subroutine fc_KC ( m1, m2, x_i, x_j, n_i, n_j, fc )
    integer, intent(in) :: m1, m2
    real(dp), intent(in) :: x_i(3), x_j(3), n_i(3), n_j(3)
    real(dp), intent(out) :: fc
    real(dp), parameter :: h = 1d-6

    real(dp) :: x_ih(3), x_jh(3), v1, v2, v3, v4

    x_ih = x_i
    x_ih(m1) = x_ih(m1) + h
    x_jh = x_j
    x_jh(m2) = x_jh(m2) + h
    call V_KC(x_ih, x_jh, n_i, n_j, v1)
    x_ih = x_i
    x_ih(m1) = x_ih(m1) - h
    x_jh = x_j
    x_jh(m2) = x_jh(m2) - h
    call V_KC(x_ih, x_jh, n_i, n_j, v2)
    x_ih = x_i
    x_ih(m1) = x_ih(m1) + h
    x_jh = x_j
    x_jh(m2) = x_jh(m2) - h
    call V_KC(x_ih, x_jh, n_i, n_j, v3)
    x_ih = x_i
    x_ih(m1) = x_ih(m1) - h
    x_jh = x_j
    x_jh(m2) = x_jh(m2) + h
    call V_KC(x_ih, x_jh, n_i, n_j, v4)

    fc = (v1+v2-v3-v4)/(4.d0*h*h)

  end subroutine

  subroutine V_KC ( x_i, x_j, n_i, n_j, v )
    real(dp), intent(in) :: x_i(3), x_j(3), n_i(3), n_j(3)
    real(dp), intent(out) :: v

    real(dp) :: r_ij(3), r, rho_ij, rho_ji

    r_ij = x_i - x_j
    r = norm2(r_ij)

    rho_ij = sqrt(r*r - (dot_product(n_i, r_ij))**2)
    rho_ji = sqrt(r*r - (dot_product(n_j, r_ij))**2)

    v = exp(-lambda*(r-z0)) &
        * (C+f_kc((rho_ij/delta)**2)+f_kc((rho_ji/delta)**2)) &
        - A_KC * (z0/r)**6

  end subroutine V_KC

  real(dp) function f_kc(rd2)
    real(dp), intent(in) :: rd2
    f_kc = exp(-rd2)*(c0+c2*rd2+c4*rd2*rd2)
  end function f_kc

  subroutine rotation_matrix(rot, costh, sinth)
    real(dp), intent(in) :: costh, sinth
    real(dp), intent(out) :: rot(3,3)
    
    rot(1,:) = (/  costh,  sinth, 0.d0 /)
    rot(2,:) = (/ -sinth,  costh, 0.d0 /)
    rot(3,:) = (/ 0.d0, 0.d0, 1.d0 /)
  end subroutine rotation_matrix

  subroutine compute_normal(il, ia_uc, neighbors, normal)
    integer, intent(in) :: il, ia_uc, neighbors(3)
    real(dp), intent(out) :: normal(3)
    normal = (/0.d0,0.d0,1.0d0/)

!     real(dp) :: at_i(3), dx(3,3), c(3,3)

!     integer :: ia_uc_g, i_uc(3), i_uc_g(3), i

!     ia_uc_g = na_uc_l*(il-1)+ia_uc
!     at_i = atoms_uc(1:3, ia_uc, il) 

!     do i = 1,3
!       i_uc(i) = sc_uc_map(neighbors(i),il)
!       i_uc_g(i) = na_uc_l*(il-1)+i_uc(i)
!       dx(1:3,i) = atoms_sc(1:3, neighbors(i), il) - at_i
!     enddo

!     call cross_prod( dx(1:3, 1), dx(1:3, 2), c(1:3,1) )
!     call cross_prod( dx(1:3, 2), dx(1:3, 3), c(1:3,2) )
!     call cross_prod( dx(1:3, 3), dx(1:3, 1), c(1:3,3) )

!     if (c(3,1).lt.0.d0) then
!       c = -c
!     endif

!     do i = 1,3
!       c(1:3,i) = c(1:3,i)/norm2(c(1:3,i))
!     enddo

!     do i = 1,3
!       normal(1:3) = normal(1:3) + c(1:3,i)
!     enddo

!     normal = normal/norm2(normal)
  end subroutine compute_normal

  subroutine cross_prod(a,b,c)
    use kinds, only: DP
    real(dp) :: a(3), b(3), c(3)
    c(1) = a(2)*b(3)-a(3)*b(2)
    c(2) = a(3)*b(1)-a(1)*b(3)
    c(3) = a(1)*b(2)-a(2)*b(1)
  end subroutine cross_prod

  subroutine read_fc_MLG (nfc_intra, nq1_fc, nq2_fc, fc_MLG)
    integer, intent(out) :: nfc_intra, nq1_fc, nq2_fc
    real(dp), allocatable, intent(out) :: fc_MLG(:,:,:,:,:,:)

    character(len=200) :: file_fc

    integer :: iu, dummyi, ix1,ix2,ia1,ia2,iq1,iq2,iq3,i1,i2,i3
    character(len=100) :: dummy
    real(dp) :: mass

    iu = 11
    file_fc = "./fc"
    
    if (rank.eq.0) then
      open(iu, file=trim(file_fc), form="formatted")
      read(iu,*) ! header
      read(iu,*) ! a1 
      read(iu,*) ! a2
      read(iu,*) ! a3
      read(iu,*) dummyi, dummy, mass
      read(iu,*) ! C1
      read(iu,*) ! C2
      read(iu,*) ! F
      read(iu,*) nq1_fc, nq2_fc, dummyi
      allocate(fc_MLG(nq1_fc,nq2_fc,2,2,3,3))
      fc_MLG = 0.d0
      do ix1 = 1,3
        do ix2 = 1,3
          do ia1 = 1,2
            do ia2 = 1,2
              read(iu,*)
              do iq2 = 1,nq1_fc
                do iq1 = 1,nq1_fc
                  read(iu,*) i1,i2,i3, &
                    fc_MLG(iq1,iq2,ia1,ia2,ix1,ix2)
                enddo
              enddo
            enddo
          enddo
        enddo
      enddo

      close(iu)
    endif

    call mpi_bcast(nq1_fc, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(nq2_fc, 1, mpi_integer, 0, comm, mpierr)
    if (rank.ne.0) allocate(fc_MLG(nq1_fc,nq2_fc,2,2,3,3))
    call mpi_bcast(fc_MLG, 3*3*2*2*nq1_fc*nq2_fc, &
      mpi_double_precision, 0, comm, mpierr)

    nfc_intra = na_uc_l*N_LAYER*nq1_fc*nq2_fc*2
  end subroutine read_fc_MLG 

end module phonons_fc
