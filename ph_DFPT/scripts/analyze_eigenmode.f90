program analyze_eigenmode
  implicit none

  integer :: nmode, iq, iq_read, ia, m1, imode

  character(len=100) :: fn
  double complex, allocatable :: evq(:,:)

  iq = 1
  write(fn, '(a,I0.5,a)') 'evq_disp.', iq, '.dat'

  open(11, file=trim(fn), form="unformatted")
  read(11) iq_read, nmode
  if (iq.ne.iq_read) then
    print *, "iq mismatch", iq, iq_read
    stop
  endif
  allocate(evq(nmode,nmode))

  do imode = 1, nmode
    read(11) evq(:,imode)
  enddo

  close(11)

  do imode = 1, nmode

    print *, "mode ", imode

    do ia = 1, nmode/3

      write(6, '(I6)', advance="no") ia

      do m1 = 1, 3
        write(6, '(F10.6)', advance="no") real(evq(3*(ia-1)+m1, imode))
      enddo

      write(6, '(3x)', advance="no") 

      do m1 = 1, 3
        write(6, '(F10.6)', advance="no") imag(evq(3*(ia-1)+m1, imode))
      enddo

      write(6, *)
    enddo
  enddo
end program analyze_eigenmode
