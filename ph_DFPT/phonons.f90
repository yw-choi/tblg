module phonons
  use mpi
  use env, only: data_dir
  use kinds, only: DP
  use constants, only: RY_TO_CMM1, RYTOEV, PI

  use kgrid, only: &
    generate_uniform_grid, &
    read_kpoints_band
  
  use phonons_fc, only: &
    compute_fc

  use io, only: &
    io_2d_real_fmt

  use phonons_solver1, only: &
    solve_ph1

  use phonons_solver_elpa, only: &
    solve_ph_elpa

  use lattice, only: &
    latt, relatt, N_LAYER, &
    na_uc, na_uc_l, atoms_uc, &
    na_sc, na_sc_l, atoms_sc, &
    sc_uc_map, isc_map, R_isc, &
    N_INTRA_SHELL, NA_INTRA_SHELL, intra_neighbors, &
    max_inter_neighbors, num_inter_neighbors, inter_neighbors, &
    ia_offset, na_loc, recvcounts, displs

  use ktetra, only: &
    tetra, tetra_type, tetra_init, tetra_dos_t, &
    ntetra, opt_tetra_init, opt_tetra_dos_t, &
    tetra_weights, opt_tetra_weights

  implicit none
  public

  real(dp) :: rcut_ph, mass
  logical :: write_evq, paralleloverq, read_freq
  integer :: nq, nq1, nq2, nq3, vdw_type

  integer :: nmode
  real(dp), allocatable :: qpoints(:,:), & ! q-points(3,nq) in frac. coordinate
                           qpoints_cart(:,:), &  ! q-points in cart. coordinate (2pi/bohr unit)
                           freq(:,:)       ! freq(nmode,nq) phonon frequencies in Ry
                           

  logical :: calc_disp, write_evq_disp
  character(len=100) :: qpoints_disp_fn
  integer :: nq_disp
  real(dp), allocatable :: qpoints_disp(:,:), freq_disp(:,:)

  !! Force constants
  integer :: nfc
  real(dp), allocatable:: &
    fc(:,:,:) ! fc_intra(3,3,nfc)

  integer, allocatable :: &
    fc_pairs(:,:)       ! fc_pairs(2,nfc)

  !! DOS
  logical :: &
    calc_dos

  real(dp) :: dos_emin, dos_emax
  integer :: dos_ne
  real(dp), allocatable :: dos(:,:) ! dos(2,dos_ne) energy (Ry), DOS (modes/Ry)
contains

  subroutine setup_qgrid

    integer :: iq
    call start_clock ('qgrid')
    call printblock_start ('q-point grid')

    !! qgrid
    if (master) then
      print '(1x,a,3I5)', &
        "Generating uniform q-grid with nq1, nq2, nq3 = ", nq1, nq2, nq3
    endif

    allocate(qpoints(3,nq))
    call generate_uniform_grid(nq1, nq2, nq3, qpoints)

    if (calc_disp) then
      if (master) then
        print *, "Reading q-point list for dispersion calculation."
        call read_kpoints_band ( qpoints_disp_fn, nq_disp, qpoints_disp )
      endif

      call mpi_bcast(nq_disp, 1, mpi_integer, 0, comm, mpierr)
      if (.not.master) then
        allocate(qpoints_disp(3,nq_disp))
      endif
      call mpi_bcast(qpoints_disp, 3*nq_disp, mpi_double_precision, 0, comm, mpierr)
    endif

    allocate(qpoints_cart(3,nq))
    do iq = 1, nq
      qpoints_cart(:,iq) = matmul(relatt, qpoints(:,iq))/(2*PI)
    enddo

    if (nq.lt.4) then
      call errore('setup_qgrid', &
        'at least 4 q-points are required for tetrahedron method', 1)
    endif

    call setup_tetra

    nmode = 3*na_uc
    allocate(freq(nmode,nq))

    call printblock_end
    call stop_clock ('qgrid')
  end subroutine setup_qgrid

  subroutine setup_fc
    call start_clock('fc')
    call printblock_start ('Computing force constants')
    call compute_fc (rcut_ph, vdw_type, nfc, fc_pairs, fc)
    if (master) then
      if (vdw_type.eq.1) then
        print *, "vdW interaction potential type = LJ"
      else
        print *, "vdW interaction potential type = KC"
      endif
      print *, "Number of force constants (nfc) = ", nfc
    endif
    call stop_clock('fc')
    call printblock_end
  end subroutine setup_fc

  subroutine solve_ph
    character(len=100) :: fn

    call printblock_start ('Solving phonons')
    call start_clock('solve_ph')

    if (paralleloverq) then
      call solve_ph1 (nmode, nq, qpoints, mass, &
                      nfc, fc_pairs, fc, &
                      write_evq, .false., freq)
    else
      call solve_ph_elpa (nmode, nq, qpoints, mass, &
                          nfc, fc_pairs, fc, &
                          write_evq, .false., freq)
    endif

    if (master) then
      write(fn, '(a)') trim(data_dir)//'/freq.dat'
      call io_2d_real_fmt ('write', fn, nmode, nq, freq)
    endif

    call printblock_end
    call stop_clock ('solve_ph')
  end subroutine solve_ph

  subroutine compute_dos
    integer :: ie, ie_loc
    character(len=100) :: fn
    integer :: recvcounts(0:nprocs-1), displs(0:nprocs-1), ie_offset, ne_loc

    integer, parameter :: dos_ne = 1000

    call start_clock('compute_dos')
    call printblock_start ('Computing Density of States')

    allocate(dos(2,dos_ne))
    !! Distribution over energy
    call distribute_indices(dos_ne, nprocs, rank, displs, recvcounts)
    ie_offset = displs(rank)
    ne_loc = recvcounts(rank)

    dos_emin = 0.d0
    dos_emax = maxval(freq)*1.1

    do ie_loc = 1, ne_loc
      ie = ie_loc + ie_offset

      dos(1,ie) = dos_emin + (ie-1)*(dos_emax-dos_emin)/(dos_ne-1)

      call dos_at_e( nmode, nq, freq, dos(1,ie), dos(2,ie) )
    enddo

    call mpi_allgatherv(mpi_in_place,0,mpi_datatype_null,&
                  dos, 2*recvcounts, 2*displs, &
                  mpi_double_precision,comm,mpierr)

    if (master) then
      write(fn, '(a)') trim(data_dir)//'/phdos.dat'
      print '(a,a)', "Saving phonon DOS to ", trim(fn)

      open(11, file=trim(fn), form="formatted", status="unknown")
      write(11,*) "# energy (meV)     DOS (modes/meV)"
      do ie = 1, dos_ne
        write(11, *) dos(1,ie)*RYTOEV*1e3, dos(2,ie)/RYTOEV*1e-3
      enddo
      close(11)
    endif
    call mpi_barrier(comm, mpierr)

    call stop_clock('compute_dos')
    call printblock_end

  end subroutine compute_dos

  subroutine setup_tetra

    integer :: nsym, s(3,3,48), t_rev(48)
    logical :: time_reversal

    integer :: i, k1, k2, k3, kstep

    call start_clock('setup_tetra')

    tetra_type = 0

    ! no symmetry imposed
    nsym = 1
    s = 0
    do i = 1, 3
      s(i,i,:) = 1
    enddo
    time_reversal = .false.
    t_rev = 0
    k1 = 0 ! no shifted grid
    k2 = 0
    k3 = 0
    kstep = 1

    if(tetra_type == 0) then
      if (master) then
        print *, "Linear tetrahedron method with Bloechl correction used"
      endif
      CALL tetra_init ( nsym, s, time_reversal, t_rev, latt, relatt/(2*PI), nq, &
           k1,k2,k3, nq1,nq2,nq3, nq, qpoints_cart)
    ELSE
      if (master) then
        IF(tetra_type == 1) THEN 
           print *, "Linear tetrahedron method is used"
        ELSE
           print *, "Optimized tetrahedron method used"
        END IF
      endif
      CALL opt_tetra_init ( nsym, s, time_reversal, t_rev, latt, relatt/(2*PI), nq, &
           k1,k2,k3, nq1,nq2,nq3, nq, qpoints_cart, kstep)
    END IF

    call stop_clock('setup_tetra')

  end subroutine setup_tetra

  subroutine compute_disp
    character(len=100) :: fn

    call printblock_start ('Computing dispersion')
    call start_clock ('compute_disp')

    allocate(freq_disp(nmode,nq_disp))
    if (paralleloverq) then
      call solve_ph1 (nmode, nq_disp, qpoints_disp, mass, &
                      nfc, fc_pairs, fc, &
                      write_evq_disp, .true., freq_disp)
    else
      call solve_ph_elpa (nmode, nq_disp, qpoints_disp, mass, &
                          nfc, fc_pairs, fc, &
                          write_evq_disp, .true., freq_disp)
    endif

    if (master) then
      write(fn, '(a)') trim(data_dir)//'/freq_disp.dat'
      call io_2d_real_fmt ('write', fn, nmode, nq_disp, freq_disp)
    endif

    call printblock_end
    call stop_clock ('compute_disp')
  end subroutine compute_disp

  ! DOS per spin at energy
  subroutine dos_at_e ( nbnd, nks, et, energy, dos )
    integer, intent(in) :: nbnd, nks
    real(dp), intent(in) :: et(nbnd, nks), energy
    real(dp), intent(out) :: dos

    real(dp) :: dosval(2)
    integer :: nspin
    nspin = 1

    if (tetra_type == 0) then
      call tetra_dos_t( et, nspin, nbnd, nks, energy, dosval)
    else
      call opt_tetra_dos_t( et, nspin, nbnd, nks, energy, dosval)
    end if
    dos = dosval(1)*0.5d0 ! DOS per spin == DOS for phonon
  end subroutine dos_at_e
end module phonons

