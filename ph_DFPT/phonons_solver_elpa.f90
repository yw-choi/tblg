module phonons_solver_elpa
  use mpi
  use kinds, only: DP
  use constants, only: TPI
  use env, only: data_dir

  use lattice, only: &
    latt, relatt, N_LAYER, &
    na_uc, na_uc_l, atoms_uc, &
    na_sc, na_sc_l, atoms_sc, &
    sc_uc_map, isc_map, R_isc, &
    N_INTRA_SHELL, NA_INTRA_SHELL, intra_neighbors, &
    max_inter_neighbors, num_inter_neighbors, inter_neighbors  

  USE IO, ONLY: &
    IO_2D_REAL_FMT

  USE ELPA

  IMPLICIT NONE
  PRIVATE

  PUBLIC :: SOLVE_PH_ELPA

  !! SCALAPACK variables
  INTEGER :: &
    CONTEXT, NPROW, NPCOL, NB, &
    MYROW, MYCOL, INFO,        &
    DESCA( 50 ), DESCZ( 50 ),  &
    N, NUMR, NUMC
  INTEGER, EXTERNAL :: NUMROC

  !! Local matrices
  COMPLEX(DP), ALLOCATABLE :: A(:,:), Z(:,:)
  REAL(DP), ALLOCATABLE :: W(:)

contains

  subroutine solve_ph_elpa (nmode, nq, qpoints, mass, nfc, fc_pairs, fc, &
                        write_evq, band, freq)
    INTEGER, INTENT(IN) :: NMODE, NQ, NFC, FC_PAIRS(2,NFC)
    REAL(DP), INTENT(IN) :: QPOINTS(3,NQ), FC(3,3,NFC), MASS
    LOGICAL, INTENT(IN) :: WRITE_EVQ, BAND
    REAL(DP), INTENT(OUT) :: FREQ(NMODE,NQ)

    INTEGER :: IQ, IMODE

    CLASS(ELPA_T), POINTER :: ELPA
    INTEGER :: SUCCESS

    CHARACTER(LEN=100) :: TIMESTR, FN

    COMPLEX(DP), ALLOCATABLE :: WORK(:)

    N  = NMODE

    call setup_scalapack

    allocate(work(nb)) ! workspace for pzlawrite

    IF (ELPA_INIT(20180525) /= ELPA_OK) THEN
      CALL ERRORE("SOLVE_TB_ELPA", "ELPA API version not supported", 1)
    ENDIF

    ELPA => ELPA_ALLOCATE()

    call ELPA%SET("na", N, success)
    call ELPA%SET("nev", N, success)
    call ELPA%SET("local_nrows", NUMR, success)
    call ELPA%SET("local_ncols", NUMC, success)
    call ELPA%SET("nblk", NB, success)
    call ELPA%SET("mpi_comm_parent", comm, success)
    call ELPA%SET("process_row", MYROW, success)
    call ELPA%SET("process_col", MYCOL, success)

    success = ELPA%SETUP()

    call ELPA%SET("solver", elpa_solver_1stage, success)

    freq = 0.d0

    !! main loop
    do iq = 1, nq
      WRITE(TIMESTR, "(a,i5,a,i5)") "Computing qpt # ", iq, '/', nq
      call timestamp (TIMESTR)

      call build_dynmat(qpoints(1:3,iq), mass, nfc, fc_pairs, fc, nmode, A)

      !! diagonalize
      call mpi_barrier(comm, mpierr) ! for correct timings only
      call start_clock('diag_elpa')
      call elpa%eigenvectors(A, W, Z, success)
      call stop_clock('diag_elpa')

      FREQ(1:NMODE,IQ) = W(1:N)

      IF (WRITE_EVQ) THEN
        if (band) then
          write(fn, '(a,I0.6,a)') trim(data_dir)//'/evq_disp.', iq, '.dat'
        else
          write(fn, '(a,I0.6,a)') trim(data_dir)//'/evq.', iq, '.dat'
        endif
        call start_clock('io_evq')
        call WRITE_2D_CMPLX_PARALLEL ( fn, nmode, 1, nmode, numr, numc, z )
        call stop_clock('io_evq')
      ENDIF

    enddo ! iq

    if (master) then
      print *, "Finished Diagonalization."
    endif

    call elpa_deallocate(elpa)
    call elpa_uninit()
    call deallocate_scalapack

    do iq = 1, nq
      do imode = 1, nmode
        if (freq(imode,iq).lt.0.d0) then
          freq(imode,iq) = -sqrt(-freq(imode,iq))
        else
          freq(imode,iq) = sqrt(freq(imode,iq))
        endif
      enddo
    enddo

  end subroutine solve_ph_elpa

  subroutine setup_scalapack
    implicit none

    integer :: info

    IF (N.GT.32*NPROCS) THEN
      nb = 32 ! default
    ELSE
      nb = (N-1)/NPROCS + 1
    ENDIF

    nprow = nint(sqrt(dble(nprocs)))

    do while (nprow.gt.0 .and. mod(nprocs, nprow).ne.0) 
      nprow = nprow - 1
    enddo

    if (nprow.eq.0) then
      call errore("scalapack", "nprow cannot be determined", 1)
    endif

    npcol = nprocs/nprow

    call blacs_get( -1, 0, context )
    call blacs_gridinit( context, 'r', nprow, npcol )
    call blacs_gridinfo( context, nprow, npcol, myrow, mycol )

    numr = numroc( n, nb, myrow, 0, nprow )
    numc = numroc( n, nb, mycol, 0, npcol )

    allocate(a(numr, numc))
    allocate(z(numr, numc))
    allocate(w(n))

    call descinit( desca, n, n, nb, nb, 0, 0, context, numr, info )
    if (info.ne.0) then
      print *, info
      call errore("blacs", "desca failed", info)
    endif
    call descinit( descz, n, n, nb, nb, 0, 0, context, numr, info )
    if (info.ne.0) then
      print *, info
      call errore("blacs", "descz failed", info)
    endif
    
  end subroutine setup_scalapack

  subroutine deallocate_scalapack
    call blacs_gridexit(context)
    deallocate(w, a, z)
  end subroutine deallocate_scalapack

  subroutine build_dynmat(q, mass, nfc, fc_pairs, fc, nmode, dynloc)
    real(dp), intent(in) :: q(3), mass

    integer, intent(in) :: nfc, fc_pairs(2,nfc), nmode
    real(dp), intent(in) :: fc(3,3,nfc) 

    complex(dp), intent(out) :: dynloc(numr,numc)

    integer :: ifc, ia_uc, ja_sc, ja_uc, m1, m2, il, jl, i, j
    real(dp) :: arg, err, R(3)
    complex(dp) :: fac, alpha

    call start_clock ('build_dynmat')

    dynloc = 0.d0

    do ifc = 1, nfc
      il = (fc_pairs(1,ifc)-1)/na_uc_l+1
      ia_uc = mod(fc_pairs(1,ifc)-1,na_uc_l)+1

      jl = (fc_pairs(2,ifc)-1)/na_sc_l+1
      ja_sc = mod(fc_pairs(2,ifc)-1,na_sc_l)+1
      ja_uc = sc_uc_map(ja_sc, jl)

      R = R_isc(1:3, isc_map(ja_sc, jl))
      arg = TPI*sum(q*R)
      fac = cmplx(cos(arg),sin(arg),kind=dp)

      do m1 = 1,3
        do m2 = 1,3
          i = 3*((il-1)*na_uc_l+ia_uc-1)+m1
          j = 3*((jl-1)*na_uc_l+ja_uc-1)+m2
          alpha = 0.5d0*fc(m1,m2,ifc)*fac
          call pzeladd ( dynloc, i, j, desca, alpha ) 
          call pzeladd ( dynloc, j, i, desca, dconjg(alpha) ) 
        enddo
      enddo

    enddo

    dynloc = dynloc / mass

    call stop_clock ('build_dynmat')
  end subroutine build_dynmat

  subroutine write_2d_cmplx_parallel ( fn, n1, l2, h2, LDA, LDB, LZ )
    integer, intent(in) :: n1, l2, h2, LDA, LDB
    complex(dp), intent(inout) :: LZ(LDA,LDB)
    character(len=*), intent(in) :: fn

    complex(dp), allocatable :: tmp(:), Z_ALL(:,:), LLZ(:,:)
    integer :: reclen, i2, n2
    logical :: exst

    integer :: LOCr, LOCc, ibcolumns, nbcolumns, ibrows, nbrows, &
               col_max, row_max, columns, rows, irow, icol, col_all, &
               node, istatus(MPI_STATUS_SIZE), ierr

    allocate(tmp(n1))
    inquire(iolength=reclen) tmp
    deallocate(tmp)

    if (master) then
      inquire(file=trim(fn), exist=exst)

      if (exst) then
        open(11, file=trim(fn), status="old")
        close(11, status="delete")
      endif

      open(11, file=trim(fn), form="unformatted", &
        status="unknown", access="direct", recl=reclen)
      allocate(z_all(n1, h2-l2+1))
    endif

    n2 = h2-l2+1

    !Begin master part
    IF (rank.EQ.0) THEN
      
      !obtain dimensions of the local array on master node
      LOCr = LDA
      LOCc = LDB

      ALLOCATE(LLZ(LOCr,LOCc))
      LLZ(1:LOCr,1:LOCc) = LZ(1:LOCr,1:LOCc)

      !k is the rank of the processor that sent the data
      !nprocs is the number of processors in the communicator
      DO node = 0, nprocs -1 
        IF (node.EQ.0) THEN
          irow = myrow
          icol = mycol
        ELSE

          !node -- is the rank of the processor that sent the data
          !status -- message status array(of type integer)
          CALL MPI_Recv(irow, 1, MPI_Integer, NODE, 10, MPI_COMM_WORLD, ISTATUS, IERR)
          CALL MPI_Recv(icol, 1, MPI_Integer, NODE, 20, MPI_COMM_WORLD, ISTATUS, IERR)

          CALL MPI_Recv(LOCr, 1, MPI_Integer, NODE, 40, MPI_COMM_WORLD, ISTATUS, IERR)
          CALL MPI_Recv(LOCc, 1, MPI_Integer, NODE, 50, MPI_COMM_WORLD, ISTATUS,IERR )

          !create a local matrix with the size of the matrix passed
          DEALLOCATE(LLZ)
          ALLOCATE( LLZ(locr,locc))

          !recieve the local matrix sent from node
          CALL MPI_Recv(LLZ, locr*locc, MPI_DOUBLE_COMPLEX, NODE, 30,MPI_COMM_WORLD, ISTATUS, IERR)
        ENDIF

        !compute the number of blocks in each local array
        nbrows = CEILING(REAL(LOCr)/REAL(NB))    !number of blocks in the row
        nbcolumns = CEILING(REAL(LOCc)/REAL(NB))    !number of blocks in the columns

        !loop over each block in the column
        DO ibcolumns = 1, nbcolumns  
          !special case - number of columns is less than NB     
          IF(ibcolumns.EQ.nbcolumns) THEN
            col_max = locc-(nbcolumns-1)*NB 
          ELSE
            col_max = NB !number of columns in a block        
          ENDIF

          !loop over each block in the row
          DO ibrows = 1, nbrows
            !special case - number of columns is less than NBCO
            IF (ibrows.EQ.nbrows) THEN
              row_max = locr-(nbrows-1)*NB
            ELSE
              row_max = NB !number of rows in a block
            ENDIF
            !for each column in the block, loop over each row in the block
            DO columns = 1, col_max
              DO rows = 1, row_max
                col_all = (icol+(ibcolumns-1)*npcol)*NB + columns
                IF (l2.le.col_all.and.col_all.le.h2) then

                  Z_ALL((irow+(ibrows-1)*nprow)*NB + rows, col_all-l2+1)&
                    = LLZ((ibrows-1)*NB + rows, (ibcolumns-1)*NB + columns)
                ENDIF
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !End master part.

    ELSE 
      ! Begin slave part.
      !send the processor coordinates in grid
      CALL MPI_Send(myrow, 1, MPI_Integer, 0, 10, MPI_COMM_WORLD, IERR)
      CALL MPI_Send(mycol, 1, MPI_Integer, 0, 20, MPI_COMM_WORLD, IERR)
      !send number rows and columns
      CALL MPI_Send(LDA, 1, MPI_Integer, 0, 40, MPI_COMM_WORLD, IERR)
      CALL MPI_Send(LDB, 1, MPI_Integer, 0, 50, MPI_COMM_WORLD, IERR)
      !send the local matrix
      CALL MPI_Send(LZ,LDA*LDB, MPI_DOUBLE_COMPLEX, 0, 30, MPI_COMM_WORLD, IERR)

    ENDIF
    ! End slave part.

    if (master) then
      do i2 = 1, h2-l2+1
        write(11, rec=i2) z_all(:,i2)
      enddo
      close(11)
      DEALLOCATE(Z_ALL, LLZ)
    endif

    call mpi_barrier(comm, mpierr)

  end subroutine write_2d_cmplx_parallel
end module phonons_solver_elpa
