program solve_tb

  implicit none
  include 'mpif.h'
  integer :: &
      nprocs, & ! number of processors
      rank, & ! rank of the current processor
      mpierr, & ! mpi error code
      comm      ! MPI_Comm_world

  logical :: &
      master    ! rank.eq.0 

  integer, parameter :: iu = 11,        &
                        iu_enk = 12     
  integer            :: iu_unk

  double complex, parameter :: one_i = (0.0d0, 1.0d0)

  integer :: M, N, na, na_1, nk, n_intra_1, n_intra_2, n_inter, &
             nb
  double precision :: alat, dist_layer, latt(3,3), relatt(3,3)

  integer, allocatable :: pair_intra_1(:,:), &
                          pair_intra_2(:,:), &
                          pair_inter(:,:),   &
                          displs(:), recvcounts(:)
  double precision, allocatable :: kpoints(:,:),   &
                                   t_intra_1(:,:), &
                                   t_intra_2(:,:), &
                                   t_inter(:,:),   &
                                   ek(:), enk(:,:),& 
                                   enk_tot(:,:)
  double complex, allocatable :: Hk(:,:), uk(:,:)

  character(len=200) :: file_hopping
  double precision :: bias
  integer :: ibl, ibu
  logical :: write_unk
  namelist/input/ file_hopping, bias, write_unk, ibl, ibu

  ! local vairables
  integer :: ik, ia, ja, ihop, ik_start, nk_loc, ib
  double precision :: k(3)
  character(len=200) :: file_unk

  comm = MPI_Comm_world

  call MPI_Init(mpierr)
  call MPI_Comm_size(comm,nprocs,mpierr)
  call MPI_Comm_rank(comm,rank,mpierr)

  master = rank.eq.0

  if (master) then
    read(5, input)
    print *, 'file_hopping = ', trim(file_hopping)
    open(iu, file=trim(file_hopping), status='old', form='formatted')

    read(iu,*) M,N
    read(iu,*) alat
    read(iu,*) dist_layer
    read(iu,*) latt(1,:)
    read(iu,*) latt(2,:)
    read(iu,*) latt(3,:)
    read(iu,*) relatt(1,:)
    read(iu,*) relatt(2,:)
    read(iu,*) relatt(3,:)
    read(iu,*) nk
    allocate(kpoints(nk,3))
    do ik = 1,nk
      read(iu,*) kpoints(ik,:)
    enddo

    read(iu,*) na

    print *, 'reading t_intra_1 ...'
    read(iu,*) n_intra_1
    allocate(pair_intra_1(n_intra_1,2))
    allocate(t_intra_1(n_intra_1,3))
    do ihop = 1, n_intra_1
      read(iu,*) pair_intra_1(ihop,:), t_intra_1(ihop,:)
    enddo

    print *, 'reading t_intra_2 ...'
    read(iu,*) n_intra_2
    allocate(pair_intra_2(n_intra_2,2))
    allocate(t_intra_2(n_intra_2,3))
    do ihop = 1, n_intra_2
      read(iu,*) pair_intra_2(ihop,:), t_intra_2(ihop,:)
    enddo

    print *, 'read t_inter ...'
    read(iu,*) n_inter
    allocate(pair_inter(n_inter,2))
    allocate(t_inter(n_inter,3))
    do ihop = 1, n_inter
      read(iu,*) pair_inter(ihop,:), t_inter(ihop,:)
    enddo
    close(iu)
  endif

  call mpi_bcast(bias, 1, mpi_double_precision, 0, comm, mpierr)
  call mpi_bcast(write_unk, 1, mpi_logical, 0, comm, mpierr)
  call mpi_bcast(ibl, 1, mpi_integer, 0, comm, mpierr)
  call mpi_bcast(ibu, 1, mpi_integer, 0, comm, mpierr)

  call mpi_bcast(M, 1, mpi_integer, 0, comm, mpierr)
  call mpi_bcast(N, 1, mpi_integer, 0, comm, mpierr)
  call mpi_bcast(alat, 1, mpi_double_precision, 0, comm, mpierr)
  call mpi_bcast(dist_layer, 1, mpi_double_precision, 0, &
                 comm, mpierr)
  call mpi_bcast(latt, 3*3, mpi_double_precision, 0, &
                 comm, mpierr)
  call mpi_bcast(relatt, 3*3, mpi_double_precision, 0, &
                 comm, mpierr)
  call mpi_bcast(nk, 1, mpi_integer, 0, comm, mpierr)
  if (.not.master) then
    allocate(kpoints(nk,3))
  endif
  call mpi_bcast(kpoints, 3*nk, mpi_double_precision, 0, &
                 comm, mpierr)
  call mpi_bcast(na, 1, mpi_integer, 0, comm, mpierr)
  call mpi_bcast(n_intra_1, 1, mpi_integer, 0, comm, mpierr)
  call mpi_bcast(n_intra_2, 1, mpi_integer, 0, comm, mpierr)
  call mpi_bcast(n_inter, 1, mpi_integer, 0, comm, mpierr)
  if (.not.master) then
    allocate(pair_intra_1(n_intra_1,2))
    allocate(t_intra_1(n_intra_1,3))
    allocate(pair_intra_2(n_intra_2,2))
    allocate(t_intra_2(n_intra_2,3))
    allocate(pair_inter(n_inter,2))
    allocate(t_inter(n_inter,3))
  endif
  call mpi_bcast(pair_intra_1, 2*n_intra_1, mpi_integer, 0, &
                 comm, mpierr)
  call mpi_bcast(pair_intra_2, 2*n_intra_2, mpi_integer, 0, &
                 comm, mpierr)
  call mpi_bcast(pair_inter, 2*n_inter, mpi_integer, 0, &
                 comm, mpierr)
  call mpi_bcast(t_intra_1, 3*n_intra_1, mpi_double_precision, &
                 0, comm, mpierr)
  call mpi_bcast(t_intra_2, 3*n_intra_2, mpi_double_precision, &
                 0, comm, mpierr)
  call mpi_bcast(t_inter, 3*n_inter, mpi_double_precision, &
                 0, comm, mpierr)

  na_1 = na/2

  if (ibl < 0) then
      ibl = 1
  endif
  if (ibu < 0) then
      ibu = na
  endif
  nb  = ibu-ibl+1

  if (master) then
    print *, 'na = ',na 
    print *, 'ibl = ', ibl, ' , ibu = ', ibu, ' , nb = ', nb
  endif

  allocate(displs(0:nprocs-1),recvcounts(0:nprocs-1))
  call distribute_indices(nk, nprocs, rank, displs, recvcounts)
  ik_start = displs(rank)+1
  nk_loc = recvcounts(rank)

  allocate(Hk(na,na), ek(nb), uk(na,nb), enk(nb,nk_loc))
  allocate(enk_tot(nb,nk))

  do ik = ik_start, ik_start+nk_loc-1
    k = kpoints(ik,:)
    write(6,'(a,I3,a,I5,a,3(F8.3,2x))') &
        'rank = ', rank, ' , ik = ', ik, ' , k = ', k(1), k(2), k(3)

    ! construct Hamiltonian
    Hk = 0.0d0
    uk = 0.0d0
    ek = 0.0d0

    ! interlayer bias
    do ia = 1, na_1
        Hk(ia, ia) = Hk(ia, ia) + bias
        Hk(na_1+ia, na_1+ia) = Hk(na_1+ia, na_1+ia) - bias
    enddo

    do ihop = 1, n_intra_1
      Hk(pair_intra_1(ihop, 1), pair_intra_1(ihop, 2)) = &
        Hk(pair_intra_1(ihop, 1), pair_intra_1(ihop, 2)) + &
        t_intra_1(ihop, 3)*exp(one_i*(k(1)*t_intra_1(ihop, 1)+k(2)*t_intra_1(ihop, 2)))
    enddo

    do ihop = 1, n_intra_2
      Hk(na_1+pair_intra_2(ihop, 1), na_1+pair_intra_2(ihop, 2)) = &
        Hk(na_1+pair_intra_2(ihop, 1), na_1+pair_intra_2(ihop, 2)) + &
        t_intra_2(ihop, 3)*exp(one_i*(k(1)*t_intra_2(ihop, 1)+k(2)*t_intra_2(ihop, 2)))
    enddo

    do ihop = 1, n_inter
      Hk(pair_inter(ihop, 1), na_1+pair_inter(ihop, 2)) = &
        Hk(pair_inter(ihop, 1), na_1+pair_inter(ihop, 2)) + &
        t_inter(ihop, 3)*exp(one_i*(k(1)*t_inter(ihop, 1)+k(2)*t_inter(ihop, 2)))
    enddo

    do ja = 1, na
      do ia = ja+1, na
        Hk(ia,ja) = dconjg(Hk(ja,ia))
      enddo
    enddo

    ! diagonalize
    ! call diagh_full(Hk, na, ek, uk)
    call diagh(Hk, na, ibl, ibu, nb, ek, uk)

    if (write_unk) then
      iu_unk = iu_enk+ik
      write(file_unk, '(a,i0.5,a)') 'unk.',ik,'.dat'
      open(iu_unk, file=trim(file_unk), form='unformatted')
      do ib = 1, nb
        ! write(iu_unk, '(F20.12)') (uk(ia,ib), ia=1,na)
        write(iu_unk) (uk(ia,ib), ia=1,na)
      enddo
      close(iu_unk)
    endif

    enk(1:nb,ik-ik_start+1) = ek(1:nb)
  enddo

  call mpi_gatherv(enk, nb*nk_loc, mpi_double_precision, &
                   enk_tot, nb*recvcounts, nb*displs, &
                   mpi_double_precision, 0, comm, mpierr)

  if (master) then

    print *, 'Saving eigenvalues ...'

    open(iu_enk, file='enk.dat', form='formatted')

    do ik=1,nk
        do ia=1,nb
            write(iu_enk, '(F20.12,1x)', advance='no') enk_tot(ia,ik)
        enddo
        write(iu_enk, '(a)')
    enddo

    close(iu_enk)

  endif

  deallocate(pair_intra_1, pair_intra_2, pair_inter)
  deallocate(t_intra_1, t_intra_2, t_inter)
  deallocate(ek, uk)
  deallocate(enk, enk_tot)

  if (master) then
    print *, "JOB DONE!"
  endif

  call MPI_Finalize(mpierr)
contains
  subroutine diagh_full(H, N, ek, uk)
      integer, intent(in) :: N
      double complex, intent(inout) :: H(N, N)
      double precision, intent(out) :: ek(N)
      double complex, intent(out) :: uk(N,N)

      ! local variables
      integer :: nh, info, lwork
      double precision :: rwork(3*N-2), dummy(1)
      double complex, allocatable :: work(:)

      call ZHEEV('V','U',N,H,N,ek,dummy,-1,rwork,info)
      lwork = dummy(1)
      allocate(work(lwork))
      call ZHEEV('V','U',N,H,N,ek,work,lwork,rwork,info)

      if (info.ne.0) then
          write(*,*) "ZHEEV info = ", info
          return
      endif

      uk = H

  end subroutine diagh_full

  subroutine diagh(H, N, il, iu, nb, ek, uk)
      integer, intent(in) :: N, il, iu, nb
      double complex, intent(inout) :: H(N, N)
      double precision, intent(out) :: ek(nb)
      double complex, intent(out) :: uk(N,nb)

      ! local variables
      integer :: nh, info, lwork, lrwork, liwork, &
                 isuppz(2*n), dummy3(1)
      double complex :: dummy1(1)
      double precision :: dummy2(1)
      integer, allocatable :: iwork(:)
      double complex, allocatable :: work(:)
      double precision, allocatable :: rwork(:)

      double precision, allocatable :: w(:)

      call ZHEEVR('V', 'I', 'U', N, H, N, 0.d0, 0.d0, &
                  il, iu, -1.0, nb, &
                  w, uk, N, isuppz, dummy1, -1, dummy2, -1, &
                  dummy3, -1, info)
      lwork = dummy1(1)
      lrwork = dummy2(1)
      liwork = dummy3(1)
      allocate(w(n))
      allocate(work(lwork))
      allocate(rwork(lrwork))
      allocate(iwork(liwork))
      call ZHEEVR('V', 'I', 'U', N, H, N, 0.0d0, 0.d0, &
                  il, iu, -1.0, nb, &
                  w, uk, N, isuppz, work, lwork, rwork, lrwork, &
                  iwork, liwork, info)

      ek(1:nb) = w(1:nb)

      if (info.ne.0) then
          write(*,*) "ZHEEVR info = ", info
          return
      endif

      deallocate(w)
      deallocate(work)
      deallocate(rwork)
      deallocate(iwork)
  end subroutine diagh

  subroutine distribute_indices(n, nprocs, rank, displs, recvcounts)
    integer, intent(in) :: n, nprocs, rank
    integer, intent(out) :: displs(0:nprocs-1),recvcounts(0:nprocs-1)

    integer :: iproc, nloc, iloc

    do iproc=0,nprocs-1

        nloc = int(n/nprocs)

        if (iproc < mod(n, nprocs)) then
          nloc = nloc + 1
          iloc = iproc*int(n/nprocs)+iproc
        else
          iloc = iproc*int(n/nprocs)+mod(n, nprocs)
        endif

        displs(iproc) = iloc
        recvcounts(iproc) = nloc
    enddo

  end subroutine distribute_indices
end program solve_tb
