# Tight-binding model calculation of twisted bilayer graphene with arbitrary commensurate rotation angle.
# Reference: S. Fang and E. Kaxiras, Phys. Rev. B 93, 235153 (2016)
from numpy import *
import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches


def main():
    M = 18
    N = 17

    # Lattice constants
    a = 2.46 # Ang
    d = 3.35 # Ang
    c = 20   # Ang

    th = arccos(float(N*N+4.*N*M+M*M)/float(2.*(N*N+N*M+M*M)))

    n_atoms = 4*(N*N+N*M+M*M)

    a_puc = a*array([[sqrt(3.)/2., -1./2., 0.],
                     [sqrt(3.)/2.,  1./2., 0.],
                     [0., 0., c/a]]).T

    rot = array([[cos(th/2),  sin(th/2), 0.],
                 [-sin(th/2),  cos(th/2), 0.],
                 [     0.,       0., 1.]])
    rot2 = array([[ cos(th/2), -sin(th/2), 0.],
                  [ sin(th/2),  cos(th/2), 0.],
                  [        0.,       0., 1.]])

    a_1st = rot.dot(a_puc)
    a_2nd = rot2.dot(a_puc)

    # commensurate cell vectors
    t = array([[ N,   M, 0], \
               [-M, N+M, 0], \
               [ 0,   0, 1]]).dot(a_1st.T).T

    b_1st = 2*pi*linalg.inv((a_1st)).T
    b_2nd = 2*pi*linalg.inv((a_2nd)).T
    tg = 2*pi*linalg.inv(t).T

    kpoints = generate_kpoints(tg)

    plot_patch([
         b_1st.dot([2./3., 1./3., 0.])[:2],
         b_1st.dot([1./3., 2./3., 0.])[:2],
         b_1st.dot([-1./3., 1./3., 0.])[:2],
        -b_1st.dot([2./3., 1./3., 0.])[:2],
        -b_1st.dot([1./3., 2./3., 0.])[:2],
        -b_1st.dot([-1./3., 1./3., 0.])[:2],
         b_1st.dot([2./3., 1./3., 0.])[:2],
        ],0.5,'r')
    xmin = -.1
    xmax = b_1st[0,0]*1.3
    ymin = -.1
    ymax = b_1st[1,1]
    for i1 in range(-N-M, N+M):
        for i2 in range(-N-M, N+M):
            tt = i1*tg[:2,0]+i2*tg[:2,1]
            if xmin < tt[0] and tt[0] < xmax \
                and ymin < tt[1] and tt[1] < ymax:
                plt.plot(tt[0]+kpoints[:,0], tt[1]+kpoints[:,1], 'k', lw=0.5)
                plot_patch([
                   tt+  tg.dot([2./3., 1./3., 0.])[:2],
                   tt+  tg.dot([1./3., 2./3., 0.])[:2],
                   tt+  tg.dot([-1./3., 1./3., 0.])[:2],
                   tt  -tg.dot([2./3., 1./3., 0.])[:2],
                   tt  -tg.dot([1./3., 2./3., 0.])[:2],
                   tt  -tg.dot([-1./3., 1./3., 0.])[:2],
                   tt+  tg.dot([2./3., 1./3., 0.])[:2],
                    ],0.5,'b')
    plt.xlim(xmin, xmax)
    plt.ylim(ymin, ymax)
    plt.show()


    return
def plot_patch(vertices, lw, color):
    plt.gca().add_patch(patches.PathPatch(Path(vertices), lw=lw, edgecolor=color, fill=False))
def supercell_lattice_points(N,M):
    """
    Find the lattice points in the commensurate supercell
    spanned by (N,M), (-M, N+M) vectors.
    Inequailities to be satisfied are as follows
    1 : M/N*i1 <= i2
    2 : -(N+M)/M*i1 <= i2
    3 : N+M + M/N*(i1+M) > i2
    4 : M - (N+M)/M*(i1-N) > i2
    """

    fM = float(M)
    fN = float(N)

    cells = []
    for i1 in range(-M,N):
        for i2 in range(0,N+2*M):
            if fM/fN*i1 <= i2 and \
               -(fN+fM)/fM*i1 <= i2 and \
               fN+fM + fM/fN*(i1+fM) > i2 and \
               fM - (fN+fM)/fM*(i1-fN) > i2:
                   cells.append([i1,i2])
    return array(cells, dtype=int32)

# interlayer hopping parameters
l_i = [0.3155, -0.0688, -0.0083] # eV
xi_i = [1.7543, 3.4692, 2.8764]  # dimensionless
x_i = [None, 0.5212, 1.5206]     # dimensionless
kappa_i = [2.0010, None, 1.5731] # dimensionless

# Eq (2), (3)
def interlayer_hopping(rbar, th12, th21):
    v0 = l_i[0] * exp(-xi_i[0]*rbar**2) * cos(kappa_i[0]*rbar)
    v3 = l_i[1] * rbar**2 * exp(-xi_i[1]*(rbar-x_i[1])**2)
    v6 = l_i[2] * exp(-xi_i[2]*(rbar-x_i[2])**2) * sin(kappa_i[2]*rbar)

    return v0 + v3 * (cos(3*th12)+cos(3*th21)) \
              + v6 * (cos(6*th12)+cos(6*th21))

def generate_kpoints(a_1st):

    xks = [[0,         0,  0],
           [0.5,       0,  0],
           [2./3., 1./3.,  0],
           [0,         0,  0]]
    wks = [30, 30, 30, 30]
    nks = len(wks)
    labels = ['$\Gamma$', 'M', 'K', '$\Gamma$']

    xks = array(xks)

    kpoints_frac = []

    # add the first point
    kpoints_frac.append(xks[0,:])

    for ik in range(nks-1):
        xk_start = xks[ik,:]
        xk_end   = xks[ik+1,:]
        wk       = wks[ik] # number of intermediate k-points

        dk = (xk_end - xk_start)/wk

        for i in range(wk):
            k = xk_start + dk*(i+1)
            kpoints_frac.append(k)

    kpoints_frac = array(kpoints_frac)
    nktot = len(kpoints_frac)

    kpoints = []

    for kf in kpoints_frac:
        kpoints.append(a_1st.dot(kf))

    f = open('kpoints.dat', 'w')
    for k in kpoints:
        f.write('%.8f %.8f %.8f\n'%(k[0],k[1],k[2]))
    f.close()
    return array(kpoints)
main()

