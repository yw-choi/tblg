# Tight-binding model calculation of twisted bilayer graphene with arbitrary commensurate rotation angle.
# Reference: S. Fang and E. Kaxiras, Phys. Rev. B 93, 235153 (2016)
from numpy import *
import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches


def main():
    M = 1
    N = 1

    # Lattice constants
    a = 2.46 # Ang
    d = 3.35 # Ang
    c = 20   # Ang

    plot_supercell(M, N, a, d, c)

    return

def plot_supercell(M, N, a, d, c):
    """
    M, N commensurate supercell index
    a    lattice constant in Ang
    d    interlayer distance in Ang
    c    cell size including the vacuum region
    make commensurate supercell (M,N) of twisted bilayer graphene
    """

    print "Generating commensurate supercell with (M,N)=(%d,%d) ..." % (M, N)

    th = arccos(float(N*N+4.*N*M+M*M)/float(2.*(N*N+N*M+M*M)))

    print ">>> Rotation angle between the top and bottom layer is %.4f deg." % (th*180/pi)
    n_atoms = 4*(N*N+N*M+M*M)

    a_puc = a*array([[sqrt(3.)/2., -1./2., 0.],
                     [sqrt(3.)/2.,  1./2., 0.],
                     [0., 0., c/a]]).T

    rot = array([[cos(th/2),  sin(th/2), 0.],
                 [-sin(th/2),  cos(th/2), 0.],
                 [     0.,       0., 1.]])
    rot2 = array([[ cos(th/2), -sin(th/2), 0.],
                  [ sin(th/2),  cos(th/2), 0.],
                  [        0.,       0., 1.]])

    a_bot = rot.dot(a_puc)
    a_top = rot2.dot(a_puc)

    # commensurate cell vectors
    t = array([[ N,   M, 0], \
               [-M, N+M, 0], \
               [ 0,   0, 1]]).dot(a_bot.T).T
    tp = array([[   M,   N, 0], \
                [  -N, M+N, 0], \
                [   0,   0, 1]]).dot(a_top.T).T

    # Find the cells for the bottom layer that are in the commensurate super cell.
    cells_bot = supercell_lattice_points(N, M)
    cells_top = supercell_lattice_points(M, N)

    # Lattice points
    # points = []
    # for cell in cells_bot:
    #     point = cell[0] * a_bot[:,0] + cell[1] * a_bot[:,1]
    #     points.append(point)
    # points = array(points)
    # plt.scatter(points[:,0], points[:,1], s=1, color='r')

    # points = []
    # for cell in cells_top:
    #     point = cell[0] * a_top[:,0] + cell[1] * a_top[:,1]
    #     points.append(point)
    # points = array(points)
    # plt.scatter(points[:,0], points[:,1], s=1, color='b')

    # Atom positions
    s = 1
    atoms_bot = []
    for cell in cells_bot:
        point = cell[0] * a_bot[:,0] + cell[1] * a_bot[:,1]
        c1 = cell
        c2 = cell + array([1./3, 1./3])
        atoms_bot.append(c1[0]*a_bot[:,0]+c1[1]*a_bot[:,1])
        atoms_bot.append(c2[0]*a_bot[:,0]+c2[1]*a_bot[:,1])
    atoms_bot = array(atoms_bot)
    plt.scatter(atoms_bot[:,0], atoms_bot[:,1], s=s, color='r')

    atoms_top = []
    for cell in cells_top:
        point = cell[0] * a_top[:,0] + cell[1] * a_top[:,1]
        c1 = cell
        c2 = cell + array([1./3, 1./3])
        atoms_top.append(c1[0]*a_top[:,0]+c1[1]*a_top[:,1])
        atoms_top.append(c2[0]*a_top[:,0]+c2[1]*a_top[:,1])
    atoms_top = array(atoms_top)
    print len(atoms_bot)+len(atoms_top)
    print n_atoms
    plt.scatter(atoms_top[:,0], atoms_top[:,1], s=s, color='b')

    # primitive unit cell of the bottom layer
    plt.gca().add_patch(patches.PathPatch(Path([[0,0], a_bot[:2,0]]), lw=.5, edgecolor='r'))
    plt.gca().add_patch(patches.PathPatch(Path([[0,0], a_bot[:2,1]]), lw=.5, edgecolor='r'))
    # primitive unit cell of the top layer
    plt.gca().add_patch(patches.PathPatch(Path([[0,0], a_top[:2,0]]), lw=.5, edgecolor='b'))
    plt.gca().add_patch(patches.PathPatch(Path([[0,0], a_top[:2,1]]), lw=.5, edgecolor='b'))

    # boundary of commensurate supercell
    plt.gca().add_patch(patches.PathPatch(Path([[0,0], t[:2,0]]), lw=1, edgecolor='r'))
    plt.gca().add_patch(patches.PathPatch(Path([[0,0], t[:2,1]]), lw=1, edgecolor='r'))
    plt.gca().add_patch(patches.PathPatch(Path([t[:2,0], t[:2,0]+t[:2,1]]), lw=1, edgecolor='r'))
    plt.gca().add_patch(patches.PathPatch(Path([t[:2,1], t[:2,0]+t[:2,1]]), lw=1, edgecolor='r'))

    plt.gca().add_patch(patches.PathPatch(Path([[0,0], tp[:2,0]]), lw=1, edgecolor='b'))
    plt.gca().add_patch(patches.PathPatch(Path([[0,0], tp[:2,1]]), lw=1, edgecolor='b'))
    plt.gca().add_patch(patches.PathPatch(Path([tp[:2,0], tp[:2,0]+tp[:2,1]]), lw=1, edgecolor='b'))
    plt.gca().add_patch(patches.PathPatch(Path([tp[:2,1], tp[:2,0]+tp[:2,1]]), lw=1, edgecolor='b'))

    plt.xlim(-1,1.05*(t[0,0]+t[0,1]))
    plt.ylim(1.05*a_bot[1,0],1.05*t[1,1])
    plt.show()
    return

def supercell_lattice_points(N,M):
    """
    Find the lattice points in the commensurate supercell
    spanned by (N,M), (-M, N+M) vectors.
    Inequailities to be satisfied are as follows
    1 : M/N*i1 <= i2
    2 : -(N+M)/M*i1 <= i2
    3 : N+M + M/N*(i1+M) > i2
    4 : M - (N+M)/M*(i1-N) > i2
    """

    fM = float(M)
    fN = float(N)

    cells = []
    for i1 in range(-M,N):
        for i2 in range(0,N+2*M):
            if fM/fN*i1 <= i2 and \
               -(fN+fM)/fM*i1 <= i2 and \
               fN+fM + fM/fN*(i1+fM) > i2 and \
               fM - (fN+fM)/fM*(i1-fN) > i2:
                   cells.append([i1,i2])
    return array(cells, dtype=int32)

# interlayer hopping parameters
l_i = [0.3155, -0.0688, -0.0083] # eV
xi_i = [1.7543, 3.4692, 2.8764]  # dimensionless
x_i = [None, 0.5212, 1.5206]     # dimensionless
kappa_i = [2.0010, None, 1.5731] # dimensionless

# Eq (2), (3)
def interlayer_hopping(rbar, th12, th21):
    v0 = l_i[0] * exp(-xi_i[0]*rbar**2) * cos(kappa_i[0]*rbar)
    v3 = l_i[1] * rbar**2 * exp(-xi_i[1]*(rbar-x_i[1])**2)
    v6 = l_i[2] * exp(-xi_i[2]*(rbar-x_i[2])**2) * sin(kappa_i[2]*rbar)

    return v0 + v3 * (cos(3*th12)+cos(3*th21)) \
              + v6 * (cos(6*th12)+cos(6*th21))

main()

