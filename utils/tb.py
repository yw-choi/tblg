# Tight-binding model calculation of twisted bilayer graphene with arbitrary commensurate rotation angle.
# Reference: S. Fang and E. Kaxiras, Phys. Rev. B 93, 235153 (2016)
from numpy import *
import matplotlib.pyplot as plt


# # lattice vectors
# alat = array([[sqrt(3.)/2., -1./2.],
#               [sqrt(3.)/2.,  1./2.]])
# delta_A = array([0,0,0])
# delta_B = (alat[0,:]+alat[1,:])/3

# Parameters for intralayer/interlayer hopping (See Table I.)
# in-plane hopping parameters in eV
t = [ -2.8922,
       0.2425,
      -0.2656,
       0.0235,
       0.0524,
      -0.0209,
      -0.0148,
      -0.0211]

def main():
    M = 31
    N = 30

    # Lattice constants
    a = 2.46 # Ang
    d = 3.35 # Ang
    c = 20   # Ang

    t, a_1st, a_2nd, atoms_1st, atoms_2nd = generate_commensurate_cell(M, N, a, d, c)

    na = len(atoms_1st) + len(atoms_2nd)



    return

def generate_commensurate_cell(M, N, a, d, c):
    """
    construct commensurate supercell (M,N) of twisted bilayer graphene

    M, N commensurate supercell index
    a    lattice constant in Ang
    d    interlayer distance in Ang
    c    cell size including the vacuum region
    """

    print "Generating commensurate supercell with (M,N)=(%d,%d) ..." % (M, N)
    print ">>> a = %.3f Ang" % a
    print ">>> d = %.3f Ang" % d
    print ">>> c = %.3f Ang" % c
    print ">>> (M, N)   /  theta        /  n_atoms"

    th = arccos(float(N*N+4.*N*M+M*M)/float(2.*(N*N+N*M+M*M)))
    n_atoms = 4*(N*N+N*M+M*M)
    print ">>> (%2d, %2d) /  %.4f deg   /  %d" %( M, N, th*180/pi, n_atoms)

    a_puc = a*array([[sqrt(3.)/2., -1./2., 0.],
                     [sqrt(3.)/2.,  1./2., 0.],
                     [0., 0., c/a]]).T

    rot = array([[cos(th/2),  sin(th/2), 0.],
                 [-sin(th/2),  cos(th/2), 0.],
                 [     0.,       0., 1.]])
    rot2 = array([[ cos(th/2), -sin(th/2), 0.],
                  [ sin(th/2),  cos(th/2), 0.],
                  [        0.,       0., 1.]])

    a_1st = rot.dot(a_puc)
    a_2nd = rot2.dot(a_puc)

    # commensurate cell vectors
    t = array([[ N,   M, 0], \
               [-M, N+M, 0], \
               [ 0,   0, 1]]).dot(a_1st.T).T
    tp = array([[   M,   N, 0], \
                [  -N, M+N, 0], \
                [   0,   0, 1]]).dot(a_2nd.T).T

    # Find the cells in the commensurate super cell.
    cells_1st = supercell_lattice_points(N, M)
    cells_2nd = supercell_lattice_points(M, N)

    # Atom positions
    s = 1
    atoms_1st = []
    for cell in cells_1st:
        point = cell[0] * a_1st[:,0] + cell[1] * a_1st[:,1]
        c1 = cell
        c2 = cell + array([1./3, 1./3])
        atoms_1st.append(c1[0]*a_1st[:,0]+c1[1]*a_1st[:,1])
        atoms_1st.append(c2[0]*a_1st[:,0]+c2[1]*a_1st[:,1])
    atoms_1st = array(atoms_1st)

    atoms_2nd = []
    for cell in cells_2nd:
        point = cell[0] * a_2nd[:,0] + cell[1] * a_2nd[:,1]
        c1 = cell
        c2 = cell + array([1./3, 1./3])
        atoms_2nd.append(c1[0]*a_2nd[:,0]+c1[1]*a_2nd[:,1])
        atoms_2nd.append(c2[0]*a_2nd[:,0]+c2[1]*a_2nd[:,1])
    atoms_2nd = array(atoms_2nd)

    return t, a_1st, a_2nd, atoms_1st, atoms_2nd

def supercell_lattice_points(N,M):
    """
    Find the lattice points in the commensurate supercell
    spanned by (N,M), (-M, N+M) vectors.
    Inequailities to be satisfied are as follows
    1 : M/N*i1 <= i2
    2 : -(N+M)/M*i1 <= i2
    3 : N+M + M/N*(i1+M) > i2
    4 : M - (N+M)/M*(i1-N) > i2
    """

    fM = float(M)
    fN = float(N)

    cells = []
    for i1 in range(-M,N):
        for i2 in range(0,N+2*M):
            if fM/fN*i1 <= i2 and \
               -(fN+fM)/fM*i1 <= i2 and \
               fN+fM + fM/fN*(i1+fM) > i2 and \
               fM - (fN+fM)/fM*(i1-fN) > i2:
                   cells.append([i1,i2])
    return array(cells, dtype=int32)

# interlayer hopping parameters
l_i = [0.3155, -0.0688, -0.0083] # eV
xi_i = [1.7543, 3.4692, 2.8764]  # dimensionless
x_i = [None, 0.5212, 1.5206]     # dimensionless
kappa_i = [2.0010, None, 1.5731] # dimensionless

# Eq (2), (3)
def interlayer_hopping(rbar, th12, th21):
    v0 = l_i[0] * exp(-xi_i[0]*rbar**2) * cos(kappa_i[0]*rbar)
    v3 = l_i[1] * rbar**2 * exp(-xi_i[1]*(rbar-x_i[1])**2)
    v6 = l_i[2] * exp(-xi_i[2]*(rbar-x_i[2])**2) * sin(kappa_i[2]*rbar)

    return v0 + v3 * (cos(3*th12)+cos(3*th21)) \
              + v6 * (cos(6*th12)+cos(6*th21))

main()

