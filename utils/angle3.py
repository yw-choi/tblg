import numpy as np

a_top = np.array([[np.sqrt(3)/2, -1./2., 0.],
                  [np.sqrt(3)/2,  1./2., 0.],
                  [0., 0., 100.]],
                  dtype=np.float64)
target_angle = 1.1 # 1.1
th = 0.01
n = float(32)
m = float(33)
R = n*a_top[0,:]+m*a_top[1,:]
angle = np.arccos((n*n+4*n*m+m*m)/(2*(n*n+n*m+m*m)))*180/np.pi
print n, m, angle
