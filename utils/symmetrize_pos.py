from numpy import *

def main():

    f = open('atoms.out', 'r')
    latt = zeros((3,3))
    latt[:,0] = float64(f.readline().split())
    latt[:,1] = float64(f.readline().split())
    latt[:,2] = float64(f.readline().split())
    f.close()
    data = loadtxt('atoms.out', skiprows=4)
    atoms = data[:,:3]
    atoms_rlx = data[:,3:]

    invlatt = linalg.inv(latt)
    na = len(atoms)
    na_l = na/2

    atoms_frac = get_atoms_frac(latt, atoms)

    th = pi/3.0
    # C6
    symm = array([[cos(th), sin(th), 0.0],
                  [-sin(th), cos(th), 0.0],
                  [0.0,0.0,1.0]])

    unique_atoms = []
    equiv_atoms = -ones((na_l,6), dtype=int32)
    n_equiv_atoms = zeros((na_l), dtype=int32)

    for ia in range(na_l):
        print "Processing ia=%d"%ia
        # check if the current atom is symmetry-equivalent to another atom.
        checked = ia in unique_atoms
        for ja in range(na_l):
            if ia in equiv_atoms[ja,:]:
                checked = True
                break
        # if so, skip
        if checked:
            continue

        # new unique atom
        unique_atoms.append(ia)

        cur_ia = ia
        cur_at = atoms[ia,:]
        while True:

            at_rot = symm.dot(cur_at)
            at_rot_frac = invlatt.dot(at_rot)
            at_rot_frac[:2] -= floor(at_rot_frac[:2]) # except z-axis

            next_ja = -1
            for ja in range(na_l):
                d = linalg.norm(latt.dot(atoms_frac[ja,:]-at_rot_frac))
                if d < 0.01: # Ang
                    next_ja = ja
                    break

            if next_ja == -1:
                print "Equiv. atom not found"
                print "ia = %d" % ( cur_ia )
                exit(-1)

            # Break if returned to the original atom
            if next_ja == ia:
                break

            # new equiv. atom
            n_equiv_atoms[ia] += 1
            equiv_atoms[ia, n_equiv_atoms[ia]-1] = next_ja

            # set new cur_ia
            cur_ia = next_ja
            cur_at = at_rot

            # print "unique atom = %d, isym = %d, n_equiv_atoms = %d"%(ia, isym, n_equiv_atoms[ia,isym])


    print "Symmterized structure"
    d0 = 3.35/2.0
    atoms_rlx_symm = []
    for ia in unique_atoms:
        print 'processing C6 for ia = %d'%ia

        at = atoms_rlx[ia,:]
        at_avg = at.copy()
        for i in range(n_equiv_atoms[ia]):
            ja = equiv_atoms[ia,i]
            atj = atoms_rlx[ja,:]
            for j in range(i+1):
                atj = symm.T.dot(atj)
            at_avg += atj
        at_avg /= n_equiv_atoms[ia]

        atoms_rlx_symm.append(shift_to_uc(latt,invlatt,at_avg))

        for i in range(n_equiv_atoms[ia]):
            at_avg = symm.dot(at_avg)
            atoms_rlx_symm.append(shift_to_uc(latt,invlatt,at_avg))
    # MyMz
    for ia in range(len(atoms_rlx_symm)):
        print 'processing MyMz for ia = %d'%ia
        at = atoms_rlx_symm[ia]
        at2 = at.copy()
        at2[1] = -at2[1]
        at2[2] = 2*d0-at2[2]
        atoms_rlx_symm.append(shift_to_uc(latt,invlatt,at2))

    f = open('atoms.symm.in', 'w')

    for i in range(3):
        f.write('%20.16f %20.16f %20.16f\n'%(latt[0,i],latt[1,i],latt[2,i]))

    f.write('%d\n'%(len(atoms_rlx_symm)))
    for at in atoms_rlx_symm:
        f.write('%20.16f %20.16f %20.16f\n'%(at[0],at[1],at[2]))
    f.close()

    print "DONE"
def get_atoms_frac(latt, atoms):
    na = shape(atoms)[0]
    atoms_frac = zeros((na,3))

    invlatt = linalg.inv(latt)

    for ia, at in enumerate(atoms):
        af = invlatt.dot(at)
        R  = floor(af+1.e-8)
        af[:2] -= R[:2] # no z-axis shift

        atoms_frac[ia,:] = af

    return atoms_frac


def generate_cell(M, N, a):
    """
    construct commensurate supercell (M,N) of twisted bilayer graphene

    M, N commensurate supercell index
    a    lattice constant in Ang
    d    interlayer distance in Ang
    c    cell size including the vacuum region
    """

    th = arccos(float(N*N+4.*N*M+M*M)/float(2.*(N*N+N*M+M*M)))
    na = 2*(N*N+N*M+M*M)
    c = 20
    a_puc = a*array([[sqrt(3.)/2., -1./2., 0.],
                     [sqrt(3.)/2.,  1./2., 0.],
                     [0., 0., c/a]]).T

    rot = array([[cos(th/2),  sin(th/2), 0.],
                 [-sin(th/2),  cos(th/2), 0.],
                 [     0.,       0., 1.]])
    a_1st = rot.dot(a_puc)
    # commensurate cell vectors
    t = array([[ N,   M, 0], \
               [-M, N+M, 0], \
               [ 0,   0, 1]]).dot(a_1st.T).T

    # Find the cells in the commensurate super cell.
    cells_1st = supercell_lattice_points(N, M)

    # Atom positions
    atoms_1st = []
    for cell in cells_1st:
        point = cell[0] * a_1st[:,0] + cell[1] * a_1st[:,1]
        c1 = cell + array([1./3, 1./3])
        c2 = cell + array([2./3, 2./3])
        atoms_1st.append(c1[0]*a_1st[:,0]+c1[1]*a_1st[:,1])
        atoms_1st.append(c2[0]*a_1st[:,0]+c2[1]*a_1st[:,1])
    atoms_1st = array(atoms_1st)

    return t, atoms_1st

def supercell_lattice_points(N,M):
    """
    Find the lattice points in the commensurate supercell
    spanned by (N,M), (-M, N+M) vectors.
    Inequailities to be satisfied are as follows
    1 : M/N*i1 <= i2
    2 : -(N+M)/M*i1 <= i2
    3 : N+M + M/N*(i1+M) > i2
    4 : M - (N+M)/M*(i1-N) > i2
    """

    fM = float(M)
    fN = float(N)

    cells = []
    for i1 in range(-M,N):
        for i2 in range(0,N+2*M):
            if fM/fN*i1 <= i2 and \
               -(fN+fM)/fM*i1 <= i2 and \
               fN+fM + fM/fN*(i1+fM) > i2 and \
               fM - (fN+fM)/fM*(i1-fN) > i2:
                   cells.append([i1,i2])
    return array(cells, dtype=int32)

def shift_to_uc(latt, invlatt, at):
    atf = invlatt.dot(at)
    atf[:2] -= floor(atf[:2])
    return latt.dot(atf)
main()
