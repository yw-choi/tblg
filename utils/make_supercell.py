import numpy as np
import matplotlib.pyplot as plt
def main():
    """
    make commensurate supercell (n1,n2) of twisted bilayer graphene
    """

    n1 = 30  # supercell indices
    n2 = 31

    a = np.float64(2.46) # lattice constant in Ang
    d = 3.35             # interlayer distance in Ang

    n = np.float64(n1)
    m = np.float64(n2)
    th = np.arccos((n*n+4*n*m+m*m)/(2*(n*n+n*m+m*m)))
    print n1, n2, th*180/np.pi
    rot = np.array([[np.cos(th), -np.sin(th), 0.],
                    [np.sin(th),  np.cos(th), 0.],
                    [        0.,          0., 1.]],
                    dtype=np.float64)


    a_top = a*np.array([[np.sqrt(3.)/2., -1./2., 0.],
                        [np.sqrt(3.)/2.,  1./2., 0.],
                        [0., 0., 100.]],
                        dtype=np.float64)

    a_bot = a_top.dot(np.transpose(rot))
    print a_top
    print a_bot

    # commensurate cell vectors
    t1 = np.float64(n1)*a_top[0,:]+np.float64(n2)*a_top[1,:]
    t2 = -np.float64(n2)*a_top[0,:]+np.float64(n1+n2)*a_top[1,:]

    print t1
    print t2

    points_top = supercell_lattice_points(n1,n2)
    points_bot = supercell_lattice_points(n2,n1)

    R1 = []
    R2 = []
    for i in range(len(points_top)):
        p = points_top[i]
        R1.append(p[0]*a_top[0,:]+p[1]*a_top[1,:])
        p = points_bot[i]
        R2.append(p[0]*a_bot[0,:]+p[1]*a_bot[1,:])

    R1 = np.array(R1)
    R2 = np.array(R2)

    # atoms
    atoms = []
    for i in range(len(R1)):
        m1 = R1[i,:]+1./3.*a_top[0,:]+1./3.*a_top[1,:]+np.array([0,0,50.], dtype=np.float64)
        m2 = R1[i,:]+2./3.*a_top[0,:]+2./3.*a_top[1,:]+np.array([0,0,50.], dtype=np.float64)
        m3 = R2[i,:]+1./3.*a_bot[0,:]+1./3.*a_bot[1,:]+np.array([0,0,50.-d], dtype=np.float64)
        m4 = R2[i,:]+2./3.*a_bot[0,:]+2./3.*a_bot[1,:]+np.array([0,0,50.-d], dtype=np.float64)

        atoms.append(m1)
        atoms.append(m2)
        atoms.append(m3)
        atoms.append(m4)

    f = open("supercell.in", "w")
    f.write("lattice_vector %20.16f %20.16f %20.16f\n"%(t1[0], t1[1], 0.))
    f.write("lattice_vector %20.16f %20.16f %20.16f\n"%(t2[0], t2[1], 0.))
    f.write("lattice_vector %20.16f %20.16f %20.16f\n"%(0., 0., 100.))

    for at in atoms:
        f.write("atom %20.16f %20.16f %20.16f C\n"%(at[0], at[1], at[2]))

    f.close()

    # plt.scatter(R1[:,0], R1[:,1], c='red')
    # plt.scatter(R2[:,0], R2[:,1], c='blue')

    # plt.plot([0,t1[0]], [0,t1[1]], c='black', linewidth=1)
    # plt.plot([0,t2[0]], [0,t2[1]], c='black', linewidth=1)

    # plt.plot([t1[0],t1[0]+t2[0]], [t1[1],t1[1]+t2[1]], c='black', linewidth=1)
    # plt.plot([t2[0],t1[0]+t2[0]], [t2[1],t1[1]+t2[1]], c='black', linewidth=1)

    # plt.show()

    return

def supercell_lattice_points(n1,n2):
    """
    find the integer points that satisfy the following inequilities
    1 : n2*i1/n2 <= i2
    2 : -(n1+n2)*i1/n2 <= i2
    3 : n1+n2 + n2*(i1+n2)/n1 >= i2
    4 : n2 - (n1+n2)/n2*(i1-n1) >= i2
    """

    cells = []
    for i1 in range(-n2+1,n1):
        for i2 in range(0,n1+2*n2):

            # 1
            if n2*i1%n1==0:
                lb = n2*i1/n1
            else:
                lb = int(n2*i1/n1)+1

            if lb>i2: # reject point
                continue

            # 2
            if (n1+n2)*i1%n2==0:
                lb = -(n1+n2)*i1/n2
            else:
                lb = -int((n1+n2)*i1/n2)+1

            if lb>i2: # reject point
                continue

            # 3
            hb = n1+n2+int(n2*(i1+n2)/n1)
            if hb<i2:
                continue

            # 4
            hb = n2-int((n1+n2)*(i1-n1)/n2)
            if hb<i2:
                continue

            cells.append([i1,i2])
    return np.array(cells, dtype=np.int32)
if __name__=="__main__":
    main()

