import numpy as np
import matplotlib.pyplot as plt

n1 = 30
n2 = 31
n = np.float64(n1)
m = np.float64(n2)
th = np.arccos((n*n+4*n*m+m*m)/(2*(n*n+n*m+m*m)))
# th = np.pi/6
print n1, n2, th*180/np.pi
rot = np.array([[np.cos(th), -np.sin(th), 0.],
                [np.sin(th),  np.cos(th), 0.],
                [        0.,          0., 1.]],
                dtype=np.float64)


a = np.float64(2.46) # Ang
a_top = a*np.array([[np.sqrt(3.)/2., -1./2., 0.],
                    [np.sqrt(3.)/2.,  1./2., 0.],
                    [0., 0., 100.]],
                    dtype=np.float64)

a_bot = a_top.dot(np.transpose(rot))
print a_top
print a_bot

# commensurate cell vectors
t1 = np.float64(n1)*a_top[0,:]+np.float64(n2)*a_top[1,:]
t2 = -np.float64(n2)*a_top[0,:]+np.float64(n1+n2)*a_top[1,:]

plt.plot([0., t1[0]], [0., t1[1]], '-', color='black', linewidth=1)
plt.plot([0., t2[0]], [0., t2[1]], '-', color='black', linewidth=1)
plt.plot([0., a_top[0,0]], [0., a_top[0,1]], '-', color='red', linewidth=1)
plt.plot([0., a_top[1,0]], [0., a_top[1,1]], '-', color='red', linewidth=1)

print t1
print t2

print np.linalg.norm(t1)
print np.linalg.norm(t2)

x = []
y = []
for i1 in range(-n2,n1+1):
    for i2 in range(n1+n2+1):
        v = np.float64(i1)*a_top[0,:]+np.float64(i2)*a_top[1,:]
        x.append(v[0])
        y.append(v[1])
plt.scatter(x,y,color='red',s=1)
x = []
y = []
for i1 in range(-20,40):
    for i2 in range(50):
        v = np.float64(i1)*a_bot[0,:]+np.float64(i2)*a_bot[1,:]
        x.append(v[0])
        y.append(v[1])
#plt.scatter(x,y,color='red',s=1)
plt.xlim(-10,100)
plt.ylim(-10,100)
plt.show()


