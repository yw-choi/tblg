module electrons
  use mpi
  use kinds, only: DP
  use lattice, only: na_uc
  use constants, only: RYTOEV

  implicit none
  public

  character(len=100) :: dir_tb
  real(dp) :: rcut, wgauss
  integer :: nk, nband_tot, nband, ib_start, ib_end
  real(dp), allocatable :: kpoints(:,:)

contains

  subroutine read_wfn ( ik, c_k )

    integer, intent(in) :: ik
    complex(DP), intent(out) :: c_k(na_uc, nband)
    complex(DP) :: tmp(na_uc)
    character(len=100) :: file_unk

    integer :: ib, ia, na_uc_read, nband_tot_read
    real(dp) :: normsq
    complex(dp), external :: zdotc

    write(file_unk, '(a,i0.5,a)') 'unk.',ik,'.dat'

    open(11, file=trim(dir_tb)//'/'//trim(file_unk), &
         form="unformatted")

    read(11) na_uc_read, nband_tot_read

    if (na_uc_read.ne.na_uc.or.nband_tot_read.ne.nband_tot) then
      print *, na_uc_read, na_uc
      print *, nband_tot_read, nband_tot
      call errore('read_wfn', 'dimension mismatch', 1) 
    endif

    do ib = 1, nband_tot
      if (ib_start.le.ib .and. ib.le.ib_end) then
        read(11) (c_k(ia, ib-ib_start+1), ia=1,na_uc)

        normsq = real(zdotc(na_uc, c_k(1:na_uc,ib-ib_start+1), 1, c_k(1:na_uc,ib-ib_start+1), 1))

        c_k(:, ib-ib_start+1) = c_k(:, ib-ib_start+1)/sqrt(normsq)
        
      else
        read(11) (tmp(ia), ia=1,na_uc)
      endif
    enddo

    close(11)

  end subroutine read_wfn

  ! derivative of hopping function w.r.t. ix axis,
  ! evaluated at d
  real(dp) function dtdx ( d, ix ) 
    integer, intent(in) :: ix
    real(dp), intent(in) :: d(3)

    real(DP), parameter :: Vpi  = -2.7d0 / RYTOEV   , &
                           Vsig =  0.48d0 / RYTOEV  , &
                           a0   =  2.46/sqrt(3.d0),  &
                           d0   =  3.35, &
                           r0   =  0.184d0 * 2.46
    real(dp) :: dist, fac1, fac2

    dist = norm2(d)

    !! Do not calculate duplicate parts
    if (ix.eq.1 .or. ix.eq.2) then

      fac1 = d(ix)*d(3)*d(3)/(r0*dist**3)
      fac2 = 2*d(ix)*d(3)*d(3)/dist**4

      dtdx = Vpi*exp(-(dist-a0)/r0)*( fac1 - d(ix)/(r0*dist) + fac2 ) &
             + Vsig*exp(-(dist-d0)/r0)*( -fac1 -fac2 ) 

    else
      fac1 = d(ix)*d(3)*d(3)/(r0*dist**3)
      fac2 = 2*d(3)*(dist**2-d(3)**2)/dist**4

      dtdx = Vpi*exp(-(dist-a0)/r0)*( fac1 - d(ix)/(r0*dist) - fac2 ) &
             + Vsig*exp(-(dist-d0)/r0)*( -fac1 + fac2 )
    endif

  end function dtdx
end module electrons
