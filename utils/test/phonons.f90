module phonons
  use mpi
  use kinds, only: DP
  use lattice, only: na_uc
  use constants, only: RY_TO_CMM1

  implicit none
  public

  character(len=100) :: dir_ph
  integer :: nq, nmode_tot, nmode, imode_start, imode_end
  real(dp), allocatable :: qpoints(:,:), freq(:,:)
  real(dp) :: freq_cut, mass

contains
  subroutine read_freq

    real(dp) :: tmp(nmode_tot)
    integer :: imode, iq

    allocate(freq(nmode, nq))

    if (master) then
      open(11, file=trim(dir_ph)//'/wnq.dat', form="formatted")
      do iq = 1, nq
        read(11, *) tmp

        freq(1:nmode, iq) = tmp(imode_start:imode_end)/RY_TO_CMM1
      enddo

      close(11)
    endif

    call mpi_bcast(freq, nmode*nq, mpi_double_precision, 0, comm, mpierr)

  end subroutine read_freq

  subroutine read_eigenmode ( iq, e_q )

    integer, intent(in) :: iq
    complex(DP), intent(out) :: e_q(3*na_uc, nmode)
    complex(DP) :: tmp(3*na_uc)
    character(len=100) :: file_unq

    integer :: ib, ia, ndim_read, nmode_tot_read, nmode_found_read
    real(dp) :: normsq
    complex(dp), external :: zdotc

    write(file_unq, '(a,i0.5,a)') 'unq.',iq,'.dat'

    open(11, file=trim(dir_ph)//'/'//trim(file_unq), &
         form="unformatted")
    read(11) ndim_read, nmode_tot_read, nmode_found_read

    if (ndim_read.ne.3*na_uc.or.nmode_tot_read.ne.nmode_tot) then
      call errore('read_eigenmode', 'dimension mismatch', 1) 
    endif

    do ib = 1, nmode_tot
      if (imode_start.le.ib .and. ib.le.imode_end) then
        read(11) (e_q(ia, ib-imode_start+1), ia=1,3*na_uc)
      else
        read(11) (tmp(ia), ia=1,3*na_uc)
      endif
      normsq = real(zdotc(3*na_uc, e_q(1:3*na_uc,ib-imode_start+1), 1, &
                              e_q(1:3*na_uc,ib-imode_start+1), 1))

      e_q(:, ib-imode_start+1) = e_q(:, ib-imode_start+1)/sqrt(normsq)
    enddo

    close(11)

  end subroutine read_eigenmode

end module phonons

