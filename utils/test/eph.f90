program test

  implicit none

  double precision, external :: w0gauss, wgauss
  integer :: i, ngauss
  double precision :: x(100), degaussw, y(100)
  character(len=100) :: fn

  degaussw = 0.1
  do ngauss = 0, 5
    write(fn, '(a,I1)') 'ngauss.',ngauss
    open(11, file=trim(fn), form="formatted")
    do i = 1 , 100 
      x(i) = -1+(i-1)*2/(100.d0-1.d0)
      y(i) = w0gauss(x(i)/degaussw, ngauss) / degaussw
      write(11,*) x(i), y(i)
    enddo
    close(11)
  enddo

  ngauss = -99
  write(fn, '(a,I0.3)') 'ngauss.',ngauss
  open(11, file=trim(fn), form="formatted")
  do i = 1 , 100 
    x(i) = -1+(i-1)*2/(100.d0-1.d0)
    y(i) = w0gauss(x(i)/degaussw, ngauss) / degaussw
    write(11,*) x(i), y(i)
  enddo
  close(11)

  ngauss = -1
  write(fn, '(a,I0.3)') 'ngauss.',ngauss
  open(11, file=trim(fn), form="formatted")
  do i = 1 , 100 
    x(i) = -1+(i-1)*2/(100.d0-1.d0)
    y(i) = w0gauss(x(i)/degaussw, ngauss) / degaussw
    write(11,*) x(i), y(i)
  enddo
  close(11)
end program test
