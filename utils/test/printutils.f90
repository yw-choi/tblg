subroutine printblock_start(msg)
  character(len=*) :: msg
  integer :: i, lenmsg, left, right
  integer, parameter :: WIDTH=60

  lenmsg = len(msg)
  write(6, '(a,a,a)', advance='no'), '== ', trim(msg), ' '
  do i=1,WIDTH-4-lenmsg
    write(6, '(a)', advance='no') '='
  enddo
  write(6, '(a)')
end subroutine printblock_start

subroutine printblock_end()
  integer :: i
  integer, parameter :: WIDTH=60

  do i = 1,WIDTH
    write(6, '(a)', advance='no') '='
  enddo
  write(6,'(a)')
  write(6, *)
end subroutine printblock_end
