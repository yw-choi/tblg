module lattice
  use mpi
  use kinds, only: DP
  use constants, only: PI, eps6
  use matrix_inversion, only: invmat

  implicit none
  public

  character(len=100) :: file_struct
  real(dp) :: latt(3,3), relatt(3,3)
  integer :: na_uc, na_sc
  real(dp), allocatable :: atoms_uc(:,:), atoms_sc(:,:)

  integer, allocatable :: sc_uc_map(:), neighbors(:,:), num_neighbors(:)
  integer :: max_neighbors

  integer :: lsc1, lsc2, nsc1, nsc2, nsc 
contains

  subroutine setup_lattice (rcut)
    real(dp), intent(in) :: rcut

    call start_clock('setup_lattice')
    if (master) then
      call printblock_start ( "Crystal Structure" )
    endif

    call read_structure

    call invmat (3, latt, relatt)
    relatt = 2*PI*transpose(relatt)

    call construct_aux_supercell ( rcut )

    call setup_neighbors ( rcut )

    if (master) then
      call printblock_end
    endif
    call stop_clock('setup_lattice')
  end subroutine setup_lattice

  subroutine read_structure
    character(len=100) :: dummy
    integer :: i, stat, ia, ia_sc, ilayer, i1, isc1, i2, isc2

    ! Read structures
    if (master) then
      print *, 'file_struct = ', trim(file_struct)

      open(11, file=trim(file_struct), form="formatted")

      do i = 1, 3 
        read(11,*) dummy, latt(1:3,i)
      enddo

      na_uc = 0 

      do
        read(11,*, iostat=stat) dummy
        if (stat.ne.0) then
          exit
        endif
        na_uc = na_uc + 1
      enddo

      allocate(atoms_uc(3,na_uc))

      rewind 11

      read(11,*) dummy
      read(11,*) dummy
      read(11,*) dummy

      do ia = 1, na_uc
        read(11,*) dummy, atoms_uc(1:3, ia), dummy
      enddo

      close(11)

      print *, "Lattice vectors"
      do i = 1, 3
        print '(1x,3F16.8)', latt(1:3,i)
      enddo

      print *, "Number of atoms in unit cell = ", na_uc
    endif

    call mpi_bcast(latt, 3*3, mpi_double_precision, 0, comm, mpierr)

    call mpi_bcast(na_uc, 1, mpi_integer, 0, comm, mpierr)

    if (.not.master) then
      allocate(atoms_uc(3, na_uc))
    endif

    call mpi_bcast(atoms_uc, 3*na_uc, mpi_double_precision, 0, comm, mpierr)
  end subroutine read_structure

  subroutine construct_aux_supercell ( rcut )
    real(dp), intent(in) :: rcut

    integer :: i, stat, ia, ia_sc, i1, isc1, i2, isc2

    lsc1 = 1 + int(rcut / norm2(latt(1:3,1)) )
    lsc2 = lsc1 ! just in case they differ..
    nsc1 = 2*lsc1+1
    nsc2 = 2*lsc2+1
    nsc = nsc1*nsc2 ! total number of aux. supercells

    na_sc = na_uc * nsc

    allocate(atoms_sc(3, na_sc))
    allocate(sc_uc_map(na_sc))
    atoms_sc(1:3, na_sc) = 0.d0

    ia_sc = 0
    do i1 = 0, nsc1-1
      if (i1.gt.lsc1) then
        isc1 = i1 - nsc1
      else
        isc1 = i1 
      endif

      do i2 = 0, nsc2-1
        if (i2.gt.lsc2) then
          isc2 = i2 - nsc2
        else
          isc2 = i2 
        endif

        do ia = 1, na_uc
          ia_sc = ia_sc + 1
          sc_uc_map(ia_sc) = ia
          atoms_sc(1:3, ia_sc) = matmul(latt(:,:),(/ isc1, isc2, 0 /)) &
                                 + atoms_uc(1:3, ia)
        enddo
      enddo
    enddo

    if (master) then
      print '(1x,a,I3,a,I3)', "Aux. supercell = ", nsc1, ' x ', nsc2
      print *, 'Number of atoms in aux. supercell = ', na_sc
    endif

    call mpi_barrier ( comm, mpierr )

  end subroutine construct_aux_supercell

  subroutine setup_neighbors ( rcut )
    real(DP), intent(in) :: rcut

    integer :: ia, ja_sc, ja, nnb, inb
    real(dp) :: dist

    allocate(num_neighbors(na_uc))

    do ia = 1, na_uc
 
      nnb = 0 

      ! count the number of atoms in aux. sc that are within the rcut.
      do ja_sc = 1, na_sc
        dist = norm2(atoms_sc(1:3,ja_sc) - atoms_uc(1:3,ia))
        if (dist.gt.eps6.and.dist.lt.rcut) then
          nnb = nnb + 1
        endif
      enddo

      num_neighbors(ia) = nnb
    enddo 

    max_neighbors = maxval(num_neighbors)

    if (master) then
      print *, "max_neighbors = ", max_neighbors
    endif

    allocate(neighbors(max_neighbors, na_uc))

    neighbors = 0

    ! Repeat a similar loop, and keep the neighbor indices
    do ia = 1, na_uc
      inb = 0
      do ja_sc = 1, na_sc
        dist = norm2(atoms_sc(1:3,ja_sc) - atoms_uc(1:3,ia))
        if (dist.gt.eps6.and.dist.lt.rcut) then
          inb = inb + 1
          neighbors(inb, ia) = ja_sc
        endif
      enddo
    enddo

    ! DBG
    ! do ia = 1, na_sc
    !   print '(1x,3F8.3)', atoms_sc(:,ia)
    ! enddo
    ! stop
    ! do ia = 1, na_uc
    !   print *, num_neighbors(ia)
    !   do inb = 1, num_neighbors(ia)
    !     print '(1x,I5,3F8.3,1x,F8.3)', neighbors(inb, ia), atoms_sc(1:3,neighbors(inb, ia)), norm2(atoms_uc(:,ia)-atoms_sc(:,neighbors(inb, ia)))
    !   enddo
    ! enddo
  end subroutine setup_neighbors
end module lattice
