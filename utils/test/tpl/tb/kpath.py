#!/usr/bin/env python
### generates k-points along band lines
### [Usage] kpath.py pwscf_input
from numpy import *
import sys

DEBUG = False

if len(sys.argv)==1:
    print "[Usage] kpath.py pwscf_input"
    exit(-1)

fn = sys.argv[1]


# Read band lines
f = open(fn, 'r')

l = f.readline()
if l.strip() != 'K_POINTS crystal_b':
    print "-----------------------------------------------------"
    print "[Error] input file should be in the following format."
    print "-----------------------------------------------------"
    print "K_POINTS crystal_b"
    print "nks"
    print "xk_x(1) xk_y(1) xk_z(1) wk(1) label(1)"
    print "xk_x(2) xk_y(2) xk_z(2) wk(2) label(2)"
    print "..."
    exit(-1)

nks = int(f.readline())

# Read input
xks = []
wks = []
labels = []
for ik in range(nks):
    row = f.readline().split()
    xks.append(float64(row[:3]))
    wks.append(int(row[3]))
    if len(row)==5:
        labels.append(row[4])
    else:
        labels.append('')
f.close()

xks = array(xks)

if (DEBUG):
    # Print input
    print "K_POINTS crystal_b"
    print nks
    for ik in range(nks):
        print '%10.8f  %10.8f  %10.8f  %d  %s'%(xks[ik][0], xks[ik][1], xks[ik][2], wks[ik], labels[ik])

kpoints = []

# add the first point
kpoints.append(xks[0,:])

for ik in range(nks-1):
    xk_start = xks[ik,:]
    xk_end   = xks[ik+1,:]
    wk       = wks[ik] # number of intermediate k-points

    dk = (xk_end - xk_start)/wk

    for i in range(wk):
        k = xk_start + dk*(i+1)
        kpoints.append(k)

kpoints = array(kpoints)
nktot = len(kpoints)

# print k-points
weight = 1./nktot
print '# nk'
print '#%9s  %10s  %10s  %10s'%('kx', 'ky', 'kz', 'wk')
print nktot
for ik, k in enumerate(kpoints):
    print '%10.8f  %10.8f  %10.8f  %10.8f'%(k[0], k[1], k[2], weight)
