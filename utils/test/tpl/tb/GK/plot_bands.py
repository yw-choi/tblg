# Tight-binding model calculation of twisted bilayer graphene with arbitrary commensurate rotation angle.
# Reference: S. Fang and E. Kaxiras, Phys. Rev. B 93, 235153 (2016)
from numpy import *
import matplotlib.pyplot as plt


def main():
    ymax = 10
    ymin = -10

    enk = loadtxt('enk.dat')
    nk, nb = shape(enk)
    for ib in range(nb):
        plt.plot(arange(1,nk+1),enk[:,ib], 'r-', lw=0.5)
    plt.axhline(0.300)
    plt.scatter(193, enk[193,0])
    plt.scatter(193, enk[193,1])

    # enk = loadtxt('enk.FK.dat')
    # nk, nb = shape(enk)
    # for ib in range(nb):
    #     if ib == 0:
    #         plt.plot(enk[:,ib], 'b-', lw=0.5, label="FK")
    #     else:
    #         plt.plot(enk[:,ib], 'b-', lw=0.5)

    # enk = loadtxt('enk.SK.FK.dat')
    # nk, nb = shape(enk)
    # for ib in range(nb):
    #     if ib == 0:
    #         plt.plot(enk[:,ib], 'k-', lw=0.5, label="SK+FK")
    #     else:
    #         plt.plot(enk[:,ib], 'k-', lw=0.5)

    plt.ylim(ymin, ymax)
    plt.legend()
    gridlinespec = { 'color': 'darkgrey',
                    'linestyle': ':',
                    'linewidth': 0.5 }
    plt.grid(True, axis='x', **gridlinespec)
    plt.axhline(0,  **gridlinespec)
    plt.ylabel('Energy (eV)')
    plt.savefig('bands.pdf')
    plt.show()

    return

main()

