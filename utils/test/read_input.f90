! Read input parameters from the standard input
subroutine read_input
  use mpi
  use kinds, only: dp
  use lattice, only: file_struct
  use constants, only: RYTOEV, AMU_RY
  use electrons, only: dir_tb, rcut, wgauss, nk, kpoints, &
                       nband_tot, nband, ib_start, ib_end
  use phonons, only: dir_ph, nq, qpoints, &
                     nmode_tot, nmode, imode_start, imode_end, &
                     freq_cut, mass

  namelist/input/ file_struct, wgauss, dir_tb, dir_ph, &
                  rcut, nband_tot, ib_start, ib_end, &
                  nmode_tot, imode_start, imode_end, &
                  freq_cut, mass
                  
  integer :: ik, iq

  if (master) then
    print *, "Reading input parameters ..."
    read(5, input)

    call printblock_start('Input Parameters')
    write(6, input)
    call printblock_end

  endif
  call mpi_bcast(file_struct, 100, mpi_character, 0, comm, mpierr)
  call mpi_bcast(wgauss, 1, mpi_double_precision, 0, comm, mpierr)
  call mpi_bcast(mass, 1, mpi_double_precision, 0, comm, mpierr)
  call mpi_bcast(freq_cut, 1, mpi_double_precision, 0, comm, mpierr)
  call mpi_bcast(dir_tb, 100, mpi_character, 0, comm, mpierr)
  call mpi_bcast(dir_ph, 100, mpi_character, 0, comm, mpierr)
  call mpi_bcast(rcut, 1, mpi_double_precision, 0, comm, mpierr)
  call mpi_bcast(nband_tot, 1, mpi_integer, 0, comm, mpierr)
  call mpi_bcast(ib_start, 1, mpi_integer, 0, comm, mpierr)
  call mpi_bcast(ib_end, 1, mpi_integer, 0, comm, mpierr)
  call mpi_bcast(nmode_tot, 1, mpi_integer, 0, comm, mpierr)
  call mpi_bcast(imode_start, 1, mpi_integer, 0, comm, mpierr)
  call mpi_bcast(imode_end, 1, mpi_integer, 0, comm, mpierr)

  nband = ib_end - ib_start + 1
  nmode = imode_end - imode_start + 1

  mass = mass * AMU_RY
  freq_cut = freq_cut / RYTOEV

  if (master) then
    read(5, *) nk
  endif
  call mpi_bcast(nk, 1, mpi_integer, 0, comm, mpierr)

  allocate(kpoints(3,nk))

  if (master) then
    do ik = 1, nk
      read(5, *) kpoints(1:3,ik)
    enddo
  endif

  call mpi_bcast(kpoints, nk*3, mpi_double_precision, 0, &
                 comm, mpierr)

  if (master) then
    read(5, *) nq
  endif

  call mpi_bcast(nq, 1, mpi_integer, 0, comm, mpierr)
  allocate(qpoints(3,nq))

  if (master) then
    do iq = 1, nq
      read(5, *) qpoints(1:3,iq)
    enddo
  endif

  call mpi_bcast(qpoints, nq*3, mpi_double_precision, 0, &
                 comm, mpierr)

  if (master) then
    call printblock_start ("k,q grid")
    print *, "Number of k-points = ", nk
    print '(a)', repeat("=",60)
    print '(a5,a20)', "ik","Frac."
    do ik = 1, nk
      print '(I5,3F10.5)', ik, kpoints(1:3,ik)
    enddo
    print '(a)', repeat("=",60)

    print *, "Number of q-points = ", nq
    print '(a)', repeat("=",60)
    print '(a5,a20)', "iq","Frac."
    do iq = 1, nq
      print '(I5,3F10.5)', iq, qpoints(1:3,iq)
    enddo
    print '(a)', repeat("=",60)
    call printblock_end
  endif
end subroutine read_input
