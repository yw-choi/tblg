from numpy import *
import matplotlib.pyplot as plt

dat = loadtxt('test')

plt.scatter(dat[:,0], dat[:,1])
plt.scatter(0,0)
plt.xlim(-8,8)
plt.ylim(-8,8)
plt.show()
