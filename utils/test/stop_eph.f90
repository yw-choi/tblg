! Gracefully terminate the program
subroutine stop_eph
  use mpi

  call stop_clock ( 'EPH' )

  if (rank.eq.0) then 
    call printblock_start ('Timing info')
    call print_clock ( ' ' )
    call printblock_end()
    call printblock_end()
    call system('rm -f CRASH')

    call print_memory
  endif

  call timestamp('End of run')
  call MPI_Finalize(mpierr)
end subroutine stop_eph

subroutine print_memory

  use lattice, only: na_uc, na_sc, max_neighbors
  use electrons, only: nk
  use phonons, only: nq

  double precision :: memtot

  integer, parameter :: SIZE_INT   = 4, &
                        SIZE_REAL  = 8, &
                        SIZE_CMPLX = 16

  memtot = 0.d0
  memtot = memtot + SIZE_INT*nk*nq ! kqmap
  memtot = memtot + SIZE_INT*3*nk*nq ! kqmap_G

  memtot = memtot + SIZE_REAL*3*max_neighbors*na_uc ! epmat_orb
  memtot = memtot + SIZE_CMPLX*na_sc*nk             ! phase_fac
  memtot = memtot + SIZE_CMPLX*na_uc*nband*nk       ! c_k
  memtot = memtot + SIZE_CMPLX*3*na_uc*nmode*nq     ! e_q
  memtot = memtot + SIZE_CMPLX*nband*nband*nk*nmode*nq ! epmat_band

  memtot = memtot + SIZE_REAL*3*na_uc ! atoms_uc
  memtot = memtot + SIZE_REAL*3*na_sc ! atoms_sc
  memtot = memtot + SIZE_INT*na_sc    ! sc_uc_map
  memtot = memtot + SIZE_INT*na_uc    ! num_neighbors
  memtot = memtot + SIZE_INT*max_neighbors*na_uc    ! neighbors

  memtot = memtot + SIZE_REAL*nmode*nq ! freq

  memtot = memtot + SIZE_REAL*3*nk ! kpoints
  memtot = memtot + SIZE_REAL*3*nq ! qpoints

  call printblock_start ('Memory info')
  print *, "Number of atoms in unit cell = ", na_uc
  print *, "Number of atoms in aux. supercell = ", na_sc

  print "(1x,A,F8.1,A)", "Estimated peak memory per process = ", (memtot/1024.d0/1024.d0), " MB"

  call printblock_end

end subroutine print_memory
