from numpy import *
import matplotlib.pyplot as plt

ngauss = arange(0,6)

for ng in ngauss:
    data = loadtxt('ngauss.%d'%ng)
    plt.plot(data[:,0], data[:,1], '.-', label='ngauss=%d'%ng)
    print sum(data[:,1])*(data[1,0]-data[0,0])
data = loadtxt('ngauss.-099')
plt.plot(data[:,0], data[:,1], '.-', label='ngauss=-99')
print sum(data[:,1])*(data[1,0]-data[0,0])
data = loadtxt('ngauss.-001')
plt.plot(data[:,0], data[:,1], '.-', label='ngauss=-1')
print sum(data[:,1])*(data[1,0]-data[0,0])
plt.legend()
plt.xlim(-.5,.5)
plt.savefig('plot.png', dpi=400)
plt.show()
