module elph
  use mpi
  use electrons, only: nk, kpoints, rcut, nband, read_wfn, &
                       dtdx
  use phonons, only: nq, qpoints, nmode, read_eigenmode, read_freq, freq, mass, freq_cut
  use constants, only: DP, AMU_RY, RYTOEV, eps6
  use lattice, only: na_uc, na_sc, atoms_uc, atoms_sc, latt, &
                     relatt, sc_uc_map, max_neighbors, num_neighbors, neighbors

  implicit none
  public

  integer, allocatable :: kqmap(:,:), kqmap_G(:,:,:)

  complex(DP), allocatable :: &
    phase_fac(:,:),     & ! phase_fac(na_sc, nk) 
                          !  electrons phase factor: exp(i*k*tau_i)
    epmat_band(:,:,:,:,:)    ! epmat_band(nband,nband,nk,nmode,nq)
                             !  el-ph matrix elements in band basis
  real(dp), allocatable :: &
    epmat_orb(:,:,:)         ! epmat_orb(3, max_neighbors, na_uc)
                             !  el-ph matrix elements in orbital basis

contains

  subroutine build_kqmap
    integer :: ik, iq, ikq
    real(DP) :: k(3), q(3), kq(3), kq_1BZ(3), dk(3)
    logical :: found

    call start_clock('kqmap')
    if (master) then
      call printblock_start ( "Building kqmap" )
    endif

    allocate(kqmap(nk,nq), kqmap_G(3,nk,nq))

    kqmap = -1
    kqmap_G = 0

    do ik = 1, nk
      k = kpoints(1:3, ik)
      do iq = 1, nq
        q = qpoints(1:3, iq)
      
        kq = k + q 

        kqmap_G(1:3, ik, iq) = floor(kq(1:3))
        kq_1BZ(1:3) = kq(1:3) - kqmap_G(1:3, ik, iq)

        found = .false.

        do ikq = 1, nk
          dk = kpoints(1:3, ikq) - kq_1BZ(1:3)

          if ( abs(dk(1)).lt.eps6 .and. abs(dk(2)).lt.eps6 .and. abs(dk(3)).lt.eps6 ) then
            found = .true.
            kqmap(ik, iq) = ikq
            exit
          endif
        enddo

        if (.not.found) then
          print *, ik, iq
          print *, k
          print *, q
          call errore('build_kqmap', 'k+q not found among k', 1)
        endif
      enddo
    enddo

    ! DBG
    ! do ik = 1, nk
    !   k = kpoints(ik, 1:2)
    !   do iq = 1, nq
    !     q = qpoints(iq, 1:2)
    !     print '(2I4,6F8.4, 2I4, 2F8.4, I4)', ik,iq, k, q, k+q, kqmap_G(1:2,ik,iq), k+q-kqmap_G(1:2,ik,iq), kqmap(ik,iq)
    !   enddo
    ! enddo
    if (master) then
      print *, 'DONE!'
      call printblock_end
    endif
    call stop_clock('kqmap')
  end subroutine build_kqmap

  ! Compute electron-phonon matrix elements in tight-binding orbital basis
  subroutine compute_epmat_orbital

    integer :: ia, inb, ix
    real(dp) :: at_I(3), at_J(3), dx(3)

    call start_clock ('epmat_orb')
    if (master) then
      call printblock_start ( "Computing epmat. in orbital basis" )
    endif

    allocate(epmat_orb(3, max_neighbors, na_uc))

    epmat_orb = 0.d0

    do ia = 1, na_uc
      at_I = atoms_uc(1:3, ia)
      do inb = 1, num_neighbors(ia)
        at_J = atoms_sc(1:3, neighbors(inb, ia))
        dx = at_I - at_J 
        do ix = 1, 3
          epmat_orb(ix, inb, ia) = dtdx(dx, ix)
        enddo
      enddo
    enddo

    if (master) then
      print *, 'DONE!'
      call printblock_end
    endif
    call stop_clock ('epmat_orb')
  end subroutine compute_epmat_orbital

  subroutine compute_phase_fac

    integer :: ik, iq, imode, ibnd, jbnd, ikq
    integer :: ia, ix, ja, ieq, ja_uc, inb
    real(DP) :: k(3), q(3), kq(3), at_I(3), at_J(3), arg

    complex(DP) :: mat, cfac1, cfac2, elec_fac
    call start_clock ('phase_fac')
    if (master) then
      call printblock_start ( "Computing electron phase factors" )
    endif

    allocate(phase_fac(na_sc, nk))
    phase_fac = 0.d0

    do ik = 1, nk
      k = matmul(relatt(1:3, 1:3), kpoints(1:3, ik))
      do ia = 1, na_sc
        at_I = atoms_sc(1:3, ia)
  
        arg = dot_product(k, at_I)
        phase_fac(ia,ik) = cmplx(cos(arg),sin(arg),kind=dp)
      enddo
    enddo

    if (master) then
      print *, 'DONE!'
      call printblock_end
    endif
    call stop_clock ('phase_fac')
  end subroutine compute_phase_fac

  ! Compute electron-phonon matrix elements in band basis
  subroutine compute_epmat_band

    integer :: ik, iq, imode, ibnd, jbnd, ikq
    integer :: ia, ix, ja, ieq, ja_uc, inb, ja_sc

    real(DP) :: dx(3), dist, k(3), q(3), kq(3), at_I(3), &
                at_J(3), arg

    complex(DP) :: mat, cfac1, cfac2, elec_fac
    complex(DP), allocatable :: c_k(:,:,:), c_kq(:,:,:), e_q(:,:,:)

    call start_clock ('epmat_band')
    if (master) then
      call printblock_start ( "Computing epmat in band basis" )
    endif

    call read_freq ! phonon frequencies

    ! read all the electron wfns and phonon eigenmodes
    allocate(c_k(na_uc, nband, nk), e_q(na_uc*3, nmode, nq))

    do ik = 1, nk
      call read_wfn ( ik, c_k(:,:,ik) )
    enddo

    do iq = 1, nq
      call read_eigenmode ( iq, e_q(:,:,iq) )
    enddo

    allocate(epmat_band(nband,nband,nk,nmode,nq))
    epmat_band = 0.d0

    do ik = 1, nk

      print "(a,5I4)", "ik = ", ik
      k = matmul(relatt(1:3, 1:3), kpoints(1:3, ik))

      do iq = 1, nq
        q = matmul(relatt(1:3, 1:3), qpoints(1:3, iq))

        ikq = kqmap(ik,iq)
        kq = k+q

        do imode = 1, nmode
          if (freq(imode, iq).lt.freq_cut) then
            epmat_band(:,:,ik,imode,iq) = 0.d0
            cycle
          endif

          do ibnd = 1, nband
            do jbnd = 1, nband

              mat = 0.d0 

              do ia = 1, na_uc
                at_I = atoms_uc(1:3, ia)
                do inb = 1, num_neighbors(ia)
                  ja_sc = neighbors(inb, ia)

                  at_J = atoms_sc(1:3, ja_sc)
                  dx = at_I - at_J

                  ja_uc = sc_uc_map(ja_sc)

                  cfac1 = phase_fac(ja_sc,ik) &
                          *conjg(phase_fac(ia,ikq))
                  cfac2 = phase_fac(ia,ik) &
                          *conjg(phase_fac(ja_sc,ikq))

                  elec_fac = &
                    cfac1*conjg(c_k(ia,jbnd,ikq))*c_k(ja_uc,ibnd,ik) &
                    + cfac2*conjg(c_k(ja_uc,jbnd,ikq))*c_k(ia,ibnd,ik)
                  
                  do ix = 1, 3
                    ieq = 3*(ia-1)+ix

                    mat = mat + epmat_orb(ix,inb,ia)*e_q(ieq, imode, iq)*elec_fac

                  enddo ! ix
                enddo
              enddo ! ia

              epmat_band(ibnd,jbnd,ik,imode,iq) = sqrt(1.d0/(2*mass*freq(imode,iq)))*mat

            enddo ! jbnd
          enddo ! ibnd
        enddo ! imode
      enddo ! iq
    enddo ! ik

    deallocate(c_k, e_q)

    if (master) then
      print *, 'DONE!'
      call printblock_end
    endif
    call stop_clock ('epmat_band') 
  end subroutine compute_epmat_band

  subroutine export_epmat

    integer :: ik, iq, imode, ibnd, jbnd, i, inb

    call start_clock('export_epmat')
    if (master) then
      call printblock_start ('Exporting epmat data')
    endif

    if (master) then

      ! crystal structure data
      open(11, file="crystal.dat", form="formatted")
      do i = 1,3
        write(11, '(3F16.8)') latt(1:3,i)
      enddo
      write(11, *) na_uc
      do i = 1,na_uc
        write(11, '(3F16.8)') atoms_uc(1:3,i)
      enddo
      write(11, *) na_sc
      do i = 1,na_sc
        write(11, '(3F16.8)') atoms_sc(1:3,i)
      enddo
      close(11)

      ! epmat_band
      open(11, file="epmat_band.dat", form="unformatted")
      write(11) nband, nband, nk, nmode, nq
      write(11) epmat_band
      close(11)

    endif

    if (master) then

      print *, 'DONE!'
      call printblock_end

    endif
    call stop_clock ('export_epmat')
  end subroutine export_epmat
end module elph
