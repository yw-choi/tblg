from numpy import *
import matplotlib.pyplot as plt

def main():

    enk = loadtxt('enk.dat')

    nk, nb = shape(enk)
    dat = enk.flatten()

    emin = -4.0
    emax =  4.0
    N_E  =  400
    sig  =  0.005 # eV

    energy = linspace(emin, emax, N_E)

    dos = zeros((N_E,2))
    dos[:,0] = energy

    for ie, ene in enumerate(energy):

        dE = dat - ene

        arg = dE*dE/(4.0*sig)

        for ar in arg:
            if ar < 40:
                dos[ie,1] += exp(-ar)

    dos[:,1] = dos[:,1] / (2*sqrt(pi*sig)) / nk

    savetxt('dos.dat', dos)

    plt.plot(dos[:,0], dos[:,1], '-')
    plt.show()

    return

main()
