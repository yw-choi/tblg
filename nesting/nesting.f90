program nesting
  use mpi
  use kinds, only: DP
  use constants, only: RYTOEV
  use ktetra, only: &
    tetra, tetra_type, tetra_init, &
    ntetra, linear_tetra_weights_only, tetra_dos_t
  use kgrid, only: &
    kpoint_grid 

  implicit none
  logical :: time_reversal
  integer :: &
    nrot, &
    s(3,3,48), &
    t_rev(48)

  integer :: isym, is, nspin, ik, ibnd, ik_irr, iq, jbnd, ikq, ikq_irr, ikq_sym
  real(dp) :: nelec, e_f

  integer, parameter :: &
    nbnd = 21, &
    dimk = 120

  integer :: nk, nktot

  real(dp), allocatable :: &
    wnk_irr(:,:), dwnk_irr(:,:), dwnk(:,:), wnk(:,:)

  real(dp), allocatable :: &
    nesting_fn(:)

  real(dp) :: &
    dlt

  real(dp), allocatable :: &
    enk(:,:), &
    xk(:,:), &  ! k-points in frac. cooridnate
    xk_cart(:,:), &  
    wk(:)       ! k-points weight

  integer, allocatable :: &
    isk(:), &
    equiv(:), &
    equiv_s(:), &
    ik_stars(:,:),  &
    ik_isym(:,:), &
    n_stars(:)

  character(len=100) :: fn

  call mpi_initialize
  call timestamp('Start of run')

  call init_clocks ( .true. )

  e_f = -0.87d0 / (RYTOEV*1e3)

  nktot = dimk*dimk

  t_rev = 0
  s = 0
  time_reversal = .true.
  nrot = 12
  allocate(xk(3,nktot), wk(nktot), equiv(nktot), equiv_s(nktot), xk_cart(3,nktot))
  xk = 0.d0
  wk = 0.d0

  isym =  1   ! identity                                     
  s(1,:,isym) = (/  1,  0,  0 /)
  s(2,:,isym) = (/  0,  1,  0 /)
  s(3,:,isym) = (/  0,  0,  1 /)

  isym =  2   ! 180 deg rotation - cart. axis [0,0,1]        
  s(1,:,isym) = (/ -1,  0,  0 /)
  s(2,:,isym) = (/  0, -1,  0 /)
  s(3,:,isym) = (/  0,  0,  1 /)
  
  isym =  3   !  60 deg rotation - cryst. axis [0,0,1]       
  s(1,:,isym) = (/  0,  1,  0 /)
  s(2,:,isym) = (/ -1,  1,  0 /)
  s(3,:,isym) = (/  0,  0,  1 /)
  
  isym =  4   !  60 deg rotation - cryst. axis [0,0,-1]      
  s(1,:,isym) = (/  1, -1,  0 /)
  s(2,:,isym) = (/  1,  0,  0 /)
  s(3,:,isym) = (/  0,  0,  1 /)

  isym =  5   ! 120 deg rotation - cryst. axis [0,0,1]       
  s(1,:,isym) = (/ -1,  1,  0 /)
  s(2,:,isym) = (/ -1,  0,  0 /)
  s(3,:,isym) = (/  0,  0,  1 /)

  isym =  6   ! 120 deg rotation - cryst. axis [0,0,-1]      
  s(1,:,isym) = (/  0, -1,  0 /)
  s(2,:,isym) = (/  1, -1,  0 /)
  s(3,:,isym) = (/  0,  0,  1 /)

  isym =  7   ! mirror 
  s(1,:,isym) = (/  0,  1,  0 /)
  s(2,:,isym) = (/  1,  0,  0 /)
  s(3,:,isym) = (/  0,  0, -1 /)

  isym =  8   ! mirror + 180 deg rotation - cart. axis [0,0,1]        
  s(1,:,isym) = (/  0, -1,  0 /)
  s(2,:,isym) = (/ -1,  0,  0 /)
  s(3,:,isym) = (/  0,  0, -1 /)
  
  isym =  9   ! mirror + 60 deg rotation - cryst. axis [0,0,1]       
  s(1,:,isym) = (/ -1,  1,  0 /)
  s(2,:,isym) = (/  0,  1,  0 /)
  s(3,:,isym) = (/  0,  0, -1 /)
  
  isym = 10   ! mirror + 60 deg rotation - cryst. axis [0,0,-1]      
  s(1,:,isym) = (/  1,  0,  0 /)
  s(2,:,isym) = (/  1, -1,  0 /)
  s(3,:,isym) = (/  0,  0, -1 /)

  isym = 11   ! mirror + 120 deg rotation - cryst. axis [0,0,1]       
  s(1,:,isym) = (/ -1,  0,  0 /)
  s(2,:,isym) = (/ -1,  1,  0 /)
  s(3,:,isym) = (/  0,  0, -1 /)

  isym = 12   ! mirror + 120 deg rotation - cryst. axis [0,0,-1]      
  s(1,:,isym) = (/  1, -1,  0 /)
  s(2,:,isym) = (/  0, -1,  0 /)
  s(3,:,isym) = (/  0,  0, -1 /)


  call kpoint_grid (nrot, time_reversal, .false., &
    s, t_rev, nktot, 0,0,0,dimk,dimk,1, nk, xk, wk, &
    equiv, equiv_s, n_stars, ik_stars, ik_isym )

  allocate(enk(nbnd,nk), wnk_irr(nbnd,nk), dwnk_irr(nbnd,nk), dwnk(nbnd,nktot), wnk(nbnd,nktot))

  open(11, file='tb/enk.dat', form='formatted')
  read(11,*)
  do ik = 1, nk
    read(11,*) enk(:,ik)
  enddo
  close(11)

  tetra_type = 0
  CALL tetra_init ( nrot, s, time_reversal, t_rev, nktot, &
       0,0,0, dimk,dimk,1, nk, xk)
  allocate(isk(nk))
  isk = 1
  is = 0
  nspin = 1
  nelec = 0.d0 !! not used in tetra_weights_only
  call linear_tetra_weights_only (nk, nspin, is, isk, nbnd, ntetra, &
    tetra, enk, e_f, wnk_irr, dwnk_irr)
  do ik = 1, nktot
    ik_irr = equiv(ik)
    dwnk(:,ik) = dwnk_irr(:,ik_irr)/(wk(ik_irr)*nktot)
  enddo
  dwnk = dwnk/sum(dwnk)
  do ik = 1, nktot
    ik_irr = equiv(ik)
    wnk(:,ik) = wnk_irr(:,ik_irr)/(wk(ik_irr)*nktot)
  enddo

  dlt = 0.1d0/RYTOEV/1e3
  allocate(nesting_fn(nktot))
  nesting_fn = 0.d0
  do iq = 1, nktot
    do ik = 1, nktot
      call get_kq(ik, iq, ikq)
      do ibnd = 1, nbnd
        do jbnd = 1, nbnd
          ! nesting_fn(iq) = nesting_fn(iq) + dwnk(ibnd,ik)*dwnk(jbnd,ikq)
          nesting_fn(iq) = nesting_fn(iq) + (enk(jbnd,ikq)-enk(ibnd,ik))*(wnk(ibnd,ik)-wnk(jbnd,ikq))/((enk(jbnd,ikq)-enk(ibnd,ik))**2+dlt**2)
        enddo
      enddo
    enddo
  enddo
  open(11, file="nesting_fn.dat", form="formatted")
  do iq = 1, nktot
    write(11, *) nesting_fn(iq)
  enddo
  close(11)

  call stop_nesting
contains 
  subroutine get_kq(ik, iq, ikq)
    integer, intent(in) :: ik, iq
    integer, intent(out) :: ikq

    integer :: ik1, ik2, iq1, iq2, ikq1, ikq2
    real(dp) :: k_irr(3), k(3), q(3), kq(3)

    ik1 = int((ik-1)/dimk)
    ik2 = mod(ik-1, dimk)

    iq1 = int((iq-1)/dimk)
    iq2 = mod(iq-1, dimk)

    ikq1 = ik1+iq1
    ikq2 = ik2+iq2

    if (ikq1.ge.dimk) then
      ikq1 = ikq1 - dimk
    endif

    if (ikq2.ge.dimk) then
      ikq2 = ikq2 - dimk
    endif

    ikq = ikq1*dimk + ikq2 + 1
  end subroutine get_kq
end program nesting
