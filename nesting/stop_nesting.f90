! Gracefully terminate the program
subroutine stop_nesting
  use mpi

  call timestamp('End of run')
  call MPI_Finalize(mpierr)
end subroutine stop_nesting
