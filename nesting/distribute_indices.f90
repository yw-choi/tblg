subroutine distribute_indices(n, nprocs, displs, recvcounts)
  integer, intent(in) :: n, nprocs
  integer, intent(out) :: displs(0:nprocs-1),recvcounts(0:nprocs-1)

  integer :: iproc, nloc, iloc

  do iproc=0,nprocs-1

      nloc = int(n/nprocs)

      if (iproc < mod(n, nprocs)) then
        nloc = nloc + 1
        iloc = iproc*int(n/nprocs)+iproc
      else
        iloc = iproc*int(n/nprocs)+mod(n, nprocs)
      endif

      displs(iproc) = iloc
      recvcounts(iproc) = nloc
  enddo

end subroutine distribute_indices
