from numpy import *
import matplotlib.pyplot as plt
from matplotlib.mlab import griddata
from scipy import interpolate
from scipy import optimize


# WIEN2k Fermi surface plot

a = 7.130695*.529177 # Ang
Ry = 13.605 # eV
case = 'gga.nm.dmft.beta100'  # case name
nfiles = 3       # number of energy files
lb = 32         # lower band index, 0-based
hb = 50          # upper band index, 0-based
nbands = hb-lb+1
def main():
    ef = read_fermi_energy()
    tol = 0.01 # eV
    nks, kpoints_frac, enk, enk_im = read_energies()
    kpoints = 2*pi/a*kpoints_frac

    # which bands cross the fermi level?
    fs_bands = []
    for ib in range(nbands):
        if enk[:,ib].max()<ef or \
            enk[:,ib].min()>ef:
                continue
        fs_bands.append(ib)

    fs_lines = []

    kx = kpoints[:,0].reshape(nks[0],nks[1])
    ky = kpoints[:,1].reshape(nks[0],nks[1])
    for i, ib in enumerate(fs_bands):
        ek = enk[:,ib].reshape(nks[0],nks[1])
        cs = plt.contour(kx, ky, ek-ef, levels=[0], colors='C%d'%(i+1))
        p = cs.collections[0].get_paths()[0]
        v = p.vertices
        fs_lines.append(v)
    save('fs', fs_lines)
    print 'fs.npy saved, shape(fs_lines) = ', shape(array(fs_lines))
    print 'DONE'

def read_fermi_energy():
    f = open('EF.dat', 'r')
    ef = float(f.readline())
    f.close()
    return ef

def read_energies():
    nkpts = 351
    # Read enk for irreducible kpoints
    k_irr = arange(1,nkpts+1)
    # enk_irr = array(enk_irr)*Ry # converted to eV
    enk_irr = loadtxt('./enk_qp.dat')
    enk_irr_im = loadtxt('./enk_qp_im.dat')

    # read full kpoint list from .outputkgen
    ik_to_irr = []
    kpoints = []
    enk = []
    enk_im = []
    f = open('%s.outputkgen'%case, 'r')
    while True:
        l = f.readline()

        if not l:
            print '[Error] full k-points list are not found'
            exit(-1)

        if 'DIVISION OF RECIPROCAL LATTICE VECTORS (INTERVALS)=' in l:
            nks = int32(l.split()[-3:])
            f.readline() # header

            while True:
                l = f.readline()
                if 'weights of k-points' in l:
                    break

                row = l.split()
                kp = float64(row[1:4])

                if kp[0]==nks[0] or kp[1]==nks[1] or kp[2]==nks[2]:
                    continue
                ik = int32(row[0])
                ik_irr = int32(row[4])
                ik_to_irr.append(ik_irr)
                kp[0] = kp[0]/nks[0]
                kp[1] = kp[1]/nks[1]
                kp[2] = kp[2]/nks[2]
                kpoints.append(kp)

            # ik_irr to k_irr index
            ik_irr_map = {}
            for ik in range(len(k_irr)):
                row = int32(f.readline().split()[0:2])
                ik_irr_map[row[1]] = row[0]-1

            # map enk
            for ik in range(len(kpoints)):
                ik_irr = ik_to_irr[ik]
                enk.append(enk_irr[ik_irr_map[ik_irr],:])
                enk_im.append(enk_irr_im[ik_irr_map[ik_irr],:])

            break
    f.close()

    print 'Number of irreducible k-points = ', len(k_irr)
    print 'Number of full k-points = ', len(kpoints)
    print 'shape(enk) = ', shape(enk)
    print 'shape(enk_im) = ', shape(enk_im)
    return nks, array(kpoints), array(enk), array(enk_im)

if __name__ == '__main__':
    main()
