from numpy import *
import matplotlib.pyplot as plt

# dos = loadtxt('edos.bloechl.dat')
# plt.plot(dos[:,0], dos[:,1], label="Bloechl")
# dos = loadtxt('edos.linear.dat')
# plt.plot(dos[:,0], dos[:,1], label="Linear")
# dos = loadtxt('edos.kawamura.dat')
# plt.plot(dos[:,0], dos[:,1], label="Kawamura")
# plt.legend()

dos = loadtxt('edos.dat')
plt.plot(dos[:,0], dos[:,1])
plt.xlim(-2,2)

plt.show()
