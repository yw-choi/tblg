module eliashberg

  use mpi
  use kinds, only: dp
  use env, only: data_ep
  use constants, only: TPI, RYTOEV, eps12

  use lattice, only: &
    latt, relatt, &
    nrot, s, t_rev, time_reversal

  use epmat, only: &
    dimk, nkmax, nqmax, nq, nk, xk, wk, &
    equiv, equiv_s, ik_stars, ik_isym, n_stars, &
    nbnd, enk, nmode, imodes, freq, &
    acoustic_cut, get_kq, get_ik, &
    IU_EPMAT

  use io, only: &
    io_2d_real_fmt

  use ktetra, only: &
    tetra, tetra_type, tetra_init, &
    ntetra, linear_tetra_weights_only, tetra_dos_t

  implicit none

  logical :: leliashberg, tetrahedron

  integer :: nef, ief
  real(dp) :: &
    efmin, efmax, degaussw, & 
    e_f, & ! Fermi energy (Ry)
    N_F    ! DOS per spin at the Fermi energy (states/spin/Ry)

  real(dp) :: &
    om_min, om_max

  integer :: &
    nom, nsmear

  real(dp) :: &
    lambda_nk_tot, &
    lambda_qv_tot

  real(dp), allocatable :: &
    lambda_nk(:,:), &
    lambda_qv(:,:), &
    a2F(:,:), &
    omega(:), &
    smearings(:), &
    dwnk(:,:)   ! dwnk(nbnd,nkmax) d/dE wnk (DOS weights)

  integer, parameter :: &
    IU_EF = 98
contains

  subroutine eliashberg_main
    integer :: iq, recl_g2, iom, ismear
    character(len=100) :: epmat_fn

    integer :: i

    call start_clock ('eliashberg')

    if (master) then
      print '(a)', repeat('=',60)
      print "(1x,a,F12.6)", "Calculation of Eliashberg quantities"
      print '(a)', repeat('=',60)
      open(IU_EF,file="ef.dat",form="formatted")
    endif


    !! omega grid
    om_min = 0.d0
    om_max = 220 * 1e-3 / RYTOEV ! 250 meV
    nom = 2000
    allocate(omega(nom))

    omega = 0.d0
    do iom = 1, nom
      omega(iom) = om_min + (iom-1)*(om_max-om_min)/dble(nom-1)
    enddo

    nsmear = 6
    allocate(smearings(nsmear))
    smearings(1) = 0.5/(RYTOEV*1e3)
    smearings(2) = 1.0/(RYTOEV*1e3)
    smearings(3) = 2.0/(RYTOEV*1e3)
    smearings(4) = 3.0/(RYTOEV*1e3)
    smearings(5) = 4.0/(RYTOEV*1e3)
    smearings(6) = 5.0/(RYTOEV*1e3)

    if (master) then
      print '(1x,a)', "omega, phonon smearing grid"
      print '(3x,a,I6,2F8.2)', "nom, wmin (meV), wmax (meV) = ", nom, om_min*RYTOEV*1e3, om_max*RYTOEV*1e3
      print '(3x,a,I3)', "nsmear = ", nsmear
      do ismear = 1, nsmear
        print '(3x,a,I1,a,F8.3,a)', "ismear(",ismear,") = ", smearings(ismear)*RYTOEV*1e3, " meV"
      enddo
    endif

    allocate(dwnk(nbnd,nkmax))
    allocate(a2F(nom,nsmear))
    allocate(lambda_nk(nbnd,nkmax), lambda_qv(nmode,nqmax))

    do i = 1, nef
      ief = i 
      if (nef.eq.1) then
        e_f = efmin
      else
        e_f = efmin + (efmax-efmin)/(nef-1)*(ief-1)
      endif

      if (master) then
        print '(a)', repeat('=',60)
        print "(1x,a,F12.6)", "Calculating at E_F (eV) = ", e_f * RYTOEV
        print '(a)', repeat('=',60)
        write(IU_EF,"(I5, F20.12)", advance="no") ief, e_f*RYTOEV
      endif

      call mpi_barrier(comm, mpierr)

      if (tetrahedron) then
        call compute_dos_weights
      else
        call compute_dos_weights2
      endif
      call compute_lambda
      call compute_a2F

      if (master) then
        write(IU_EF,*)
      endif
    enddo

    if (master) then
      print '(a)', repeat('=',60)
      print "(1x,a)", "End of eliashberg_main"
      print '(a)', repeat('=',60)
      close(IU_EF)
    endif

    deallocate(dwnk)
    deallocate(omega)
    deallocate(smearings)
    deallocate(a2F)
    deallocate(lambda_nk, lambda_qv)
    call mpi_barrier(comm, mpierr)

    call stop_clock ('eliashberg')
  end subroutine eliashberg_main

  subroutine compute_dos_weights 
    integer :: nspin, isk(nk), is, i, ibnd, ik, ik_irr 
    real(dp) :: nelec, dos_t(2), wnk_irr(nbnd, nk), dwnk_irr(nbnd,nk)
    character(len=100) :: fn

    call start_clock('wnk')
    if (master) then
      call timestamp ("DOS weights")
    endif

    tetra_type = 0

    CALL tetra_init ( nrot, s, time_reversal, t_rev, latt, relatt, nkmax, &
         0,0,0, dimk,dimk,1, nk, xk)

    isk = 1
    is = 0
    nspin = 1
    nelec = 0.d0 !! not used in tetra_weights_only

    call linear_tetra_weights_only (nk, nspin, is, isk, nbnd, ntetra, &
      tetra, enk, e_f, wnk_irr, dwnk_irr)

    call tetra_dos_t (enk, nspin, nbnd, nk, e_f, dos_t)

    ! per spin
    N_F = dos_t(1) * .5d0

    wnk_irr = wnk_irr * .5d0 
    if (N_F.lt.eps12) then
      dwnk_irr = 0.d0
    else
      dwnk_irr = dwnk_irr * .5d0 / N_F  
      dwnk_irr = dwnk_irr/sum(dwnk_irr)
    endif

    do ik = 1, nkmax
      ik_irr = equiv(ik)
      dwnk(:,ik) = dwnk_irr(:,ik_irr)/(wk(ik_irr)*nkmax)
    enddo

    if (master) then
      print *, "Fermi energy (eV) = ", E_F * RYTOEV
      print *, "DOS per spin at E_F (states/spin/eV) = ", N_F/RYTOEV
      print *, "sum(dwnk) = ", sum(dwnk)
      write(fn, '(a,I0.3,a)') 'dwnk.',ief,'.dat'
      call io_2d_real_fmt('write', fn, nbnd, nkmax, dwnk)
    endif

    call stop_clock('wnk')
  end subroutine compute_dos_weights

  subroutine compute_dos_weights2
    integer :: ik, ibnd
    real(dp) :: smear
    real(dp), external :: w0gauss

    call start_clock('wnk')
    if (master) then
      call timestamp ("DOS weights")
    endif

    smear = degaussw 
    do ik = 1, nkmax
      do ibnd = 1, nbnd
        dwnk(ibnd,ik) = w0gauss((e_f-enk(ibnd,equiv(ik)))/smear, 0)/smear
      enddo
    enddo

    N_F = sum(dwnk)

    dwnk = dwnk / N_F

    if (master) then
      print *, "Fermi energy (eV) = ", E_F * RYTOEV
      print *, "DOS per spin at E_F (states/spin/eV) = ", N_F/RYTOEV
      print *, "sum(dwnk) = ", sum(dwnk)
    endif

    call stop_clock('wnk')
  end subroutine compute_dos_weights2

  subroutine compute_lambda
    integer :: &
      recvcounts(0:nprocs-1), displs(0:nprocs-1), iq_offset, nq_loc, &
      iq_loc, iq_irr, iq, imode, i, ik_irr, ik, &
      ibnd, jbnd, ikq, ikq_irr, ikq_sym, recl_epmat
      
    real(dp) :: w_qv, w_log, mat
    character(len=100) :: timestr, fn

    real(dp), allocatable :: &
      g2(:,:,:)

    call start_clock ('compute_lambda')

    if (master) then
      call timestamp("Calculation of electron-phonon coupling strength")
    endif

    allocate(g2(nbnd,nbnd,nkmax))
    inquire(iolength=recl_epmat) g2

    lambda_nk = 0.d0
    lambda_qv = 0.d0
    w_log = 0.d0

    call distribute_indices(nqmax, nprocs, displs, recvcounts) 
    nq_loc = recvcounts(rank)
    iq_offset = displs(rank)

    do iq_loc = 1, nq_loc
      iq = iq_offset + iq_loc
      iq_irr = equiv(iq)

      write(fn, '(a,I0.6,a)') &
        trim(data_ep)//'/epmat.', iq, '.dat'
      open(IU_EPMAT, file=trim(fn), form="unformatted", &
        status="old", access="direct", recl=recl_epmat)

      do i = 1, nmode
        imode = imodes(i)
        if (freq(i, iq_irr).lt.acoustic_cut) then
          cycle
        endif

        read(IU_EPMAT, rec=i) g2

        do ik = 1, nkmax
          ik_irr = equiv(ik)
          call get_kq(ik, iq, ikq, ikq_irr, ikq_sym)

          do ibnd = 1, nbnd
            do jbnd = 1, nbnd
              mat = g2(ibnd,jbnd,ik)
              w_qv = freq(i,iq_irr)

              lambda_nk(ibnd,ik) = lambda_nk(ibnd,ik) + 2.0d0*N_F            * mat/w_qv*dwnk(jbnd,ikq)
              lambda_qv(i,iq)    = lambda_qv(i,iq)    + 2.0d0*N_F*nkmax/w_qv * mat*dwnk(ibnd,ik)*dwnk(jbnd,ikq)
              w_log              = w_log              + 2.0d0*N_F            * log(w_qv)*mat/w_qv*dwnk(ibnd,ik)*dwnk(jbnd,ikq)
            enddo
          enddo
        enddo
      enddo
      close(IU_EPMAT)
    enddo

    call mpi_allreduce(mpi_in_place, lambda_nk, nbnd*nkmax, &
      mpi_double_precision, mpi_sum, comm, mpierr)

    call mpi_allreduce(mpi_in_place, lambda_qv, nmode*nqmax, &
      mpi_double_precision, mpi_sum, comm, mpierr)

    call mpi_allreduce(mpi_in_place, w_log, 1, &
      mpi_double_precision, mpi_sum, comm, mpierr)

    lambda_nk_tot = sum(lambda_nk*dwnk)
    lambda_qv_tot = sum(lambda_qv)/nqmax
    w_log = exp(w_log / lambda_nk_tot)

    if (master) then
      print *, "Total electron-phonon coupling strength"
      print '(3x,a,2F20.12)', "lambda_nk_tot, lambda_qv_tot = ", lambda_nk_tot, lambda_qv_tot
      write(IU_EF, "(3F20.12)", advance="no") N_F/RYTOEV, lambda_nk_tot, w_log*RYTOEV*1e3

      write(fn, '(a,I0.3,a)') 'lambda_nk.',ief,'.dat'
      call io_2d_real_fmt('write', fn, nbnd, nkmax, lambda_nk)
    endif

    call stop_clock ('compute_lambda')
  end subroutine compute_lambda


  subroutine compute_a2F
    real(dp), external :: w0gauss
    character(len=100) :: fn

    integer :: &
      recvcounts(0:nprocs-1), displs(0:nprocs-1), iq_offset, nq_loc, &
      iq_loc, iq_irr, iq, imode, i, ik_irr, ik, &
      ibnd, jbnd, ikq, ikq_irr, ikq_sym, &
      iom, ismear

    call start_clock ('compute_a2F')

    if (master) then
      call timestamp("Calculation of Eliashberg function")
    endif

    call distribute_indices(nqmax, nprocs, displs, recvcounts) 
    nq_loc = recvcounts(rank)
    iq_offset = displs(rank)

    a2F = 0.d0

    do ismear = 1, nsmear
      do iom = 1, nom
        do iq_loc = 1, nq_loc
          iq = iq_offset + iq_loc
          iq_irr = equiv(iq)

          do i = 1, nmode
            if (freq(i, iq_irr).lt.acoustic_cut) then
              cycle
            endif

            a2F(iom,ismear) = a2F(iom,ismear) &
              + 0.5d0/nqmax*freq(i,iq_irr)*lambda_qv(i,iq)*w0gauss((omega(iom)-freq(i,iq_irr))/smearings(ismear), 0)/smearings(ismear)
          enddo
        enddo
      enddo
    enddo

    call mpi_allreduce(mpi_in_place, a2F, nom*nsmear, mpi_double_precision, mpi_sum, comm, mpierr)
    
    if (master) then
      print '(1x,a)', "Writing to a2F.dat"
      write(fn, '(a,I0.3,a)') 'a2F.',ief,'.dat'
      open(11, file=trim(fn), form="formatted")
        do iom = 1, nom
          write(11, "(F20.6)", advance="no") omega(iom)*RYTOEV*1e3
          do ismear = 1, nsmear
            write(11, "(F20.6)", advance="no") a2F(iom,ismear)
          enddo
          write(11,"(a)")
        enddo
      close(11)
    endif

    call stop_clock ('compute_a2F')
  end subroutine compute_a2F
end module eliashberg
