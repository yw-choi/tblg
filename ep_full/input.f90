module input

  use mpi

  use constants, only: &
    RYTOEV, ANGSTROM_AU, AMU_RY

  use env, only: &
    data_ph, data_tb, data_ep

  use lattice, only: struct_fn, lsymmetry

  use epmat, only: &
    dimk, lowest_band, highest_band, &
    nbnd_tot, nbnd, lread_epmat, &
    acoustic_cut, rcut_tb, mass, imodes_fn, &
    start_task, end_task, n_print_progress

  use eliashberg, only: &
    leliashberg, tetrahedron, degaussw, &
    efmin, efmax, nef

  implicit none
  public

contains

  subroutine read_input

    logical :: exst
    logical, external :: makedirqq

    call printblock_start ( "Input parameters" )

    !! &input
    data_ph = './data'
    data_tb = './data'
    data_ep = './data'
    struct_fn = 'atoms.in'

    rcut_tb = 5.d0 * ANGSTROM_AU
    mass = 12.0107 * AMU_RY
    acoustic_cut = 1.d-6 / RYTOEV
    lowest_band = -1
    highest_band = -1

    leliashberg = .false.

    nef   = 0 
    efmin = 0.d0
    efmax = 0.d0

    imodes_fn = ''

    start_task = 1
    end_task = -1
    n_print_progress = 1000

    tetrahedron = .false.
    degaussw = 0.3d0

    lsymmetry = .true.

    namelist/input/ &
      data_ph, data_tb, data_ep, struct_fn, rcut_tb, &
      dimk, mass, lread_epmat, acoustic_cut, &
      nbnd_tot, lowest_band, highest_band, &
      efmin, efmax, nef, leliashberg, imodes_fn, &
      start_task, end_task, n_print_progress, &
      tetrahedron, degaussw, lsymmetry

    if (master) then
      rewind 5
      read(5, input)
      write(6, input)

      inquire(directory=trim(data_ep), exist=exst)

      if (.not.exst) then
        if (.not.makedirqq(data_ep)) then
          call errore('read_input', 'Failed to create data directory', 1)
        endif
      endif

      mass = mass * AMU_RY
      rcut_tb = rcut_tb * ANGSTROM_AU
      acoustic_cut = acoustic_cut / RYTOEV

      efmin = efmin / RYTOEV
      efmax = efmax / RYTOEV
      degaussw = degaussw / RYTOEV
    endif

    call mpi_bcast(data_ep, 100, mpi_character, 0, comm, mpierr)
    call mpi_bcast(data_tb, 100, mpi_character, 0, comm, mpierr)
    call mpi_bcast(data_ph, 100, mpi_character, 0, comm, mpierr)
    call mpi_bcast(struct_fn, 100, mpi_character, 0, comm, mpierr)
    call mpi_bcast(imodes_fn, 100, mpi_character, 0, comm, mpierr)
    call mpi_bcast(rcut_tb, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(nbnd_tot, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(lowest_band, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(highest_band, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(n_print_progress, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(start_task, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(end_task, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(mass, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(lread_epmat, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(lsymmetry, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(acoustic_cut, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(leliashberg, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(efmin, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(efmax, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(nef, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(tetrahedron, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(degaussw, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(dimk, 1, mpi_integer, 0, comm, mpierr)

    !! band indices
    if (lowest_band.ge.1.and.highest_band.ge.1 & 
        .and.lowest_band.gt.highest_band) then
      call errore('input', 'lb should be greater than hb', 1)
    endif

    call printblock_end

  end subroutine read_input

end module input
