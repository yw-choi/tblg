! Electron-phonon interaction in twisted bilayer graphene 
! within the tight-binding model,
! written by Young Woo Choi in 2018.
program ep

  use mpi
  use input, only: &
    read_input

  use lattice, only: &
    setup_lattice

  use epmat, only: &
    lread_epmat, &
    rcut_tb, &
    setup_kqgrid, &
    read_eigenvalues, &
    compute_epmat, &
    read_imodes

  use eliashberg, only: &
    leliashberg, &
    eliashberg_main

  implicit none

  call mpi_initialize
  call timestamp('Start of run')

  call init_clocks ( .true. )
  call start_clock ( 'EP' ) 

  if (master) then
    print '(a)', repeat('=',60)
    print '(a,a,a)', '== ','Electron-phonon interaction for twisted bilayer graphene',' =='
    print '(a)', repeat('=',60)

    print *, 'Running on ', nprocs, ' nodes'
    print '(a)', repeat('=',60)
  endif

  call read_input

  call setup_lattice ( rcut_tb )

  call setup_kqgrid

  call read_imodes

  call read_eigenvalues

  if (.not.lread_epmat) then
    call compute_epmat
  endif

  if (leliashberg) then
    call eliashberg_main
  endif

  call stop_ep
end program ep
