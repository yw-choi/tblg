program ph

  use mpi
  use env, only: &
    data_dir

  use input, only: &
    read_input

  use lattice, only: &
    setup_lattice

  use phonons, only: &
    rcut_ph, &
    setup_qgrid, &
    setup_fc, &
    solve_ph, &
    read_freq, &
    nmode, &
    nq, &
    freq, &
    calc_dos, &
    compute_dos, &
    calc_disp, &
    compute_disp

  use io, only: &
    io_2d_real_fmt

  implicit none

  character(len=100) :: fn

  call mpi_initialize
  call timestamp('Start of run')

  call init_clocks ( .true. )
  call start_clock ( 'ph' ) 

  if (master) then
    print '(a)', repeat('=',60)
    print '(a,a,a)', '== ','Phonons in twisted bilayer graphene',' =='
    print '(a)', repeat('=',60)

    print *, 'Running on ', nprocs, ' nodes'
    print '(a)', repeat('=',60)
  endif

  !! SETUP
  call read_input
  call setup_lattice ( rcut_ph )
  call setup_qgrid

  !! SOLVE
  if (.not.read_freq) then
    call setup_fc
    call solve_ph
  else
    write(fn, '(a)') trim(data_dir)//'/freq.dat'
    call io_2d_real_fmt ('read', fn, nmode, nq, freq)
  endif

  !! DOS
  if (calc_dos) then
    call compute_dos
  endif

  !! BAND
  if (calc_disp) then
    call compute_disp
  endif

  call stop_ph
end program ph
