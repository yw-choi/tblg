module phonons
  use mpi
  use env, only: data_dir
  use kinds, only: DP
  use constants, only: RY_TO_CMM1, RYTOEV, PI, TPI, ANGSTROM_AU

  use kgrid, only: &
    kpoint_grid, &
    read_kpoints_band
  
  use phonons_fc, only: &
    compute_fc

  use io, only: &
    io_2d_real_fmt

  use phonons_solver1, only: &
    solve_ph1

  use phonons_solver_elpa, only: &
    solve_ph_elpa

  use lattice, only: &
    latt, relatt, N_LAYER, &
    na_uc, na_uc_l, atoms_uc, &
    na_sc, na_sc_l, atoms_sc, &
    sc_uc_map, isc_map, R_isc, &
    N_INTRA_SHELL, NA_INTRA_SHELL, intra_neighbors, &
    max_inter_neighbors, num_inter_neighbors, inter_neighbors, &
    ia_offset, na_loc, recvcounts, displs, symmetry, &
    time_reversal, s, t_rev, nrot

  use ktetra, only: &
    tetra, tetra_type, tetra_init, tetra_dos_t, &
    ntetra, opt_tetra_init, opt_tetra_dos_t, &
    tetra_weights, opt_tetra_weights

  implicit none
  public

  real(dp) :: rcut_ph, mass
  logical :: write_evq, paralleloverq, read_freq
  integer :: nq, dimq, vdw_type, nqmax

  integer :: nmode
  real(dp), allocatable :: &
    wq(:), &
    xq(:,:), & ! q-points(3,nq) in frac. coordinate
    freq(:,:)       ! freq(nmode,nq) phonon frequencies in Ry

  integer, allocatable :: &
    equiv(:),  &
    equiv_s(:)

  logical :: calc_disp, write_evq_disp
  character(len=100) :: qpoints_disp_fn
  integer :: nqb
  real(dp), allocatable :: xqb(:,:), freq_disp(:,:)

  !! Force constants
  integer :: nfc
  real(dp), allocatable:: &
    fc(:,:,:) ! fc_intra(3,3,nfc)

  integer, allocatable :: &
    fc_pairs(:,:) ! fc_pairs(2,nfc)

  !! DOS
  logical :: &
    calc_dos

  real(dp) :: dos_emin, dos_emax
  integer :: dos_ne
  real(dp), allocatable :: dos(:,:) ! dos(2,dos_ne) energy (Ry), DOS (modes/Ry)
contains

  subroutine setup_qgrid

    integer :: iq, iq1, iq2, iq_equiv
    real(dp) :: xq_cart(3), xkg(3)
    call start_clock ('qgrid')
    call printblock_start ('q-point grid')

    !! qgrid
    nqmax = dimq*dimq
    allocate(xq(3,nqmax), wq(nqmax))
    allocate(equiv(nqmax), equiv_s(nqmax))
    xq = 0.d0
    wq = 0.d0
    if (master) then
      call kpoint_grid (nrot, time_reversal, .not.symmetry, &
        s, t_rev, nqmax, 0,0,0,dimq,dimq,1, nq, xq, wq, &
        equiv, equiv_s) 

      print *, "q-point grid dimension = ", dimq, "x", dimq
      print *, "Number of irreducible q-points = ", nq

      open(11, file=trim(data_dir)//"/qpoints.dat", form="formatted")
      write(11, *) nqmax, nq
      do iq = 1, nq
        xq_cart = matmul(relatt,xq(:,iq))*ANGSTROM_AU
        write(11, "(6F10.6,1x,F12.8)") xq(:,iq), xq_cart, wq(iq)
      enddo
      close(11)

      open(11, file=trim(data_dir)//"/qpoints_equiv.dat", form="formatted")
      write(11, *) nqmax
      do iq = 1, nqmax
        iq1 = int((iq-1)/dimq)+1
        iq2 = mod(iq-1,dimq)+1
        xkg(1) = dble(iq1-1)/dimq
        xkg(2) = dble(iq2-1)/dimq
        xkg(3) = 0.d0
        xq_cart = matmul(relatt,xkg)*ANGSTROM_AU
        write(11, "(2F10.6,I10,I5)") xq_cart(1:2), equiv(iq), equiv_s(iq)
      enddo
      close(11)
    endif
    call mpi_bcast(nq, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(xq, 3*nq, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(wq, nq, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(equiv, nq, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(equiv_s, nq, mpi_integer, 0, comm, mpierr)

    if (calc_disp) then
      if (master) then
        print *, "Reading q-point list for dispersion calculation."
        call read_kpoints_band ( qpoints_disp_fn, nqb, xqb )
      endif

      call mpi_bcast(nqb, 1, mpi_integer, 0, comm, mpierr)
      if (.not.master) then
        allocate(xqb(3,nqb))
      endif
      call mpi_bcast(xqb, 3*nqb, mpi_double_precision, 0, comm, mpierr)
    endif

    if (calc_dos) then
      call setup_tetra
    endif

    nmode = 3*na_uc
    allocate(freq(nmode,nq))

    call printblock_end
    call stop_clock ('qgrid')
  end subroutine setup_qgrid

  subroutine setup_fc
    call start_clock('fc')
    call printblock_start ('Computing force constants')
    call compute_fc (rcut_ph, vdw_type, nfc, fc_pairs, fc)
    if (master) then
      if (vdw_type.eq.1) then
        print *, "vdW interaction potential type = LJ"
      else
        print *, "vdW interaction potential type = KC"
      endif
      print *, "Number of force constants (nfc) = ", nfc
    endif
    call stop_clock('fc')
    call printblock_end
  end subroutine setup_fc

  subroutine solve_ph
    character(len=100) :: fn

    call printblock_start ('Solving phonons')
    call start_clock('solve_ph')

    if (paralleloverq) then
      call solve_ph1 (nmode, nq, xq, mass, &
                      nfc, fc_pairs, fc, &
                      write_evq, .false., freq)
    else
      call solve_ph_elpa (nmode, nq, xq, mass, &
                          nfc, fc_pairs, fc, &
                          write_evq, .false., freq)
    endif

    if (master) then
      write(fn, '(a)') trim(data_dir)//'/freq.dat'
      call io_2d_real_fmt ('write', fn, nmode, nq, freq)
    endif

    call printblock_end
    call stop_clock ('solve_ph')
  end subroutine solve_ph

  subroutine compute_dos
    integer :: ie, ie_loc
    character(len=100) :: fn
    integer :: recvcounts(0:nprocs-1), displs(0:nprocs-1), ie_offset, ne_loc

    integer, parameter :: dos_ne = 1000

    call start_clock('compute_dos')
    call printblock_start ('Computing Density of States')

    allocate(dos(2,dos_ne))
    !! Distribution over energy
    call distribute_indices(dos_ne, nprocs, rank, displs, recvcounts)
    ie_offset = displs(rank)
    ne_loc = recvcounts(rank)

    dos_emin = 0.d0
    dos_emax = maxval(freq)*1.1

    do ie_loc = 1, ne_loc
      ie = ie_loc + ie_offset

      dos(1,ie) = dos_emin + (ie-1)*(dos_emax-dos_emin)/(dos_ne-1)

      call dos_at_e( nmode, nq, freq, dos(1,ie), dos(2,ie) )
    enddo

    call mpi_allgatherv(mpi_in_place,0,mpi_datatype_null,&
                  dos, 2*recvcounts, 2*displs, &
                  mpi_double_precision,comm,mpierr)

    if (master) then
      write(fn, '(a)') trim(data_dir)//'/phdos.dat'
      print '(a,a)', "Saving phonon DOS to ", trim(fn)

      open(11, file=trim(fn), form="formatted", status="unknown")
      write(11,*) "# energy (meV)     DOS (modes/meV)"
      do ie = 1, dos_ne
        write(11, *) dos(1,ie)*RYTOEV*1e3, dos(2,ie)/RYTOEV*1e-3
      enddo
      close(11)
    endif
    call mpi_barrier(comm, mpierr)

    call stop_clock('compute_dos')
    call printblock_end

  end subroutine compute_dos

  subroutine setup_tetra
    real(dp), allocatable :: xk_cart(:,:)
    integer :: i, k1, k2, k3, kstep

    call start_clock('setup_tetra')

    k1 = 0 ! no shifted grid
    k2 = 0
    k3 = 0
    kstep = 1

    allocate(xk_cart(3,nq))
    do i = 1, nq
      xk_cart(:,i) = matmul(relatt, xq(:,i))/TPI
    enddo

    if(tetra_type == 0) then
      if (master) then
        print *, "Linear tetrahedron method with Bloechl correction used"
      endif
      CALL tetra_init ( nrot, s, time_reversal, t_rev, latt, relatt/TPI, nqmax, &
           k1,k2,k3, dimq,dimq,1, nq, xk_cart)
    ELSE
      if (master) then
        IF(tetra_type == 1) THEN 
           print *, "Linear tetrahedron method is used"
        ELSE
           print *, "Optimized tetrahedron method used"
        END IF
      endif
      CALL opt_tetra_init ( nrot, s, time_reversal, t_rev, latt, relatt/TPI, nqmax, &
           k1,k2,k3, dimq,dimq,1, nq, xk_cart, kstep)
    END IF

    call stop_clock('setup_tetra')

  end subroutine setup_tetra

  subroutine compute_disp
    character(len=100) :: fn

    call printblock_start ('Computing dispersion')
    call start_clock ('compute_disp')

    allocate(freq_disp(nmode,nqb))
    if (paralleloverq) then
      call solve_ph1 (nmode, nqb, xqb, mass, &
                      nfc, fc_pairs, fc, &
                      write_evq_disp, .true., freq_disp)
    else
      call solve_ph_elpa (nmode, nqb, xqb, mass, &
                          nfc, fc_pairs, fc, &
                          write_evq_disp, .true., freq_disp)
    endif

    if (master) then
      write(fn, '(a)') trim(data_dir)//'/freq_disp.dat'
      call io_2d_real_fmt ('write', fn, nmode, nqb, freq_disp)
    endif

    call printblock_end
    call stop_clock ('compute_disp')
  end subroutine compute_disp

  ! DOS per spin at energy
  subroutine dos_at_e ( nbnd, nks, et, energy, dos )
    integer, intent(in) :: nbnd, nks
    real(dp), intent(in) :: et(nbnd, nks), energy
    real(dp), intent(out) :: dos

    real(dp) :: dosval(2)
    integer :: nspin
    nspin = 1

    if (tetra_type == 0) then
      call tetra_dos_t( et, nspin, nbnd, nks, energy, dosval)
    else
      call opt_tetra_dos_t( et, nspin, nbnd, nks, energy, dosval)
    end if
    dos = dosval(1)*0.5d0 ! DOS per spin == DOS for phonon
  end subroutine dos_at_e
end module phonons

