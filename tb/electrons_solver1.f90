module electrons_solver1
  use mpi
  use constants, only: TPI
  use kinds, only: DP
  use diag, only: diagh

  use env, only: data_dir

  use lattice, only: &
    latt, relatt, &
    na_uc, atoms_uc, &
    na_sc, atoms_sc, &
    sc_uc_map, neighbors, &
    num_neighbors, max_neighbors, &
    isc_map, R_isc

  use io, only: &
    io_2d_cmplx, &
    io_2d_real_fmt

  implicit none
  public

contains

  ! Parallel over k-points.
  ! LAPACK is used
  subroutine solve_tb1 (nk, kpoints, nhop, hop_pairs, hop, &
                        nband, lb, hb, write_cnk, band, enk)

    integer, intent(in) :: nk, nhop, nband, lb, hb, &
                           hop_pairs(2,nhop)
    real(dp), intent(in) :: kpoints(3,nk), hop(nhop)
    logical, intent(in) :: write_cnk, band
    real(dp), intent(out) :: enk(nband, nk)
    real(dp) :: enk_buffer(nband,nk)
    integer :: recvcounts(0:nprocs-1), displs(0:nprocs-1), &
               ik_offset, nk_loc, ik_loc, ik
    integer :: ib, i
    complex(dp), allocatable :: Hmat(:,:), cnk(:,:)
    complex(dp), external :: zdotc
    complex(dp) :: phase
    real(dp) :: errmax, invnorm
    character(len=100) :: fn

    allocate(Hmat(na_uc, na_uc), cnk(na_uc,nband))

    enk = 0.d0

    !! Distribution of k-points
    call distribute_indices(nk, nprocs, rank, displs, recvcounts)
    ik_offset = displs(rank)
    nk_loc = recvcounts(rank)

    if (master) then
      print *, "Distribution of k-points"

      do i = 0, nprocs-1
        print '(1x,a,3I5)', 'rank, ik_offset, nk_loc = ', i, displs(i), recvcounts(i)
      enddo
    endif

    !! main loop
    do ik_loc = 1, nk_loc
      if (master) then
        print '(1x,a,I5,a,I5)', " >> Computing kpt # ", ik_loc, '/', nk_loc
      endif

      ik = ik_loc + ik_offset

      call build_Hmat(kpoints(1:3,ik), nhop, hop_pairs, hop, &
                      Hmat, errmax)

      ! write(fn, '(a,I0.6,a)') trim(data_dir)//'/hmat.', ik, '.dat'
      ! call io_2d_cmplx ( 'write', fn, na_uc, na_uc, hmat )

      if (errmax.gt.1e-10) then
        print *, 'errmax = ', errmax
        call errore('solve_tb', 'Hermicity is broken', 1)
      endif

      call diagh(na_uc, Hmat, lb, hb, enk(:,ik), cnk)

      ! Normalize
      do ib = 1, nband
        invnorm = 1.d0/sqrt( zdotc ( na_uc, cnk(1:na_uc, ib), 1, cnk(1:na_uc, ib), 1 ) )
        cnk(1:na_uc, ib) = cnk(1:na_uc, ib) * invnorm
      enddo

      if (write_cnk) then
        if (band) then
          write(fn, '(a,I0.6,a)') trim(data_dir)//'/cnk_band.', ik, '.dat'
        else
          write(fn, '(a,I0.6,a)') trim(data_dir)//'/cnk.', ik, '.dat'
        endif

        call start_clock('io_cnk')
        call io_2d_cmplx ( 'write', fn, na_uc, nband, cnk )
        call stop_clock ('io_cnk')
      endif
    enddo
    
    if (master) then
      print *, "Finished diagonalization. Collecting eigenvalues.."
    endif

    ! call mpi_allgatherv(mpi_in_place, 0, mpi_datatype_null,&
    !               enk, nband*recvcounts, nband*displs, &
    !               mpi_double_precision, comm, mpierr)
    call mpi_allgatherv(enk(:,ik_offset+1:ik_offset+nk_loc), nband*nk_loc, mpi_double_precision,&
                  enk_buffer, nband*recvcounts, nband*displs, &
                  mpi_double_precision, comm, mpierr)
    enk = enk_buffer

  end subroutine solve_tb1

  subroutine build_Hmat(k, nhop, hop_pairs, hop, Hmat, errmax)
    real(dp), intent(in) :: k(3)

    integer, intent(in) :: nhop, hop_pairs(2,nhop)
    real(dp), intent(in) :: hop(nhop) 

    real(dp), intent(out) :: errmax
    complex(dp), intent(out) :: Hmat(na_uc, na_uc)

    integer :: ihop, ia_uc, ja_sc, ja_uc
    real(dp) :: arg, err, R(3)

    call start_clock ('build_Hmat')

    Hmat = 0.d0

    do ihop = 1, nhop
      ia_uc = hop_pairs(1,ihop)
      ja_sc = hop_pairs(2,ihop)
      ja_uc = sc_uc_map(ja_sc)
      R = R_isc(1:3, isc_map(ja_sc))
      arg = TPI*sum(k*R)
      Hmat(ia_uc,ja_uc) = Hmat(ia_uc,ja_uc)+hop(ihop)*cmplx(cos(arg),sin(arg),kind=dp)
    enddo

    errmax = -1.d0
    do ia_uc = 1, na_uc
      do ja_uc = ia_uc, na_uc
        err = abs(Hmat(ia_uc,ja_uc)-conjg(Hmat(ja_uc,ia_uc)))
        if (err.gt.errmax) then
          errmax = err
        endif
        Hmat(ia_uc,ja_uc) = 0.5d0*(Hmat(ia_uc,ja_uc)+conjg(Hmat(ja_uc,ia_uc)))
        Hmat(ja_uc,ia_uc) = conjg(Hmat(ia_uc,ja_uc))
      enddo
    enddo

    call stop_clock ('build_Hmat')
  end subroutine build_Hmat
end module electrons_solver1
