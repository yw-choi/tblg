module input

  use mpi
  use constants, only: RYTOEV, ANGSTROM_AU, AMU_RY
  use env, only: data_dir
  use lattice, only: read_struct, struct_fn, M, N, alat, d_layer, shift, symmetry

  use electrons, only: rcut_hopping, lowest_band, highest_band, &
                       dimk, write_cnk, &
                       dos_emin, dos_emax, dos_ne, &
                       calc_band, write_cnk_band, kpoints_band_fn, &
                       calc_dos, read_enk, paralleloverk, bias, &
                       uniform_n_grid
  use ktetra, only: tetra_type

  implicit none
  public

contains

  subroutine read_input

    logical :: exst
    logical, external :: makedirqq

    call printblock_start ( "Input parameters" )

    !! &input
    read_struct = .false.
    data_dir = "./data"

    M = 0
    N = 1
    alat = 2.46d0
    d_layer = 3.35d0
    shift(1:2) = 0.d0

    paralleloverk = .false.

    rcut_hopping = 5.d0 * ANGSTROM_AU
    lowest_band = -1
    highest_band = -1
    dimk = 1
    write_cnk = .false.
    tetra_type = 0
    dos_emin = -2.d0/RYTOEV
    dos_emax = 2.d0/RYTOEV
    dos_ne   = 100
    calc_band = .false.
    write_cnk_band = .false.
    kpoints_band_fn = ''
    calc_dos = .false.
    read_enk = .false.
    symmetry = .true.

    uniform_n_grid = .false.

    bias = 0.d0

    namelist/input/ data_dir, read_struct, struct_fn, &
                     M, N, alat, d_layer, shift, &
                     paralleloverk, &
                     rcut_hopping, lowest_band, highest_band, &
                     dimk, write_cnk, tetra_type, &
                     dos_emin, dos_emax, dos_ne, &
                     calc_band, write_cnk_band, &
                     kpoints_band_fn, calc_dos, read_enk, bias, &
                     symmetry, uniform_n_grid


    if (master) then
      read(5, input)
      write(6, input)

      inquire(directory=trim(data_dir), exist=exst)

      if (.not.exst) then
        if (.not.makedirqq(data_dir)) then
          call errore('read_input', 'Failed to create data directory', 1)
        endif
      endif

      alat = alat * ANGSTROM_AU
      d_layer = d_layer * ANGSTROM_AU
      rcut_hopping = rcut_hopping * ANGSTROM_AU
      dos_emin = dos_emin / RYTOEV
      dos_emax = dos_emax / RYTOEV
      bias = bias / RYTOEV
    endif

    call mpi_bcast(read_struct, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(data_dir, 100, mpi_character, 0, comm, mpierr)
    call mpi_bcast(M, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(N, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(alat, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(d_layer, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(shift, 2, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(paralleloverk, 1, mpi_logical, 0, comm, mpierr)

    call mpi_bcast(rcut_hopping, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(lowest_band, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(highest_band, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(dimk, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(write_cnk, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(tetra_type, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(dos_emin, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(dos_emax, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(dos_ne, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(calc_band, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(symmetry, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(write_cnk_band, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(uniform_n_grid, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(kpoints_band_fn, 100, mpi_character, 0, comm, mpierr)
    call mpi_bcast(calc_dos, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(read_enk, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(bias, 1, mpi_double_precision, 0, comm, mpierr)

    call printblock_end

  end subroutine read_input

end module input
