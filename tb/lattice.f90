module lattice
  use mpi
  use env, only: data_dir
  use kinds, only: DP
  use constants, only: PI, eps6, ANGSTROM_AU
  use matrix_inversion, only: invmat
  use tblg, only: tblg_unitcell

  implicit none
  public

  real(dp) :: latt(3,3), relatt(3,3)
  integer :: na_uc, na_sc
  real(dp), allocatable :: atoms_uc(:,:), atoms_sc(:,:)

  integer, allocatable :: &
    sc_uc_map(:), &    ! sc_uc_map(na_sc) sc atom index to uc index
    isc_map(:),   &    ! isc_map(na_sc) supercell index
    R_isc(:,:),   &    ! R_isc(3,nsc)   supercell Bravais lattice vector
    neighbors(:,:), &  ! neighbors(max_neighbors,na_uc) neighbor atom indices
    num_neighbors(:)   ! num_neighbors(na_uc)

  integer :: max_neighbors ! max. number of neighbors

  integer :: lsc1, lsc2, nsc1, nsc2, nsc 

  logical :: read_struct ! if true, read lattice vectors & atom positions from struct_fn 
  character(len=100) :: struct_fn

  integer :: M, N
  real(dp) :: alat, d_layer, shift(2)

  integer :: nrot, s(3,3,48), t_rev(48)
  logical :: time_reversal, symmetry
contains

  subroutine setup_lattice (rcut)
    real(dp), intent(in) :: rcut

    call start_clock('setup_lattice')
    call printblock_start ( "Crystal Structure" )

    call setup_crystal_structure

    call construct_aux_supercell ( rcut )

    call setup_neighbors ( rcut )

    call setup_symmetry 
    
    call printblock_end
    call stop_clock('setup_lattice')
  end subroutine setup_lattice

  subroutine setup_crystal_structure
    character(len=100) :: fn
    integer :: ia
    logical :: exst

    real(dp) :: costh, theta

    if (master) then

      write(fn, '(a)') trim(struct_fn)

      if (read_struct) then

        call io_lattice ('read', fn, latt, na_uc, atoms_uc)

      else ! read_struct == .false.

        print *, "Generating unit cell of twisted bilayer graphene"
        print '(1x,a,2I5)', "(M,N) = ", M, N
        print '(1x,a,2F12.6)', "shift = ", shift(1:2)
        print '(1x,a,F12.6)', "alat (Ang) = ", alat/ANGSTROM_AU
        print '(1x,a,F12.6)', "d_layer (Ang) = ", d_layer/ANGSTROM_AU

        call tblg_unitcell(M, N, shift, alat, d_layer, &
                           latt, na_uc, atoms_uc, costh, theta)

        print '(1x,a,F12.6)', "Rotation angle (deg.) = ", theta*180.d0/PI

        call io_lattice('write', fn, latt, na_uc, atoms_uc)
      endif ! read_struct

      call invmat (3, latt, relatt)
      relatt = 2*PI*transpose(relatt)

      print *, "Number of atoms in unit cell = ", na_uc
      print *, "Lattice vectors (Ang)"
      print '(1x,a,3F16.8)', 'a1 = ', latt(1:3,1)/ANGSTROM_AU
      print '(1x,a,3F16.8)', 'a2 = ', latt(1:3,2)/ANGSTROM_AU
      print '(1x,a,3F16.8)', 'a3 = ', latt(1:3,3)/ANGSTROM_AU
      print *, "Reciprocal Lattice vectors (Ang^-1)"
      print '(1x,a,3F16.8)', 'b1 = ', relatt(1:3,1)*ANGSTROM_AU
      print '(1x,a,3F16.8)', 'b2 = ', relatt(1:3,2)*ANGSTROM_AU
      print '(1x,a,3F16.8)', 'b3 = ', relatt(1:3,3)*ANGSTROM_AU

    endif

    call mpi_bcast(latt, 3*3, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(relatt, 3*3, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(na_uc, 1, mpi_integer, 0, comm, mpierr)

    if (.not.master) then
      allocate(atoms_uc(3, na_uc))
    endif

    call mpi_bcast(atoms_uc, 3*na_uc, mpi_double_precision, 0, comm, mpierr)

  end subroutine setup_crystal_structure

  subroutine construct_aux_supercell ( rcut )
    real(dp), intent(in) :: rcut

    integer :: i, stat, ia, ia_sc, i1, isc1, i2, isc2, isc 

    lsc1 = 1 + int(rcut / norm2(latt(1:3,1)) )
    lsc2 = lsc1 ! just in case they differ..
    nsc1 = 2*lsc1+1
    nsc2 = 2*lsc2+1
    nsc = nsc1*nsc2 ! total number of aux. supercells

    na_sc = na_uc * nsc

    allocate(atoms_sc(3, na_sc))
    allocate(sc_uc_map(na_sc), isc_map(na_sc), R_isc(3,nsc))
    atoms_sc(1:3, na_sc) = 0.d0

    ia_sc = 0
    isc = 0
    do i1 = 0, nsc1-1
      if (i1.gt.lsc1) then
        isc1 = i1 - nsc1
      else
        isc1 = i1 
      endif

      do i2 = 0, nsc2-1
        if (i2.gt.lsc2) then
          isc2 = i2 - nsc2
        else
          isc2 = i2 
        endif

        isc = isc + 1

        R_isc(1:3,isc) = (/ isc1, isc2, 0 /)

        do ia = 1, na_uc
          ia_sc = ia_sc + 1
          sc_uc_map(ia_sc) = ia
          isc_map(ia_sc) = isc
          atoms_sc(1:3, ia_sc) = matmul(latt(:,:), R_isc(1:3,isc)) &
                                 + atoms_uc(1:3, ia)
        enddo
      enddo
    enddo

    if (master) then
      print '(1x,a,I3,a,I3)', "Aux. supercell = ", nsc1, ' x ', nsc2
      print *, 'Number of atoms in aux. supercell = ', na_sc
    endif

    call mpi_barrier ( comm, mpierr )

  end subroutine construct_aux_supercell

  subroutine setup_neighbors ( rcut )
    real(DP), intent(in) :: rcut

    integer :: ia, ja_sc, ja, nnb, inb
    real(dp) :: dist

    allocate(num_neighbors(na_uc))

    do ia = 1, na_uc
 
      nnb = 0 

      ! count the number of atoms in aux. sc that are within the rcut.
      do ja_sc = 1, na_sc
        dist = norm2(atoms_sc(1:3,ja_sc) - atoms_uc(1:3,ia))
        if (dist.gt.eps6.and.dist.lt.rcut) then
          nnb = nnb + 1
        endif
      enddo

      num_neighbors(ia) = nnb
    enddo 

    max_neighbors = maxval(num_neighbors)

    if (master) then
      print *, "max_neighbors = ", max_neighbors
    endif

    allocate(neighbors(max_neighbors, na_uc))

    neighbors = 0

    ! Repeat a similar loop, and keep the neighbor indices
    do ia = 1, na_uc
      inb = 0
      do ja_sc = 1, na_sc
        dist = norm2(atoms_sc(1:3,ja_sc) - atoms_uc(1:3,ia))
        if (dist.gt.eps6.and.dist.lt.rcut) then
          inb = inb + 1
          neighbors(inb, ia) = ja_sc
        endif
      enddo
    enddo

    ! DBG
    ! do ia = 1, na_sc
    !   print '(1x,3F8.3)', atoms_sc(:,ia)
    ! enddo
    ! stop
    ! do ia = 1, na_uc
    !   print *, num_neighbors(ia)
    !   do inb = 1, num_neighbors(ia)
    !     print '(1x,I5,3F8.3,1x,F8.3)', neighbors(inb, ia), atoms_sc(1:3,neighbors(inb, ia)), norm2(atoms_uc(:,ia)-atoms_sc(:,neighbors(inb, ia)))
    !   enddo
    ! enddo
  end subroutine setup_neighbors

  subroutine io_lattice ( task, fn, latvec, na, atoms )
    character(len=*), intent(in) :: task, & ! 'read' or 'write'
                                      fn      ! file name

    real(dp), intent(inout) :: latvec(3,3)
    integer, intent(inout) :: na
    real(dp), allocatable, intent(inout) :: atoms(:,:)
    
    integer :: i, ia
    logical :: exst
    
    if (trim(task).eq.'read') then
      print *, 'Reading structure from ', trim(fn)

      inquire(file=trim(fn), exist=exst)
      
      if (.not.exst) then
        call errore('io_lattice', trim(fn)//' not found', 1)
      endif

      open(11, file=trim(fn), form="formatted")

      do i = 1, 3 
        read(11,*) latvec(1:3,i)
      enddo

      latvec = latvec * ANGSTROM_AU

      na = 0 

      read(11, *) na
      allocate(atoms(3,na))

      do ia = 1, na
        read(11,*) atoms(1:3, ia)
      enddo

      atoms = atoms * ANGSTROM_AU

      close(11)
      
    else if (trim(task).eq.'write') then
      print *, 'Writing structure to ', trim(fn)

      open(11, file=trim(fn), form="formatted", status="replace")

      do i = 1, 3 
        write(11,*) latvec(1:3,i) / ANGSTROM_AU
      enddo

      write(11, *) na

      do ia = 1, na
        write(11,*) atoms(1:3, ia) / ANGSTROM_AU
      enddo

      close(11)

    else
      call errore('io_lattice', 'invalid task', 1)
    endif

  end subroutine io_lattice

  subroutine write_orb_idx

    integer :: ia_sc
    character(len=100) :: fn

    if (master) then
      write(fn, '(a)') trim(data_dir)//'/orb_idx.dat'
      open(11, file=trim(fn), form="formatted")

      write(11, '(6A7)') &
        'ia_sc', & 
        'ia_uc', & 
        'isc', & 
        'n1', & 
        'n2', & 
        'n3'
      do ia_sc = 1, na_sc

        write(11,'(6I7)') &
          ia_sc, &
          sc_uc_map(ia_sc), &
          isc_map(ia_sc), &
          R_isc(1,isc_map(ia_sc)), &
          R_isc(2,isc_map(ia_sc)), &
          R_isc(3,isc_map(ia_sc))
      enddo

      close(11)
    endif
    call mpi_barrier(comm, mpierr)
  end subroutine write_orb_idx

  subroutine setup_symmetry
    integer :: isym

    t_rev = 0

    if (symmetry) then

      time_reversal = .true.
      
      nrot = 12

      s = 0

      isym =  1   ! identity                                     
      s(1,:,isym) = (/  1,  0,  0 /)
      s(2,:,isym) = (/  0,  1,  0 /)
      s(3,:,isym) = (/  0,  0,  1 /)

      isym =  2   ! 180 deg rotation - cart. axis [0,0,1]        
      s(1,:,isym) = (/ -1,  0,  0 /)
      s(2,:,isym) = (/  0, -1,  0 /)
      s(3,:,isym) = (/  0,  0,  1 /)
      
      isym =  3   !  60 deg rotation - cryst. axis [0,0,1]       
      s(1,:,isym) = (/  0,  1,  0 /)
      s(2,:,isym) = (/ -1,  1,  0 /)
      s(3,:,isym) = (/  0,  0,  1 /)
      
      isym =  4   !  60 deg rotation - cryst. axis [0,0,-1]      
      s(1,:,isym) = (/  1, -1,  0 /)
      s(2,:,isym) = (/  1,  0,  0 /)
      s(3,:,isym) = (/  0,  0,  1 /)

      isym =  5   ! 120 deg rotation - cryst. axis [0,0,1]       
      s(1,:,isym) = (/ -1,  1,  0 /)
      s(2,:,isym) = (/ -1,  0,  0 /)
      s(3,:,isym) = (/  0,  0,  1 /)

      isym =  6   ! 120 deg rotation - cryst. axis [0,0,-1]      
      s(1,:,isym) = (/  0, -1,  0 /)
      s(2,:,isym) = (/  1, -1,  0 /)
      s(3,:,isym) = (/  0,  0,  1 /)

      isym =  7   ! mirror 
      s(1,:,isym) = (/  0,  1,  0 /)
      s(2,:,isym) = (/  1,  0,  0 /)
      s(3,:,isym) = (/  0,  0, -1 /)

      isym =  8   ! mirror + 180 deg rotation - cart. axis [0,0,1]        
      s(1,:,isym) = (/  0, -1,  0 /)
      s(2,:,isym) = (/ -1,  0,  0 /)
      s(3,:,isym) = (/  0,  0, -1 /)
      
      isym =  9   ! mirror + 60 deg rotation - cryst. axis [0,0,1]       
      s(1,:,isym) = (/ -1,  1,  0 /)
      s(2,:,isym) = (/  0,  1,  0 /)
      s(3,:,isym) = (/  0,  0, -1 /)
      
      isym = 10   ! mirror + 60 deg rotation - cryst. axis [0,0,-1]      
      s(1,:,isym) = (/  1,  0,  0 /)
      s(2,:,isym) = (/  1, -1,  0 /)
      s(3,:,isym) = (/  0,  0, -1 /)

      isym = 11   ! mirror + 120 deg rotation - cryst. axis [0,0,1]       
      s(1,:,isym) = (/ -1,  0,  0 /)
      s(2,:,isym) = (/ -1,  1,  0 /)
      s(3,:,isym) = (/  0,  0, -1 /)

      isym = 12   ! mirror + 120 deg rotation - cryst. axis [0,0,-1]      
      s(1,:,isym) = (/  1, -1,  0 /)
      s(2,:,isym) = (/  0, -1,  0 /)
      s(3,:,isym) = (/  0,  0, -1 /)
    else
      time_reversal = .false.
      nrot = 1
      s = 0
      isym =  1   ! identity                                     
      s(1,:,isym) = (/  1,  0,  0 /)
      s(2,:,isym) = (/  0,  1,  0 /)
      s(3,:,isym) = (/  0,  0,  1 /)
    endif

  end subroutine setup_symmetry
end module lattice
