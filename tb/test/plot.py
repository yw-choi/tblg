from numpy import *
import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches
def plot_patch(vertices, lw, color):
    plt.gca().add_patch(patches.PathPatch(Path(vertices), lw=lw, edgecolor=color, fill=False))

dat = loadtxt('./kpoints_symm.dat', skiprows=1)
relatt = zeros((2,2))
relatt[:,0] = array([0.26774666, -0.15458361])
relatt[:,1] = array([-0.00000000,0.30916721])
plot_patch([
    relatt.dot([2./3., 1./3.])[:2],
    relatt.dot([1./3., 2./3.])[:2],
    relatt.dot([-1./3., 1./3.])[:2],
   -relatt.dot([2./3., 1./3.])[:2],
   -relatt.dot([1./3., 2./3.])[:2],
   -relatt.dot([-1./3., 1./3.])[:2],
    relatt.dot([2./3., 1./3.])[:2],
    ],0.5,'r')
plt.xlim(-relatt[0,0], relatt[0,0])
plt.ylim(-relatt[0,0], relatt[0,0])
plt.plot(dat[:,3], dat[:,4], '.')
plt.show()
