from numpy import *
import matplotlib.pyplot as plt
dos2=loadtxt('./edos_symm.dat')
plt.plot(dos2[:,0], dos2[:,1], 'r-')
dos1=loadtxt('./edos_full.dat')
plt.plot(dos1[:,0], dos1[:,1], 'k--')
plt.xlim(-.5,.5)
plt.show()
