# Tight-binding model calculation of twisted bilayer graphene with arbitrary commensurate rotation angle.
from numpy import *
import matplotlib.pyplot as plt


def main():
    ymax = 2
    ymin = -2

    enk = loadtxt('data/enk_band.dat')
    nk, nb = shape(enk)
    for ib in range(nb):
        plt.plot(enk[:,ib], 'r-', lw=0.5)

    # enk = loadtxt('enk.FK.dat')
    # nk, nb = shape(enk)
    # for ib in range(nb):
    #     if ib == 0:
    #         plt.plot(enk[:,ib], 'b-', lw=0.5, label="FK")
    #     else:
    #         plt.plot(enk[:,ib], 'b-', lw=0.5)

    # enk = loadtxt('enk.SK.FK.dat')
    # nk, nb = shape(enk)
    # for ib in range(nb):
    #     if ib == 0:
    #         plt.plot(enk[:,ib], 'k-', lw=0.5, label="SK+FK")
    #     else:
    #         plt.plot(enk[:,ib], 'k-', lw=0.5)

    plt.ylim(ymin, ymax)
    gridlinespec = { 'color': 'darkgrey',
                    'linestyle': ':',
                    'linewidth': 0.5 }
    plt.grid(True, axis='x', **gridlinespec)
    plt.axhline(0,  **gridlinespec)
    plt.ylabel('Energy (eV)')
    # plt.savefig('bands.pdf')
    plt.show()

    return

main()

