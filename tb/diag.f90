module diag
  use kinds, only: DP

  private

  public :: diagh, diagh_full

contains

  subroutine diagh(N, H, il, iu, ek, uk)
    integer, intent(in) :: N, il, iu
    complex(dp), intent(inout) :: H(N, N)
    real(dp), intent(out) :: ek(iu-il+1)
    complex(dp), intent(out) :: uk(N,iu-il+1)

    ! local variables
    integer :: nh, info, lwork, lrwork, liwork, &
               isuppz(2*n), dummy3(1), nb
    complex(dp) :: dummy1(1)
    real(dp) :: dummy2(1)
    integer, allocatable :: iwork(:)
    complex(dp), allocatable :: work(:)
    real(dp), allocatable :: rwork(:)

    real(dp), allocatable :: w(:)

    nb = iu - il + 1

    allocate(w(n))
    call ZHEEVR('V', 'I', 'U', N, H, N, 0.d0, 0.d0, &
                il, iu, -1.0, nb, &
                w, uk, N, isuppz, dummy1, -1, dummy2, -1, &
                dummy3, -1, info)
    lwork = dummy1(1)
    lrwork = dummy2(1)
    liwork = dummy3(1)
    allocate(work(lwork))
    allocate(rwork(lrwork))
    allocate(iwork(liwork))
    call ZHEEVR('V', 'I', 'U', N, H, N, 0.0d0, 0.d0, &
                il, iu, -1.0, nb, &
                w, uk, N, isuppz, work, lwork, rwork, lrwork, &
                iwork, liwork, info)

    ek(1:nb) = w(1:nb)

    if (info.ne.0) then
        write(*,*) "ZHEEVR info = ", info
        return
    endif

    deallocate(w)
    deallocate(work)
    deallocate(rwork)
    deallocate(iwork)
  end subroutine diagh

  subroutine diagh_full ( N, A, W )
    integer, intent(in) :: N
    complex(dp), intent(inout) :: A(N, N)
    real(dp), intent(out) :: W(N)

    integer :: INFO, LWORK

    complex(dp), allocatable :: WORK(:)
    complex(dp) :: LWORK_OPT
    real(dp) :: RWORK(3*N-2)

    LWORK = -1
    call ZHEEV ( 'V', 'U', N, A, N, W, LWORK_OPT, LWORK, RWORK, INFO )

    LWORK = INT(LWORK_OPT)
    allocate(WORK(LWORK))

    call ZHEEV ( 'V', 'U', N, A, N, W, WORK, LWORK, RWORK, INFO )

    if (INFO.ne.0) then
      call errore('diagh_full', 'diagonalization failed', INFO)
    endif

  end subroutine diagh_full

  subroutine diag_feast (N, NNZ, A, IA, JA, evmin, evmax, M0, eigval, eigvec, M) 
    integer, intent(in) :: N, NNZ, M0, IA(N+1), JA(NNZ)
    real(dp), intent(in) :: evmin, evmax
    complex(dp), intent(inout) :: A(NNZ)
    real(dp), intent(out) :: eigval(M0)
    complex(dp), intent(out) :: eigvec(N,M0)
    integer, intent(out) :: M

    character(len=1) :: UPLO = 'U'
    integer, dimension(64) :: fpm 
    real(dp) :: epsout, arg
    integer :: loop, info, i, j
    real(dp), allocatable :: resid(:)

    allocate(resid(M0))
    call feastinit(fpm)
    fpm(1) = 0
    fpm(9) = loc_comm

    call zfeast_hcsrev(UPLO, N, A, IA, JA, fpm, epsout, loop, &
      evmin, evmax, M0, eigval, eigvec, M, resid, info)

    ! set phase of eigenvector
    do i = 1, M
      eigvec(:,i) = eigvec(:,i) / sqrt(dot_product(conjg(eigvec(:,i)), eigvec(:,i)))
      arg = 0.d0
      do j = 1, N
        if (abs(eigvec(j,i)).gt.0.0001) then
          arg = atan2(imag(eigvec(j,i)), real(eigvec(j,i)))
          exit
        endif
      enddo
      eigvec(:,i) = eigvec(:,i) * cmplx(cos(arg),-sin(arg),kind=dp) 
    enddo
  end subroutine diag_feast

end module diag
