! Gracefully terminate the program
subroutine stop_tb
  use mpi

  call stop_clock ( 'tb' )

  call printblock_start ('Timing info')
  if (rank.eq.0) then 
    call print_clock ( ' ' )
    call system('rm -f CRASH')
  endif
  call printblock_end()
  call timestamp('End of run')
  call MPI_Finalize(mpierr)
end subroutine stop_tb
