program tb

  use mpi
  use env, only: &
    data_dir

  use input, only: &
    read_input

  use lattice, only: &
    setup_lattice

  use electrons, only: &
    nbnd, nk, enk, e_f0, &
    rcut_hopping, &
    calc_band, &
    calc_dos, &
    read_enk, &
    setup_kgrid, &
    setup_band_indices, &
    setup_hoppings, &
    solve_tb, &
    compute_band, &
    compute_dos, &
    find_fermi_energy, &
    setup_egrid

  use io, only: &
    io_2d_real_fmt


  implicit none

  character(len=100) :: fn

  call mpi_initialize
  call timestamp('Start of run')

  call init_clocks ( .true. )
  call start_clock ( 'tb' ) 

  if (master) then
    print '(a)', repeat('=',60)
    print '(a,a,a)', '== ','Tight-binding calculation for twisted bilayer graphene',' =='
    print '(a)', repeat('=',60)

    print *, 'Running on ', nprocs, ' nodes'
    print '(a)', repeat('=',60)
  endif

  !! SETUP
  call read_input
  call setup_lattice ( rcut_hopping )
  call setup_kgrid
  call setup_band_indices

  !! SOLVE
  if (.not.read_enk) then
    call setup_hoppings
    call solve_tb
  else
    write(fn, '(a)') trim(data_dir)//'/enk.dat'
    call io_2d_real_fmt ('read', fn, nbnd, nk, enk, e_f0)
  endif

  call setup_egrid

  !! DOS
  if (calc_dos) then
    call compute_dos
  endif

  !! BAND
  if (calc_band) then
    if (read_enk) then
      call setup_hoppings
    endif
    call compute_band
  endif

  call stop_tb
end program tb
