module electrons_hoppings

  use kinds, only: DP
  use constants, only: eps12, eps6, RYTOEV, ANGSTROM_AU
  use lattice, only: latt, relatt, na_uc, atoms_uc, na_sc, atoms_sc, &
                     sc_uc_map, neighbors, num_neighbors, max_neighbors, &
                     isc_map, R_isc
                     

  implicit none
  
  public :: compute_hoppings, dtdx
  
  private

  ! Slater-Koster tight-binding parameters
  real(DP), parameter :: Vpi  = -2.7d0/RYTOEV   , &
                         Vsig =  0.48d0/RYTOEV  , &
                         a0   =  2.46d0/sqrt(3.d0)*ANGSTROM_AU,  &
                         d0   =  3.35d0*ANGSTROM_AU, &
                         r0   =  0.184d0 * 2.46d0*ANGSTROM_AU

contains

  subroutine compute_hoppings ( rcut, bias, nhop, hop_pairs, hop )

    real(dp), intent(in) :: rcut, bias

    integer, intent(out) :: nhop
    integer, allocatable, intent(out) :: hop_pairs(:,:)
    real(DP), allocatable, intent(out) :: hop(:)

    integer :: ia, ja, ihop, inb, ja_uc
    real(DP) :: at1(3), at2(3), dx(3), dist, dfac

    !! Count number of hoppings for a given rcut
    nhop = 0

    do ia = 1, na_uc
      at1 = atoms_uc(1:3, ia)

      do inb = 1, num_neighbors(ia)
        ja = neighbors(inb, ia)
        ! skip the same atom (dist.lt.eps6)
        dist = norm2(atoms_sc(1:3, ja) - at1)
        if (dist.gt.eps6.and.dist.lt.rcut) then
          nhop = nhop + 1
        endif
      enddo
    enddo

    if (abs(bias).gt.eps12) then
      nhop = nhop + na_uc
    endif

    allocate(hop_pairs(2,nhop))
    allocate(hop(nhop))
    hop_pairs = -1
    hop = 0.d0

    !! Calculate hopping amplitudes
    ihop = 0

    do ia = 1, na_uc
      at1 = atoms_uc(1:3, ia)

      do inb = 1, num_neighbors(ia)
        ja = neighbors(inb, ia)
        at2 = atoms_sc(1:3, ja) 
        dx = at1 - at2
        dist = norm2(dx)

        if (dist.lt.eps6.or.dist.gt.rcut) then
          cycle
        endif

        ihop = ihop + 1

        dfac = (dx(3)/dist)**2

        hop(ihop) = Vpi*exp(-(dist-a0)/r0)*(1.d0-dfac)+Vsig*exp(-(dist-d0)/r0)*dfac

        hop_pairs(1,ihop)   = ia
        hop_pairs(2,ihop)   = ja
      enddo
    enddo

    !! bias
    if (abs(bias).gt.eps12) then
      do ia = 1, na_uc
        ihop = ihop+1
        hop(ihop) = bias*atoms_uc(3,ia)
        hop_pairs(1,ihop) = ia
        hop_pairs(2,ihop) = ia
      enddo
    endif

  end subroutine compute_hoppings

  ! derivative of hopping function w.r.t. ix axis,
  ! evaluated at d
  real(dp) function dtdx ( d, ix ) 
    integer, intent(in) :: ix
    real(dp), intent(in) :: d(3)
    real(dp) :: dist, fac1, fac2

    dist = norm2(d)

    !! Do not calculate duplicate parts
    if (ix.eq.1 .or. ix.eq.2) then

      fac1 = d(ix)*d(3)*d(3)/(r0*dist**3)
      fac2 = 2*d(ix)*d(3)*d(3)/dist**4

      dtdx = Vpi*exp(-(dist-a0)/r0)*( fac1 - d(ix)/(r0*dist) + fac2 ) &
             + Vsig*exp(-(dist-d0)/r0)*( -fac1 -fac2 ) 

    else
      fac1 = d(ix)*d(3)*d(3)/(r0*dist**3)
      fac2 = 2*d(3)*(dist**2-d(3)**2)/dist**4

      dtdx = Vpi*exp(-(dist-a0)/r0)*( fac1 - d(ix)/(r0*dist) - fac2 ) &
             + Vsig*exp(-(dist-d0)/r0)*( -fac1 + fac2 )
    endif

  end function dtdx
end module electrons_hoppings
