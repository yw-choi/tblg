module electrons
  use mpi
  use env, only: &
    data_dir

  use kinds, only: &
    DP

  use lattice, only: &
    latt, relatt, &
    na_uc, atoms_uc, &
    na_sc, atoms_sc, &
    sc_uc_map, neighbors, &
    num_neighbors, max_neighbors, &
    isc_map, R_isc, &
    nrot, s, t_rev, time_reversal, symmetry
                      
  use constants, only: &
    RYTOEV, PI, TPI, ANGSTROM_AU

  use kgrid, only: &
    kpoint_grid, &
    read_kpoints_band

  use io, only: &
    io_2d_real_fmt

  use electrons_hoppings, only: &
    compute_hoppings

  use electrons_solver1, only: &
    solve_tb1

  use electrons_solver_elpa, only: &
    solve_tb_elpa

  use electrons_solver_elpa, only: &
    solve_tb_elpa

  use ktetra, only: &
    tetra, tetra_type, tetra_init, tetra_dos_t, &
    ntetra, opt_tetra_init, opt_tetra_dos_t, &
    tetra_weights, opt_tetra_weights

  implicit none
  public

  real(dp) :: rcut_hopping, &  ! hopping function cutoff
              e_f0,         &  ! charge-neutral Fermi level (Ry)
              bias             ! interlayer bias (Ry)

  integer :: dimk, &          ! k-points grid dimension
             nkmax, &         ! nkmax=dimk*dimk
             nk, &            ! number of k-points
             nbnd, &         ! number of bands
             lowest_band, &   ! first band index
             highest_band     ! last band index

  real(dp), allocatable :: &
             xk(:,:),      &  ! k-points in frac. cooridnate
             wk(:),        &  ! k-points weight
             enk(:,:)         ! enk(nbnd,nk) electron eigenvalues (Ry).
  integer, allocatable :: &
    equiv(:),  &
    equiv_s(:)

  real(dp) :: N_F0   ! DOS per spin at charge neutral point (enk = 0)

  integer :: nhop ! number of hopping elements in real space
  integer, allocatable :: hop_pairs(:,:) ! hop_pairs(2,nhop) (ia_uc, ja_sc) pairs
  real(DP), allocatable :: hop(:) ! hop(nhop) hopping parameters
    
  logical :: &
    paralleloverk, & 
    write_cnk, &
    read_enk, &
    calc_dos, &
    uniform_n_grid

  real(dp) :: dos_emin, dos_emax
  integer :: dos_ne
  real(dp), allocatable :: dos(:,:) ! dos(2,dos_ne) energy (Ry) DOS (states/spin/Ry)

  logical :: calc_band, write_cnk_band
  character(len=100) :: kpoints_band_fn
  integer :: nkb
  real(dp), allocatable :: xkb(:,:), enkb(:,:)

contains

  subroutine setup_kgrid
    integer :: ik, ik1, ik2, ik_equiv
    real(dp) :: xk_cart(3), xkg(3)
    call start_clock ('kgrid')
    call printblock_start ('k-point grid')

    ! if symmetry = .false., nk = dimk*dimk,
    !    symmetry = .true. nk = # of irreducible k-points
    nkmax = dimk*dimk
    allocate(xk(3,nkmax), wk(nkmax))
    allocate(equiv(nkmax), equiv_s(nkmax))
    xk = 0.d0
    wk = 0.d0

    if (master) then
      call kpoint_grid (nrot, time_reversal, .not.symmetry, &
        s, t_rev, nkmax, 0,0,0,dimk,dimk,1, nk, xk, wk, &
        equiv, equiv_s) 

      print *, "k-point grid dimension = ", dimk, "x", dimk
      print *, "Number of irreducible k-points = ", nk

      open(11, file=trim(data_dir)//"/kpoints.dat", form="formatted")
      write(11, *) nkmax, nk
      do ik = 1, nk
        xk_cart = matmul(relatt,xk(:,ik))*ANGSTROM_AU
        write(11, "(4F10.6,1x,F12.8)") xk(1:2,ik), xk_cart(1:2), wk(ik)
      enddo
      close(11)

      open(11, file=trim(data_dir)//"/kpoints_equiv.dat", form="formatted")
      write(11, *) nkmax
      do ik = 1, nkmax
        ik1 = int((ik-1)/dimk)+1
        ik2 = mod(ik-1,dimk)+1
        xkg(1) = dble(ik1-1)/dimk
        xkg(2) = dble(ik2-1)/dimk
        xkg(3) = 0.d0
        xk_cart = matmul(relatt,xkg)*ANGSTROM_AU
        write(11, "(2F10.6,I10,I5)") xk_cart(1:2), equiv(ik), equiv_s(ik)
      enddo
      close(11)
    endif
    call mpi_bcast(nk, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(xk, 3*nk, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(wk, nk, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(equiv, nk, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(equiv_s, nk, mpi_integer, 0, comm, mpierr)

    if (calc_band) then
      if (master) then
        print *, "Reading k-point list for band calculation."
        call read_kpoints_band ( kpoints_band_fn, nkb, xkb )
      endif

      call mpi_bcast(nkb, 1, mpi_integer, 0, comm, mpierr)
      if (.not.master) then
        allocate(xkb(3,nkb))
      endif
      call mpi_bcast(xkb, 3*nkb, mpi_double_precision, 0, comm, mpierr)
    endif
  
    call setup_tetra

    call printblock_end
    call stop_clock ('kgrid')
  end subroutine setup_kgrid

  subroutine setup_band_indices
    call printblock_start ('Band indices')

    !! band indices
    if (lowest_band.ge.1.and.highest_band.ge.1 & 
        .and.lowest_band.gt.highest_band) then
      call errore('setup_band_indices', 'lb should be greater than hb', 1)
    endif

    if (lowest_band.le.0) lowest_band = 1
    if (highest_band.le.0) highest_band = na_uc

    nbnd = highest_band-lowest_band+1

    if (master) then
      print *, 'na_uc / 2 = ', na_uc / 2
      print *, "lowest band = ", lowest_band
      print *, "highest band = ", highest_band
      print *, "nbnd = ", nbnd
    endif

    allocate(enk(nbnd,nk))

    call printblock_end
  end subroutine setup_band_indices

  subroutine setup_hoppings
    call printblock_start ('Computing hopping parameters')
    call start_clock('hoppings')
    call compute_hoppings ( rcut_hopping, bias, nhop, hop_pairs, hop )
    if (master) then
      print *, "Number of hopping parameters = ", nhop
    endif
    call stop_clock('hoppings')
    call printblock_end
  end subroutine setup_hoppings

  subroutine compute_dos
    integer :: ie, ie_loc
    character(len=100) :: fn
    integer :: recvcounts(0:nprocs-1), displs(0:nprocs-1), ie_offset, ne_loc

    call start_clock('compute_dos')
    call printblock_start ('Computing Density of States')

    !! Distribution over energy
    call distribute_indices(dos_ne, nprocs, rank, displs, recvcounts)
    ie_offset = displs(rank)
    ne_loc = recvcounts(rank)

    if (master) then
      print *, "Density of states calculation"
    endif

    do ie_loc = 1, ne_loc
      ie = ie_loc + ie_offset
      call dos_at_e( dos(1,ie), dos(2,ie) )
    enddo

    call mpi_allgatherv(mpi_in_place,0,mpi_datatype_null,&
                  dos, 2*recvcounts, 2*displs, &
                  mpi_double_precision,comm,mpierr)

    if (master) then
      write(fn, '(a)') trim(data_dir)//'/edos.dat'
      print '(a,a)', "Saving electron DOS to ", trim(fn)

      open(11, file=trim(fn), form="formatted", status="unknown")
      write(11,'(a,2F10.6, I)') "# emin (eV), emax (eV), ne = ", &
                  dos_emin*RYTOEV, &
                  dos_emax*RYTOEV, &
                  dos_ne 

      write(11,*) "# energy (eV)     DOS (states/spin/eV)"
      do ie = 1, dos_ne
        write(11, *) dos(1,ie)*RYTOEV, dos(2,ie)/RYTOEV
      enddo
      close(11)
    endif
    call mpi_barrier(comm, mpierr)

    call stop_clock('compute_dos')
    call printblock_end
  end subroutine compute_dos

  subroutine setup_tetra
    real(dp), allocatable :: xk_cart(:,:)
    real(dp) :: dos_t(2)
    integer :: i, k1, k2, k3, kstep

    call start_clock('setup_tetra')

    k1 = 0 ! no shifted grid
    k2 = 0
    k3 = 0
    kstep = 1

    allocate(xk_cart(3,nk))
    do i = 1, nk
      xk_cart(:,i) = matmul(relatt, xk(:,i))/TPI
    enddo

    if(tetra_type == 0) then
      if (master) then
        print *, "Linear tetrahedron method with Bloechl correction used"
      endif
      CALL tetra_init ( nrot, s, time_reversal, t_rev, latt, relatt/TPI, nkmax, &
           k1,k2,k3, dimk,dimk,1, nk, xk_cart)
      
    ELSE
      if (master) then
        IF(tetra_type == 1) THEN 
           print *, "Linear tetrahedron method is used"
        ELSE
           print *, "Optimized tetrahedron method used"
        END IF
      endif
      CALL opt_tetra_init ( nrot, s, time_reversal, t_rev, latt, relatt/TPI, nkmax, &
           k1,k2,k3, dimk,dimk,1, nk, xk_cart, kstep)
    END IF

    call stop_clock('setup_tetra')

  end subroutine setup_tetra

  subroutine setup_egrid
    integer :: i, nmax
    real(dp) :: energy, wg(nbnd,nk), n, ngrid(dos_ne)
    integer :: is, isk(nk)

    integer :: recvcounts(0:nprocs-1), displs(0:nprocs-1), &
      i_offset, ni_loc, i_loc
    is = 0
    isk = 1

    allocate(dos(2,dos_ne))
    if (uniform_n_grid) then
      if (master) then
        print *, 'Finding uniform n grid'
      endif
      nmax = (highest_band-lowest_band+1)*2.d0
      call distribute_indices(dos_ne, nprocs, rank, displs, recvcounts)
      i_offset = displs(rank)
      ni_loc = recvcounts(rank)
      do i_loc = 1, ni_loc
        i = i_offset + i_loc
        n = dble(i-1)*nmax/(dos_ne-1)

        if(tetra_type == 0) then
          call tetra_weights(nk,1,nbnd,n,ntetra,tetra,enk,energy,wg,is,isk)
        else
          call opt_tetra_weights(nk,1,nbnd,n,ntetra,tetra,enk,energy,wg,is,isk)
        endif

        dos(1,i) = energy
        ngrid(i) = n
      enddo
      call mpi_allgatherv(mpi_in_place,0,mpi_datatype_null,&
                    dos(1,:), recvcounts, displs, &
                    mpi_double_precision,comm,mpierr)
      call mpi_allgatherv(mpi_in_place,0,mpi_datatype_null,&
                    ngrid, recvcounts, displs, &
                    mpi_double_precision,comm,mpierr)
      if (master) then
        print *, 'carrier vs. energy (eV) grid'
        do i = 1, dos_ne
          print '(2F20.8)', ngrid(i), dos(1,i)*RYTOEV
        enddo
      endif
    else
      do i = 1, dos_ne
        dos(1,i) = dos_emin + (i-1)*(dos_emax-dos_emin)/(dos_ne-1)
      enddo
    endif


  end subroutine setup_egrid

  subroutine solve_tb
    character(len=100) :: fn

    call printblock_start ('Solving TB Hamiltonians')
    call start_clock('solve_tb')

    if (paralleloverk) then
      call solve_tb1 (nk, xk, nhop, hop_pairs, hop, &
                      nbnd, lowest_band, highest_band, &
                      write_cnk, .false., enk)
    else
      ! call solve_tb2 (nk, kpoints, nhop, hop_pairs, hop, &
      !                 nbnd, lowest_band, highest_band, &
      !                 write_cnk, .false., enk)
      call solve_tb_elpa (nk, xk, nhop, hop_pairs, hop, &
                          nbnd, lowest_band, highest_band, &
                          write_cnk, .false., enk)
    endif

    call find_fermi_energy 

    enk = enk - e_f0

    if (master) then
      print *, "Writing eigenvalues..."
      write(fn, '(a)') trim(data_dir)//'/enk.dat'
      call io_2d_real_fmt ('write', fn, nbnd, nk, enk, e_f0)
    endif

    call printblock_end
    call stop_clock ('solve_tb')

  end subroutine solve_tb

  subroutine find_fermi_energy
    real(dp) :: nelec

    !! Finding the Fermi energy
    e_f0 = 0.d0
    nelec = dble(na_uc) - (lowest_band-1)*2
    call fermi_energy ( nelec, nbnd, nk, enk, e_f0 )
    if (master) then
      print *, "e_f0 (eV) = ", e_f0 * RYTOEV
    endif

  end subroutine find_fermi_energy

  ! DOS per spin at energy
  subroutine dos_at_e ( energy, dos )
    real(dp), intent(in) :: energy
    real(dp), intent(out) :: dos

    real(dp) :: dosval(2)
    integer :: nspin
    nspin = 1

    if (tetra_type == 0) then
      call tetra_dos_t( enk, nspin, nbnd, nk, energy, dosval)
    else
      call opt_tetra_dos_t( enk, nspin, nbnd, nk, energy, dosval)
    end if
    dos = dosval(1)*0.5d0 ! DOS at e_f0 per spin
  end subroutine dos_at_e

  subroutine fermi_energy ( nelec, nbnd, nkstot, et, ef )
    integer, intent(in) :: nbnd, nkstot
    real(dp), intent(in) :: nelec, et(nbnd,nkstot)
    real(dp), intent(out) :: ef

    integer :: nspin, isk(nkstot), is

    real(dp) :: wg(nbnd, nkstot)

    real(dp), external :: efermit

    isk = 1
    is = 0
    nspin = 1

    call start_clock('fermi_e')

    ! ef = efermit (et, nbnd, nkstot, nelec, nspin, ntetra, tetra, is, isk)


    !
    ! ... calculate weights for the metallic case using tetrahedra
    !
    IF (tetra_type == 0) then
      !
      ! Bloechl's tetrahedra
      !
      CALL tetra_weights( nkstot, nspin, nbnd, nelec, &
                         ntetra, tetra, et, ef, wg, 0, isk )
      !
    ELSE
      !
      ! Linear or Optimized tetrahedra
      !
      !             
      CALL opt_tetra_weights ( nkstot, nspin, nbnd, nelec, ntetra,&
          tetra, et, ef, wg, 0, isk )
      !
    END IF ! tetra_type
    !

    call stop_clock('fermi_e')
  end subroutine fermi_energy
  
  subroutine compute_band

    integer :: recvcounts(0:nprocs-1), displs(0:nprocs-1), ik_offset, nk_loc
    integer :: ik_loc, ik, ib
    complex(dp), allocatable :: Hmat(:,:), un(:,:)
    real(dp) :: k_cart(3)

    character(len=100) :: fn

    call printblock_start ('Computing band structure')
    call start_clock ('compute_band')

    allocate(enkb(nbnd, nkb))

    if (paralleloverk) then
      call solve_tb1 (nkb, xkb, nhop, hop_pairs, hop, &
                      nbnd, lowest_band, highest_band, &
                      write_cnk_band, .true., enkb)
    else
      call solve_tb_elpa (nkb, xkb, nhop, hop_pairs, hop, &
                          nbnd, lowest_band, highest_band, &
                          write_cnk_band, .true., enkb)
    endif

    enkb = enkb - e_f0

    if (master) then
      write(fn, '(a)') trim(data_dir)//'/enk_band.dat'
      call io_2d_real_fmt ('write', fn, nbnd, nkb, enkb, e_f0)
    endif

    call mpi_barrier(comm, mpierr)

    call printblock_end
    call stop_clock ('compute_band')

  end subroutine compute_band

end module electrons
