from numpy import *
import matplotlib.pyplot as plt
import glob, os

i = 0
d = './'
f = open('%s/atoms.out'%d, 'r')
a1 = float64(f.readline().split())
a2 = float64(f.readline().split())
a3 = float64(f.readline().split())
na = int(f.readline())

data = []
for l in f:
    row = float64(l.split())
    data.append(row)
f.close()
data = array(data)

# print "%30s %10.6f %10.6f %10.6f"%(d, abs(data[:,3]-data[:,0]).max(), abs(data[:,4]-data[:,1]).max(),abs(data[:,5]-data[:,2]).max())
na = len(data)
plt.figure(1)
z = []
for ia in range(na/2,na):
    z.append(linalg.norm(data[ia,3:5]-data[ia,0:2])/2.46)
plt.tripcolor(data[na/2:,0], data[na/2:,1], z, shading='gouraud')
plt.colorbar()
plt.figure(2)
z = []
for ia in range(na/2):
    z.append(linalg.norm(data[ia,3:5]-data[ia,0:2])/2.46)
plt.tripcolor(data[:na/2,0], data[:na/2,1], z, shading='gouraud')
plt.colorbar()
plt.show()
