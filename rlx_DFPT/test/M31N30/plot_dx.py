from numpy import *
import matplotlib.pyplot as plt

f = open('atoms.out', 'r')
a1 = float64(f.readline().split())
a2 = float64(f.readline().split())
a3 = float64(f.readline().split())
na = int(f.readline())

data = []
for l in f:
    row = float64(l.split())
    data.append(row)
f.close()
data = array(data)

print (data[:,3]-data[:,0]).max()
print (data[:,4]-data[:,1]).max()
na = len(data)
plt.tripcolor(data[na/2:,3], data[na/2:,4], data[na/2:,5]-data[na/2:,2], shading='gouraud')
plt.show()
