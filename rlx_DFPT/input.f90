module input

  use mpi
  use constants, only: RYTOEV, ANGSTROM_AU, AMU_RY
  use lattice, only:  M, N, alat, d_layer, shift
  use rlxmod, only: initial_dx, do_rlx, constraint_type, vdw_type, rcut, conv, symmetrize
  implicit none
  public

contains

  subroutine read_input

    logical :: exst
    logical, external :: makedirqq

    call printblock_start ( "Input parameters" )

    !! &system
    initial_dx = ''
    M = 0
    N = 1
    alat = 2.46d0
    d_layer = 3.35d0
    shift(1:2) = 0.d0
    rcut = 5.d0
    vdw_type = 1 ! 1: LJ, 2: KC
    constraint_type = 0
    conv = 1.d-3
    symmetrize = .false.

    namelist/system/ initial_dx, M, N, alat, d_layer, shift, rcut, &
                     constraint_type, vdw_type, &
                     do_rlx, conv, symmetrize

    if (master) then
      read(5, system)
      write(6, system)

      alat = alat * ANGSTROM_AU
      d_layer = d_layer * ANGSTROM_AU
      rcut = rcut * ANGSTROM_AU
    endif

    call mpi_bcast(do_rlx, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(symmetrize, 1, mpi_logical, 0, comm, mpierr)
    call mpi_bcast(M, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(N, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(alat, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(d_layer, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(shift, 2, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(rcut, 1, mpi_double_precision, 0, comm, mpierr)
    call mpi_bcast(constraint_type, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(vdw_type, 1, mpi_integer, 0, comm, mpierr)
    call mpi_bcast(rcut, 1, mpi_double_precision, 0, comm, mpierr)

    call printblock_end

  end subroutine read_input

end module input
