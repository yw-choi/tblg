subroutine solve ( n, a, b )
  use kinds, only: dp

  integer, intent(in) :: n
  real(dp), intent(inout) :: a(n,n), b(n)

  integer :: info, lwork, ipiv(n)
  real(dp) :: dummy(1)
  real(dp), allocatable :: work(:)

  lwork = -1
  call dsysv('Lower', N, 1, a, N, ipiv, b, n, dummy, lwork, info)
  lwork = dummy(1)
  allocate(work(lwork))
  call dsysv('Lower', N, 1, a, N, ipiv, b, n, work, lwork, info)

  if (info.gt.0) then
    WRITE(*,*)'The element of the diagonal factor '
    WRITE(*,*)'D(',INFO,',',INFO,') is zero, so that'
    WRITE(*,*)'D is singular; the solution could not be computed.'
    STOP
  endif

  deallocate(work)
end subroutine solve
